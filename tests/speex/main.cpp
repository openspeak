#include "../include/speexpp.h"
#include "../../audio/include/PortAudioWrapper.h"
#include <iostream>

int main()
{
    std::cerr << "init speex .. ";
    SpeexWrapper *wrap = new SpeexWrapper(2);
    std::cerr << "init audio .. ";
    AudioWrapper *audio = new AudioWrapper();
    std::cerr << "finished startup .. \n";
    if (!wrap || !audio)    std::cerr << "NULL pointers after new\n";
    audio->Start(2);

    audio->setFramesize(wrap->getFrameSize());

    while(true)
    {
        short* buffer = (short*)audio->getInput();
        if (!buffer) return 0;
        char* cbuf = wrap->encode(buffer);
	if (!cbuf)	
	    continue;
       
        buffer = wrap->decode(cbuf);

        if (buffer)

        audio->setOutput(buffer);
        delete[] buffer;
        delete[] cbuf;
    }

    delete wrap;
    delete audio;
    return 0;
}
