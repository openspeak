/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"

#define CHECK_OPTION(name,def) 	if (!mConfig->optionExists(name)) \
								{ \
									if (!mCommand->getStringValue(name).empty()) \
										mConfig->setOption(name, mCommand->getStringValue(name));  \
									else \
										mConfig->setOption(name, def); \
								}

using namespace openSpeak;

#ifdef OS_PLATFORM_LINUX
	void SigHandler(int code)
	{
		LOG_DEBUG("Core: Got a shutdown signal")
		ServerCore::getSingleton()->stop();
	}
#endif

	ServerCore::ServerCore(int argc, char** argv)
	{	
	/* Some basic setup */
		mConnection = 0;
		mQuit = false;
		mID = 0;
		mLastCheck = 0.0;
		
	/* Setup a new action for the SIGTERM signal on linux */
#ifdef OS_PLATFORM_LINUX
		struct sigaction action;
		memset(&action, 0, sizeof(struct sigaction));
		action.sa_handler = &SigHandler;
		if (sigaction(SIGTERM, &action, 0) < 0)
			EXCEPTION("Couldn't register a handler for SIGTERM events")
		if (sigaction(SIGINT, &action, 0))
			EXCEPTION("Couldn't register a handler for SIGINT events")
#endif
		
		mCommand = new CmdLineParser("openSpeak Server", "v0.1 SVN");

		CmdLineOption ServerOptions[] = {
			{"help", 'h', OPTION_HELP, NULL, "Print this help message", ""},
			{"version", 'v', OPTION_VERSION, NULL, "Print version informations", ""},
#ifdef OS_PLATFORM_LINUX
			{"daemon", 'd', OPTION_ARG_NONE, NULL, "Use this option to make the server fork to the background", ""},
			{"pid", 'p', OPTION_ARG_STRING, NULL, "Save/Load the pid in/from the specified file", "FILE"},
			{"kill", 'k', OPTION_ARG_NONE, NULL, "Kill an instance of the server (use it with --pid FILE)",""},
#endif
			{"config", 'c', OPTION_ARG_STRING, NULL, "Specify the config file to load", "FILE"},		
			{"log", 'l', OPTION_ARG_STRING, NULL, "Specify the log file to write to", "FILE"},
			{"create-config", 'a', OPTION_ARG_NONE, NULL, "Create the initial config file and exit", ""},
			{NULL}
		};
		mCommand->addOption(ServerOptions);

		if (mCommand->parseLine(argc, argv))
		{
			printf("\nIncorrect Argument !\nPlease run %s --help\n", argv[0]);
			exit(-1);
		}
		
		std::string path, cfgfile, logfile;
#if defined( OS_PLATFORM_WIN32 )
		if (mCommand->getStringValue("log").empty())
		{

			path = "./";
			WIN32_FIND_DATA FileData;
			if (FindFirstFile(std::string(path + "log/*").c_str(), &FileData) == INVALID_HANDLE_VALUE)
				if(_mkdir(std::string(path + "log/").c_str()) == -1)
					EXCEPTION("Couldn't create log directory!")        
#elif defined( OS_PLATFORM_LINUX )
		if (mCommand->getStringValue("config").empty() || mCommand->getStringValue("log").empty())
		{
			path = getenv("HOME") + std::string("/.openspeak/");
			if (opendir(std::string(path).c_str()) == NULL)
				if (mkdir(path.c_str(), S_IRWXU | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH) == -1)
					EXCEPTION("Couldn't create directory ~/.openspeak")
			if (opendir(std::string(path + "log/").c_str()) == NULL)
				if (mkdir(std::string(path + "log/").c_str(), S_IRWXU | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH) == -1)
					EXCEPTION("Couldn't create directoriy ~/.openspeak/log")
#endif
			if (!mCommand->getStringValue("config").empty())
				cfgfile = mCommand->getStringValue("config");
			else
				cfgfile = path + "settings.server.xml";
			
			if (!mCommand->getStringValue("log").empty())
				logfile = mCommand->getStringValue("log");
			else
				logfile = path + "log/server.log";
		}
		else
		{
			cfgfile = mCommand->getStringValue("config");
			logfile = mCommand->getStringValue("log");
		}
		
		new Log(logfile, mCommand->getSwitch("daemon") || mCommand->getSwitch("kill") || mCommand->getSwitch("create-config"));
		LOG_SILENT("**** Started Log ****")    
		
		mConfig = new Config(cfgfile);

	/* Initialise the clock and the timer */
		LOG_DEBUG("Startup: Creating Clock and Timer")
		mClock = new Clock();
		mTimer = new CommonTimer(mClock);   

	}

	ServerCore::~ServerCore()
	{
		LOG_DEBUG("Shutdown: Quitting after " + StringUtils::toString(mTimer->getTime()/60) + " minutes")

	/* Send closing packet and delete the connection */
		if (mConnection && mConnection->isBound())
		{
			LOG_DEBUG("Shutdown: Closing connection")
			OutStream out;
			out << ID_SERVER_CLOSING;
			forceBroadcast(out);
			delete mConnection;
		}
		else if (mConnection)
		{
			delete mConnection;
		}

	/* Deleting all Users */
		LOG_DEBUG("Shutdown: Cleaning up users")
		for (UserMap::iterator it = mUsers.begin(); it != mUsers.end(); ++it)
			delete it->second;

	/* Delete the stuff */
		LOG_DEBUG("Shutdown: Cleaning up member classes")
		delete mConfig;
		delete mCommand;
		delete mTimer;
		delete mClock;
		LOG_SILENT("*** Closed Log ***")
		delete Log::getSingleton();
	}

	void ServerCore::start()
	{
#ifdef OS_PLATFORM_LINUX
	/* Check if we should kill an older instance */
		if (mCommand->getSwitch("kill"))
		{
			std::string pidfile, tmp;
			if (!mCommand->getStringValue("pid").empty())
				pidfile = mCommand->getStringValue("pid");
			else
				pidfile = "/tmp/openspeak_server.pid";			
			std::ifstream file(pidfile.c_str());
			if (file.fail())
				EXCEPTION("Couldn't read from pid file " + pidfile)
			char c;
			while (file.get(c))
				tmp += c;
			if (tmp.empty())
				EXCEPTION("Couldn't read from pid file " + pidfile)
			file.close();
			int res = kill(StringUtils::toInt(tmp), SIGTERM);
			if (res == -1)		
				LOG_ERROR("Couldn't kill the process. The process might not exist or your permissions are not sufficient")
			else
				std::cerr << "Successfully killed the process.\n";
			return;
		}
#endif
		
	/* Parse the config */
		try
		{
			LOG_DEBUG("Startup: Parsing the config file")
			bool checkcfg = false;
			if (!mConfig->parse())
			{
				LOG_DEBUG("Startup: No config file found. Got created")
				checkcfg = true;
			}
			else if (!mConfig->checkOptions("password", "port", "maxusers", "quality", ""))
			{
				LOG_DEBUG("Startup: Variable missing in config file. Adding")
				checkcfg = true;
			}

			if (checkcfg)
			{
			/* If it returns false, a new file was created and we can enter some basic information */
				CHECK_OPTION("password", "")
				CHECK_OPTION("port", "23042")
				CHECK_OPTION("maxusers", "8")
				CHECK_OPTION("quality", "1")
			}
			
			if (mCommand->getSwitch("create-config"))
			{
				if (checkcfg)
					std::cerr << "Created configfile.\n";
				else
					std::cerr << "Configfile already exists.\n";
				return;
			}


	/* Start the connection */
			LOG_DEBUG("Startup: Creating connection")
			mConnection = new UDPConnection(false);
			mConnection->addListener(this);    

	/* Try to bind the server to a nice port */
			LOG_DEBUG("Startup: Bound connection to port " + mConfig->getOption("port"))
			mConnection->bind( StringUtils::toUShort(mConfig->getOption("port").c_str()) );
			
	/* Initialize the callbacks */
			Callbacks::initCallbacks(mConnection);    

	/* Initialize some vars */
			mMaxUsers = StringUtils::toUShort(getOption("maxusers").c_str());
			mCurrentUsers = 0;    
		}
		catch (openSpeak::Exception &e)
		{
			LOG_FATAL("Exception thrown: " + e.getMessage())
			return;
		}

	/* Fork the server if the user wants it to */
#ifdef OS_PLATFORM_LINUX
		pid_t pid = 0;
		if (mCommand->getSwitch("daemon"))
		{
			std::cerr << "Starting openSpeak in daemon mode.\nTo stop the server, kill it\n";
			pid = fork();
			if (pid < 0)
			{
				std::cerr << "Forking the server failed!\n";
				exit(-1);	
			}	
		}	
		if (pid == 0)
		{	
#endif

			_loop();
			
#ifdef OS_PLATFORM_LINUX
		}
		else
		{
			std::string pidfile;
			if (!mCommand->getStringValue("pid").empty())
			{
				pidfile = mCommand->getStringValue("pid");
			}
			else
			{
				pidfile = "/tmp/openspeak_server.pid";    		
			}
			std::ofstream file(pidfile.c_str(), std::ios::trunc);
			file << pid;
			if (file.fail())
				EXCEPTION("Writing the pid to " + pidfile + " failed!")
			file.close();
		}
#endif

	/* Send closing packet and close the connection when we're finished */
		LOG_DEBUG("Core: Closing Connection")
		OutStream out;
		out << ID_SERVER_CLOSING;
		forceBroadcast(out);
		mConnection->close();
	}

	ushort ServerCore::addUser(const sockaddr_in &sock, const std::string &name)
	{
	/* Check if there is a place on the server */
		if (mCurrentUsers >= mMaxUsers)
			return SERVER_FULL;

	/* Check if the sockaddr or the name already exist */
		if (!mUsers.empty())
		{
			if (getUser(sock) != 0)
				return ALREADY_REGISTERED;
			for (UserMap::const_iterator it = mUsers.begin(); it != mUsers.end(); ++it)
			{
			/* Check for the name */
				if (it->second->name == name)
					return NICK_IN_USE;
			}
		}
		
	/* Create a User and insert it into the map */
		User *tmp = new User();
		tmp->name = name;
		tmp->id = ++mID;
		mUsers.insert(std::make_pair(sock, tmp));
		updateUser(tmp);
		++mCurrentUsers;
		return OKAY;
	}

	void ServerCore::removeUser(const sockaddr_in &sock)
	{
	/* Search for the User and remove him */
		UserMap::iterator it = mUsers.find(sock);
		if (it != mUsers.end())
		{
			LOG_DEBUG("Core: Deleting user " + it->second->name)
			delete it->second;
			mUsers.erase(it);
			--mCurrentUsers;
		}
	}

	User* ServerCore::getUser(const sockaddr_in &sock)
	{
	/* Search for the User and return a pointer to it or null */
		UserMap::const_iterator it = mUsers.find(sock);
		if (it != mUsers.end())
			return it->second;
		else
			return 0;
	}

	User* ServerCore::getUser(const ushort &id)
	{
		UserMap::const_iterator it;
		for (it = mUsers.begin(); it != mUsers.end(); ++it)
		{
			if (it->second->id == id)
				return it->second;
		}
		return 0;
	}

	User* ServerCore::getUser(const std::string &name)
	{
		UserMap::const_iterator it;
		for (it = mUsers.begin(); it != mUsers.end(); ++it)
		{
			if (it->second->name == name)
				return it->second;
		}
		return 0;
	}

	sockaddr_in ServerCore::getUserSocket(const User *User)
	{
		UserMap::const_iterator it;
		for (it = mUsers.begin(); it != mUsers.end(); ++it)
		{
			if (it->second == User)
				return it->first;
		}
		return sockaddr_in();
	}

	sockaddr_in ServerCore::getUserSocket(const ushort &id)
	{
		UserMap::const_iterator it;
		for (it = mUsers.begin(); it != mUsers.end(); ++it)
		{
			if (it->second->id == id)
				return it->first;
		}
		return sockaddr_in();
	}

	sockaddr_in ServerCore::getUserSocket(const std::string &name)
	{
		UserMap::const_iterator it;
		for (it = mUsers.begin(); it != mUsers.end(); ++it)
		{
			if (it->second->name == name)
				return it->first;
		}
		return sockaddr_in();
	}

	void ServerCore::updateUser(const sockaddr_in &sock)
	{
		UserMap::iterator it = mUsers.find(sock);
		if (it != mUsers.end())
			it->second->lastMsg = mTimer->getTime();

	}

	void ServerCore::updateUser(User* usr)
	{
		if (usr)
			usr->lastMsg = mTimer->getTime();
	}

	void ServerCore::broadcast(const std::string &msg, const sockaddr_in &sock)
	{
		UserMap::const_iterator it;
		for (it = mUsers.begin(); it != mUsers.end(); ++it)
		{
			if (!((it->first.sin_addr.s_addr == sock.sin_addr.s_addr) && (it->first.sin_port == sock.sin_port)) && it->second->finished)
				mConnection->send(msg, it->first);
		}
	}

	void ServerCore::forceBroadcast(const std::string &msg, const sockaddr_in &sock)
	{
		UserMap::const_iterator it;
		for (it = mUsers.begin(); it != mUsers.end(); ++it)
		{
			if (!((it->first.sin_addr.s_addr == sock.sin_addr.s_addr) && (it->first.sin_port == sock.sin_port)) && it->second->finished)
				mConnection->forceSend(msg, it->first);
		}
	}

	void ServerCore::userFinished(const sockaddr_in &sock)
	{
	/* Check if the User exists and save the struct */
		User* usr = getUser(sock);
		if (!usr)
			return;

		OutStream out;
	/* Send all users to this user */
		out << ID_NUMBER_OF_USERS << (mUsers.size() - 1);
		mConnection->send(out, sock);
		UserMap::const_iterator it;
		for (it = mUsers.begin(); it != mUsers.end(); ++it)
		{
			out.reset();
			if (it->second != usr && it->second->finished)
			{
				out << ID_USER << it->second->id << it->second->name;
				mConnection->send(out, sock);
			}
		}

	/* Submit server settings (only audio quality atm) */
		out.reset();
		out << ID_SERVERSETTING << QUALITY;
		if (getOption("quality").empty())
			out << "0";
		else
			out << getOption("quality");
		mConnection->send(out, sock);

	/* Finished the User */
		usr->finished = true;
		updateUser(usr);
		out.reset();
		out << ID_FINISHED_CONNECTING << usr->id;
		mConnection->send(out, sock);
		LOG_USER("User " + usr->name + " connected")

	/* Tell others that we got a new user */
		out.reset();
		out << ID_NEW_USER << usr->id << usr->name;
		broadcast(out, sock);
		LOG_DEBUG("Core: User " + usr->name + " finished connecting")
	}

	std::string ServerCore::getOption(const std::string &option) const
	{
		if (mCommand->getStringValue(option) != "")
			return mCommand->getStringValue(option);
		else
			return mConfig->getOption(option);
	}

	void ServerCore::onPacket(Packet* pkt)
	{
		User* usr = getUser(pkt->sender);
		if (usr && usr->finished)
			updateUser(usr);

	}

	void ServerCore::_loop()
	{
		LOG_DEBUG("Startup: Entering mainloop")
		while (!mQuit)
		{
#ifdef OS_PLATFORM_WIN32
			Sleep(1);
#else
			usleep(1);
#endif

	/* Update the connection and parse packets */
			try
			{
				mClock->makeStep();
				if ((mTimer->getTime() - mLastCheck >= 5.0))
				{
					_checkUsers();
					mLastCheck = mTimer->getTime();
				}
				while(mConnection->select())
					mConnection->process();
			}
			catch (openSpeak::Exception &e)
			{
				LOG_ERROR("Exception thrown: " + e.getMessage())
			}
		}	
	}

	void ServerCore::_checkUsers()
	{
		UserMap::const_iterator it;
		for (it = mUsers.begin(); it != mUsers.end(); ++it)
		{
			if (it->second->lastMsg == 0.0)
			{
				if (it->second->finished)
					updateUser(it->second);
			}
			else if ((mTimer->getTime() - it->second->lastMsg) >= 7.5)
			{
				LOG_DEBUG("Core: Sending disconnect packets for removed user " + it->second->name)
				OutStream out;
				out << ID_DISCONNECTING_USER << it->second->id;
				broadcast(out);
				out.reset();
				out << ID_ERROR << GOT_DISCONNECTED;
				mConnection->forceSend(out, it->first);
				LOG_USER("Removed user " + it->second->name)
				removeUser(it->first);            
			}
		}
	}

