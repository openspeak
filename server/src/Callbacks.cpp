/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"

namespace Callbacks
{
    using namespace openSpeak;

    void initCallbacks(UDPConnection *udp)
    {
    /* Add all callback functions */
        udp->addCallback(ID_CHALLENGE, &onChallenge);
        udp->addCallback(ID_REGISTER, &onRegister);
        udp->addCallback(ID_VERSION, &onVersion);
        udp->addCallback(ID_SOUND, &onSound);
		udp->addCallback(ID_STOP_SOUND, &onStopSound);
        udp->addCallback(ID_PASSWORD, &onPassword);
        udp->addCallback(ID_DISCONNECTING, &onDisconnect);
        udp->addCallback(ID_MESSAGE, &onMessage);
        udp->addCallback(ID_CHANGE, &onChange);
        udp->addCallback(ID_PONG, &onPong);
        udp->addCallback(ID_PING, &onPing);
    }

    void onChallenge(UDPConnection *udp, Packet *pkt)
    {
    /* We got a client and should say welcome to him */
        OutStream out;
        out << ID_WELCOME;
        udp->send(out, pkt->sender);
        onRegister(udp, pkt);
    }

    void onRegister(UDPConnection *udp, Packet *pkt)
    {
        if (pkt->data.empty())
            return;
    /* This client wants to register himself as a valid user */
        LOG_DEBUG("Callback: Got Register packet")
        OutStream out;
		std::vector<std::string> vec = StringUtils::split(pkt->data);

    /* Return a message depending on the existence of the nick or socket */
        ushort nickavail = ServerCore::getSingleton()->addUser(pkt->sender,vec[0]);

        if (nickavail == OKAY)
        {
            out << ID_OK << NICK_ACCEPTED;
            udp->send(out, pkt->sender);
        }
        else
        {
            out << ID_ERROR << nickavail;
            udp->send(out, pkt->sender);
            LOG_USER("User "+vec[0]+" got rejected!")
            return;
        }
    /* Check for password and that stuff */
        if (vec.size() > 1)
        {
            pkt->data.erase(pkt->data.begin());
            onPassword(udp, pkt);
        }
        else if (!ServerCore::getSingleton()->getOption("password").empty())
        {
            out.reset();
            out << ID_NEED_PASSWORD;
            udp->send(out, pkt->sender);
            LOG_DEBUG("Callback: User "+vec[0]+" needs password")
        }
        else
        {
            User *usr = ServerCore::getSingleton()->getUser(pkt->sender);
            if (!usr)
                return;
            usr->gotPassword = true;
            ServerCore::getSingleton()->userFinished(pkt->sender);
        }
    }

    void onVersion(UDPConnection *udp, Packet *pkt)
    {
        if (pkt->data.empty())
            return;
        if (StringUtils::toUShort(pkt->data) != OS_PROTOCOL_VERSION)
        {
            OutStream out;
            out << ID_ERROR << VERSION_MISMATCH;
            udp->send(out, pkt->sender);
            LOG_DEBUG("Callback: Version mismatch detected")
        }
    }

    void onSound(UDPConnection  *udp, Packet *pkt)
    {
        if (pkt->data.empty())
            return;
        User *usr = ServerCore::getSingleton()->getUser(pkt->sender);
        if (!usr) return;

        OutStream out;
        out << ID_SOUND << usr->id << pkt->data;
        ServerCore::getSingleton()->broadcast(out, pkt->sender);
    }

    void onStopSound(UDPConnection  *udp, Packet *pkt)
    {
        User *usr = ServerCore::getSingleton()->getUser(pkt->sender);
        if (!usr) return;

        OutStream out;
        out << ID_STOP_SOUND << usr->id;
        ServerCore::getSingleton()->broadcast(out, pkt->sender);
    }

    void onPassword(UDPConnection *udp, Packet *pkt)
    {
        OutStream out;
        if (pkt->data.empty())
        {
            out << ID_ERROR << PASSWORD;
            udp->send(out, pkt->sender);
            return;
        }
    /* Get the User */
        User *usr = ServerCore::getSingleton()->getUser(pkt->sender);

    /* Check if the User exists */
        if (!usr)
        {
            out << ID_ERROR << NOT_REGISTERED;
            udp->send(out, pkt->sender);
            return;
        }

    /* Check if he already got the password or if the entered password is correct */
        else if (!usr->gotPassword )
        {
            Hash hash(ServerCore::getSingleton()->getOption("password"));
            if (pkt->data == hash.getHash())
            {
                usr->gotPassword = true;
                out << ID_OK << PASSWORD;
                udp->send(out, pkt->sender);
                ServerCore::getSingleton()->userFinished(pkt->sender);          
            }
            else
            {
                out << ID_NEED_PASSWORD;
                udp->send(out, pkt->sender);
                LOG_DEBUG("Callback: User " + usr->name + " sent wrong password")
            }
        }
    }

    void onDisconnect(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
    {
        User *usr = ServerCore::getSingleton()->getUser(pkt->sender);
        if (!usr)
            return;
        LOG_USER("User "+usr->name+" disconnected")

    /* Send the disconnect notification to all other Users */
        OutStream out;
        out << ID_DISCONNECTING_USER << usr->id;
        ServerCore::getSingleton()->broadcast(out, pkt->sender);

    /* Delete the User */
        ServerCore::getSingleton()->removeUser(pkt->sender);
    }

    void onMessage(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
    {
        if (pkt->data.empty())
            return;
        User *usr = ServerCore::getSingleton()->getUser(pkt->sender);
        if (!usr)
            return;
        OutStream out;
		std::string::size_type index = pkt->data.find(' ');
        out << ID_MESSAGE << ServerCore::getSingleton()->getUser(pkt->sender)->id << pkt->data.substr(index+1);
        udp->send(out, ServerCore::getSingleton()->getUserSocket(StringUtils::toUShort(pkt->data.substr(0,index))));
    }

    void onChange(UDPConnection *udp, Packet *pkt)
    {
    /* Get some important data and check rights */
		std::vector<std::string> vec = StringUtils::split(pkt->data);
        if (vec.size() < 2)   return;
        ushort type = StringUtils::toUShort(vec[0]);
        User *usr = ServerCore::getSingleton()->getUser(pkt->sender);
        if (!usr) return;
        OutStream out;

        switch (type)
        {
            case NICK:
            {
				if (!ServerCore::getSingleton()->getUser(vec[1]))
                {
					LOG_DEBUG("User "+usr->name+" renamed to "+vec[1])
                    usr->name = vec[1];
                    out << ID_CHANGE << NICK << usr->id << usr->name;
                    ServerCore::getSingleton()->broadcast(out, pkt->sender);
                }
                else
                {
                    out << ID_ERROR << NICK_IN_USE;
                    udp->send(out, pkt->sender);
                }
                break;
            }
            case SERVERSETTING:
                break;
            case STATUS:
                break;
        }
    }

    void onPong(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
    {
    }

    void onPing(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
    {
        std::stringstream tmp;
        tmp << ID_PONG;
        udp->send(tmp.str(), pkt->sender);
    }
}
