/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_CALLBACKS_H__
#define __OS_CALLBACKS_H__

#include "openSpeak.h"

namespace Callbacks
{
	/** \brief Initializes all callbacks in Callbacks.h 
	 *	\param udp The UDPConnection to register with
	 */
    void initCallbacks(openSpeak::UDPConnection *udp);

	/** \brief The callback function for the ID_CHALLENGE packet
	 *	\param udp The UDPConnection on which the packet arrived
	 *	\param pkt The Packet that arrived
	 */
    void onChallenge(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt);

	/** \brief The callback function for the ID_REGISTER packet
	 *	\param udp The UDPConnection on which the packet arrived
	 *	\param pkt The Packet that arrived
	 */
    void onRegister(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt);

	/** \brief The callback function for the ID_VERSION
	 *	\param udp The UDPConnection on which the packet arrived
	 *	\param pkt The Packet that arrived
	 */
    void onVersion(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt);

	/** \brief The callback function for the ID_SOUND packet
	 *	\param udp The UDPConnection on which the packet arrived
	 *	\param pkt The Packet that arrived
	 */
    void onSound(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt);

	/** \brief The callback function for the ID_STOPSOUND packet
	 *	\param udp The UDPConnection on which the packet arrived
	 *	\param pkt The Packet that arrived
	 */
    void onStopSound(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt);

	/** \brief The callback function for the ID_PASSWORD packet
	 *	\param udp The UDPConnection on which the packet arrived
	 *	\param pkt The Packet that arrived
	 */
    void onPassword(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt);

	/** \brief The callback function for the ID_DISCONNECT packet
	 *	\param udp The UDPConnection on which the packet arrived
	 *	\param pkt The Packet that arrived
	 */
    void onDisconnect(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt);

	/** \brief The callback function for the ID_MESSAGE packet
	 *	\param udp The UDPConnection on which the packet arrived
	 *	\param pkt The Packet that arrived
	 */
    void onMessage(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt);

	/** \brief The callback function for the ID_CHANGE packet
	 *	\param udp The UDPConnection on which the packet arrived
	 *	\param pkt The Packet that arrived
	 */
    void onChange(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt);

	/** \brief The callback function for the ID_PONG packet
	 *	\param udp The UDPConnection on which the packet arrived
	 *	\param pkt The Packet that arrived
	 */
    void onPong(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt);

	/** \brief The callback function for the ID_PING packet
	 *	\param udp The UDPConnection on which the packet arrived
	 *	\param pkt The Packet that arrived
	 */
    void onPing(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt);
}

#endif
