/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __SERVER_CORE_H__
#define __SERVER_CORE_H__

#include "openSpeak.h"

	/** \struct User
	 *	\brief A user connected to the server
	 *
	 *	Each user connected to the server is represented by a User struct.
	 *	It contains all important informations needed for each user
	 */
	struct User
	{
		ushort id;			/**< The unique ID of the user */
		std::string name;	/**< The name of the user */		
		bool gotPassword;	/**< True if the user got the correct password */
		bool finished;		/**< True if the user has finished connecting */
		double lastMsg;		/**< Time when we got the last packet from the user */

		/** \brief Empty constructor to initialize the User struct */
		User(void) { gotPassword = false; finished = false; id = 0; name = ""; lastMsg = 0.0; }
	};

	/** \brief The map we're saving the users in */
	typedef std::map< sockaddr_in, User*, openSpeak::compSockaddr > UserMap;

	#ifdef OS_PLATFORM_LINUX
	/** \brief Handler for SIGTERM events on Linux
	 * 	\param code The code that gets handled (should be SIGTERM)
	 */
	void SigHandler(int code);
	#endif

	/** \class ServerCore
	 *	\brief The core class of the openSpeak server.
	 *
	 *	The ServerCore is the main class of the openSpeak server and is used to control all users on the server
	 *	and broadcast messages.
	 */
	class ServerCore : public openSpeak::Listener, public openSpeak::Singleton<ServerCore>
	{
	  public:
		/** \brief The constructor of the ServerCore class
		 *	\param argc The amount of commandline arguments
		 *	\param argv The commandline arguments
		 */
		ServerCore(int argc, char** argv);

		/** \brief The destructor of the ServerCore class */
		~ServerCore(void);

		/** \brief Start the server */
		void start(void);

		/** \brief Stop the server */
		inline void stop(void) { mQuit = true; }

		/** \brief Add a User to the server
		 *	\param sock The socket of the user
		 *	\param name The user's name
		 *	\return The ID of the newly created user
		 */
		ushort addUser(const sockaddr_in &sock, const std::string &name);

		/** \brief Remove a User from the server
		 *	\param sock The socket of the user we should remove
		 */
		void removeUser(const sockaddr_in &sock);

		/** \brief Get a User from the server
		 *	\param sock The socket of the user
		 *	\return The User we wanted or 0 if there is no matching one
		 */
		User* getUser(const sockaddr_in &sock);

		/** \brief Get a User from the server
		 *	\param id The ID of the user
		 *	\return The User we wanted or 0 if there is no matching one
		 */
		User* getUser(const ushort &id);

		/** \brief Get a User from the server
		 *	\param name The name of the user
		 *	\return The User we wanted or 0 if there is no matching one
		 */
		User* getUser(const std::string &name);

		/** \brief Get the socket of a User from the server
		 *	\param usr The User we want the socket from
		 *	\return The socket of the user or an empty struct
		 */
		sockaddr_in getUserSocket(const User *usr);

		/** \brief Get the socket of a User from the server
		 *	\param id The ID of the user we want the socket from
		 *	\return The socket of the user or an empty struct
		 */
		sockaddr_in getUserSocket(const ushort &id);

		/** \brief Get the socket of a User from the server
		 *	\param name The name of the user we want the socket from
		 *	\return The socket of the user or an empty struct
		 */
		sockaddr_in getUserSocket(const std::string &name);

		/** \brief Set the time of the last message to the current time for a given User
		 *  \param sock The socket of the user we want to update
		 */
		void updateUser(const sockaddr_in &sock);

		/** \brief Set the time of the last message to the current time for a give User
		 *	\param usr The user we want to update
		 */
		void updateUser(User* usr);

		/** \brief Broadcast a message to all users exept the socket given
		 * 	\param msg The message to send
		 * 	\param sock The socket to exclude
		 */
		void broadcast(const std::string &msg, const sockaddr_in &sock = sockaddr_in());
		
		/** \brief Broadcast a message to all users exept the socket given
		 * 	\param out The OutStream to send
		 * 	\param sock The socket to exclude
		 */
		void broadcast(openSpeak::OutStream out, const sockaddr_in &sock = sockaddr_in()) { broadcast(out.asString(), sock); }
		
		/** \brief Force the broadcast of a message to all users exept the socket given
		 * 	\param msg The message to send
		 * 	\param sock The socket to exclude
		 */
		void forceBroadcast(const std::string &msg, const sockaddr_in &sock = sockaddr_in());
		
		/** \brief Force the broadcast of a message to all users exept the socket given
		 * 	\param out The OutStream to send
		 * 	\param sock The socket to exclude
		 */
		void forceBroadcast(openSpeak::OutStream out, const sockaddr_in &sock = sockaddr_in()) { forceBroadcast(out.asString(), sock); }

		/** \brief Finish the connection attempt of a user
		 *	\param sock The socket of the user to identify him
		 */ 
		void userFinished(const sockaddr_in &sock);

		/** \brief Get an option from either the config or the commandline
		 * 	\param option The option to get
		 */
		std::string getOption(const std::string &option) const;
		
		/** \brief Set an option in the config file
		 * 	\param option The option to set
		 * 	\param value The new value of the option
		 */
		inline void setOption(const std::string &option, const std::string &value) { mConfig->setOption(option,value); }

		virtual void onPacket(openSpeak::Packet *pkt);

	  protected:
		/** \brief The loop in which all actions take place */
		void _loop(void);
	  
		/** \brief Check all users if they're still connected */
		void _checkUsers(void);

		openSpeak::UDPConnection *mConnection;	/**< The connection we're using to communicate */

		openSpeak::Config *mConfig;				/**< The config file we're using */

		openSpeak::CmdLineParser *mCommand;		/**< The commandline parser to get commandline options from */

		UserMap mUsers;							/**< A vector to save all users on the server */

		openSpeak::Clock *mClock;				/**< The clock for the correct time */
		openSpeak::CommonTimer *mTimer;			/**< The main timer which uses the clock */
		
		double mLastCheck;						/**< The last time we called checkUsers() to see if all users are still connected */

		ushort mMaxUsers;						/**< The maximum amount of Users on the server */
		ushort mCurrentUsers;					/**< The current amount of Users on the server */
		ushort mID;								/**< The currently highest given ID */

		bool mQuit;								/**< Are we going to quit */
	};

#endif

