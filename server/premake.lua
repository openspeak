--  openSpeak - The open source VoIP application
--  Copyright (C) 2006 - 2007  openSpeak Team (http://sourceforge.net/projects/openspeak/)
--
--  This program is free software; you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation; either version 2 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License along
--  with this program; if not, write to the Free Software Foundation, Inc.,
--  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
--

package = newpackage()
package.name = "openSpeak Server"
package.target = "openspeak_server"
package.kind = "exe"
package.language = "c++"
package.files = {
	matchrecursive("include/*.h", "src/*.cpp")
}

-- For Linux
if (linux) then

	package.includepaths = { "../lib/include", "include", "/usr/local/include", "/usr/include" }
	package.libpaths = { "../libs" }
	package.config["Debug"].links = { "openspeak_d" }
	package.config["Release"].links = { "openspeak" }

-- For Windows
elseif (windows) then

	package.includepaths = { "include", "../lib/include" }
	package.libpaths = { "../libs" }
	package.config["Debug"].links = { "openspeak_d", "ws2_32" }
	package.config["Release"].links = { "openspeak", "ws2_32" }

end

package.config["Debug"].buildflags = { "extra-warnings" }
package.config["Release"].buildflags = { "no-symbols", "optimize-speed", "no-frame-pointer" }	

package.config["Debug"].objdir = "../obj/server/debug"
package.config["Release"].objdir = "../obj/server"
