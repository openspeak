/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_USER_MGR_H__
#define __OS_USER_MGR_H__

#include "openSpeak.h"


/** \struct User
 * 	\brief Struct to save all informations of a user on the server
 * 
 * 	This struct is used to save all informations one might need of a 
 * 	specific user on the server
 */
struct User
{
    std::string name;
    ushort id;
};

/** \brief A map to save all users with their respective ids */
typedef std::map< uint, User* > UserMap;

/** \class UserMgr
 * 	\brief Singleton class to deal with the users on the server
 * 
 * 	The UserMgr class is used to add or remove users which
 * 	join/disconnect from the server. It's also used to change specific
 * 	properties of already connected users.
 */
class UserMgr : public openSpeak::Singleton<UserMgr>
{
  public:
	/** \brief The constructor of the UserMgr class */
    UserMgr(void);
    
    /** \brief The destructor of the UserMgr class */
    ~UserMgr(void);

	/** \brief Add a user with id and name to the manage
	 * 	\param id The ID of the user
	 * 	\param name The name of the user
	 */
    void addUser(const uint &id, const std::string &name);
    
    /** \brief Remove the user with the specified id from the manager
     * 	\param id The ID of the user
     */
    void removeUser(const uint &id);
    
    /** \brief Change the name of a saved user
     * 	\param id The ID of the user
     * 	\param name The new name of the user
     */
    void changeUser(const uint &id, const std::string &name);
    
    /** \brief Change the name of a saved user
     * 	\param name The old name of the user
     * 	\param newname The new name of the user
     */
    inline void changeUser(const std::string &name, const std::string &newname) { changeUser(getUserID(name), newname); }
    
    /** \brief Get a pointer to the user struct of a specific user
     * 	\param id The ID of the user
     *	\return The User struct of the user
     */
    User* getUser(const uint &id);
    
    /** \brief Get a pointer to the user struct of a specific user
     * 	\param name The name of the user
     *	\return The User struct of the user
     */
    User* getUser(const std::string &name);
    
    /** \brief Get the ID of a specific user
     * 	\param name The name of the user
     *	\return The ID of the user
     */
    ushort getUserID(const std::string &name);

 protected:
    UserMap                     mUsers;		/**< The saved users on the server */
};

#endif
