/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */
#ifndef __OS_CLIENT_APPLICATION_H__
#define __OS_CLIENT_APPLICATION_H__

#include "openSpeak.h"

/** \class ClientApplication
 * 	\brief The main class of the client
 * 	
 * 	The ClientApplication class is the main class of the client and 
 * 	serves as an entry point for wxWidgets. It's responsible for the 
 * 	startup and quitting of the application.
 */
class ClientApplication : public wxApp
{
public:
	/** \brief The virtual destructor of the ClientApplication class */
    virtual ~ClientApplication();

	/** \brief Overloaded virtual initialization method which gets called by wxWidgets when starting up */
    virtual bool OnInit();
    
    /** \brief Overloaded virtual exit method which gets called by wxWidgets when shutting down */
    virtual int OnExit();

	/** \brief Function to get the MainWindow of the application
	 * 	\return Pointer to the MainWindow
	 */
    inline GUI::MainWindow* getWindow(void) { return mWindow; }

	/** \brief Get the value of an option from either the config or the commandline (commandline > config)
	 * 	\param option The option to get the value from
	 * 	\return The value of the option
	 */
	std::string getOption(const std::string &option);
	
	/** \brief Set the value of an option in the config
	 * 	\param option The option to set
	 * 	\param value The value of the option
	 */
	inline void setOption(const std::string &option, const std::string &value) { mConfig->setOption(option, value); }
	
	/** \brief Check if the specifed option exists in the config file
	 * 	\param option The option to check
	 * 	\return True if the option exists
	 */
	inline bool optionExists(const std::string &option) { return mConfig->optionExists(option); }

	/** \brief Get the path in which the config is saved
	 * 	\return The path as a string
	 */
	std::string getPath(void) { return mPath; }

protected:
    GUI::MainWindow                 *mWindow;	/**< The MainWindow of the application */
    Thread                          *mThread;	/**< The Thread which handles the connection and the audio i/o */
	openSpeak::Config               *mConfig;	/**< The Config we're reading and saving our options from/to */
	openSpeak::CmdLineParser        *mCommand;	/**< The parser of the commandline */

	std::string                     mPath;		/**< The path in which the config is located */
};

/** brief Declare the application to be able to call wxGetApp() */
DECLARE_APP(ClientApplication);

#endif
