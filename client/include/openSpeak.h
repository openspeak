/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OPEN_SPEAK_H__
#define __OPEN_SPEAK_H__

/** STL includes */
#include <string>
#include <vector>
#include <map>

/** Dependecies */
#include <wx/wx.h>
#include <wx/treectrl.h>
#include <speex/speex.h>
#include <speex/speex_preprocess.h>
#include <portaudio.h>
#include <osLib.h>

/** Classes of the client */
class ClientCore;
class ClientApplication;
class UserMgr;
class FavoritesMgr;
class Thread;

class SpeexWrapper;
class AudioWrapper;
class AudioMixer;

/** GUI classes of the client */
namespace GUI
{
/* All GUI classes */
	class MainWindow;
	class FavoritesWindow;
	class ChatWindow;
	class EditFavoritesDialog;
	class ConnectDialog;
	class SettingsDialog;
	class PasswordDialog;
}

/** Including all headers of the client */
#include "speex/SpeexWrapper.h"
#include "audio/PortAudioWrapper.h"
#include "audio/AudioMixer.h"
#include "gui/Helper.h"

#include "ClientApplication.h"
#include "Callbacks.h"
#include "UserMgr.h"
#include "FavoritesMgr.h"
#include "Thread.h"

#include "gui/Events.h"
#include "gui/KeyListener.h"
#include "gui/MainWindow.h"
#include "gui/FavoritesWindow.h"
#include "gui/ChatWindow.h"
#include "gui/EditFavoritesDialog.h"
#include "gui/ConnectDialog.h"
#include "gui/SettingsDialog.h"
#include "gui/PasswordDialog.h"

#endif
