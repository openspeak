/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */
  
#ifndef __OS_FAVORITES_MGR_H__
#define __OS_FAVORITES_MGR_H__

#include "openSpeak.h"

/** \struct Favorite
 * 	\brief Little struct to combine all informations needed for a favorite server
 * 
 * 	The Favorite struct is used to save	all required informations 
 * 	for a favorite server.
 */
struct Favorite
{
    std::string name;		/**< The name of the favorite */
    std::string server;		/**< The serverip/url of the favorite */
    std::string password;	/**< The password of the server */
    ushort port;			/**< The port of the server */
    ushort id;				/**< A unique ID for the favorite */
    
    Favorite(void) : port(0), id(0) { }
};

/** \brief A vector to save all favorites for faster access */
typedef std::vector<Favorite*> FavoritesVector;

/** \class FavoritesMgr
 * 	\brief A class to control the favorites xml file and add/remove/change favorites
 * 
 * 	The FavoritesMgr class is used to read all existing favorites from a xml file and
 * 	provide an easy interface to add/remove/change favorites in this file.
 */
class FavoritesMgr
{
 public:
 	/** \brief The constructor of the FavoritesMgr class */
 	FavoritesMgr(void);
 	
 	/** \brief The destructor of the FavoritesMgr class */
 	~FavoritesMgr(void);
 	
 	/** \brief Initialization function which loads the xml file and parses it */
 	void initialize();
 	
 	/** \brief Get all favorites currently in the file
 	 * 	\return A std::vector with all favorites
 	 */
 	FavoritesVector getFavorites(void) { return mFavorites; }
 	
 	/** \brief Get a specific favorite (defined by the id)
 	 * 	\param id The id of the specific favorite
 	 * 	\return A pointer to the favorite
 	 */
 	Favorite* getFavorite(const ushort &id);	
 	
 	/** \brief Add a favorite to the file
 	 * 	\param fav The favorite to add
 	 */
 	void addFavorite(Favorite *fav);
 	
 	/** \brief Add a favorite to the file
 	 * 	\param name The name of the favorite
 	 * 	\param server The server of the favorite
 	 * 	\param port The port of the server
 	 * 	\param password The password of the server
 	 */
 	void addFavorite(const std::string &name, const std::string &server, const ushort &port, const std::string &password = "");
 	
 	/** \brief Remove a favorite from the file
 	 * 	\param id The ID of the favorite to remove
 	 */
 	void removeFavorite(const ushort &id);
 	
 	/** \brief Remove a favorite from the file
 	 * 	\param fav A pointer to the favorite to remove
 	 */
 	void removeFavorite(Favorite *fav);
 	
 	/** \brief Replace a favorite through a new one
 	 * 	\param fav The new favorite
 	 */
 	void setFavorite(Favorite *fav);
 	
 	/** \brief Save everything back to the file
 	 * 	\return True if successfull
 	 */
 	bool save(void);
 	
 protected:
 	/** \brief This function creates an empty xml file */
 	void _createFile(void); 
 
 	FavoritesVector	mFavorites;	/**< The vector with all favorites */
 
 	TiXmlDocument 	*mDoc;		/**< The xml favorites document */
 	std::string 	mFile;		/**< The name of the xml file */
};

#endif
