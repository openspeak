/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_THREAD_H__
#define __OS_THREAD_H__

#include "openSpeak.h"


/** \enum State
 * 	\brief The state of connection we're currently in
 * 
 * 	The State enum defines the current states the connection might have.
 * 	While connecting to a server, the state should change from DISCONNECTED
 * 	to full CONNECTED.
 */
enum State
{
	DISCONNECTED = 0,
	STARTING,
	CHALLENGING,
	REGING,					// formerly known as REGISTERING
	REGED,					// formerly known as REGISTERED
	NEED_PASSWORD,
	AUTHENTICATED,
	GETTING_USERS,
	CONNECTED
};


/** \class Thread
 * 	\brief A seperate Thread to deal with the connection and the audio i/o
 * 
 * 	The Thread class opens a seperate thread to deal with the connection
 * 	to the server and the audio input/output as well as the
 * 	encoding/decoding of the incoming or outgoing voice streams.
 */
class Thread : public wxThread, public openSpeak::Listener, public openSpeak::Singleton<Thread>
{
 public:
 	/** \brief The constructor of the Thread class */ 	 
    Thread(void);
    
    /** \brief The destructor of the Thread class */
    ~Thread(void);

	/** \brief Virtual entry point function which gets called by wxw when running the thread */
    virtual void* Entry(void);
    
    /** \brief Function used to start the Thread (aka go into Entry()) */
    inline void start(void) { Run(); }
    
    /** \brief Stop the loop in Entry() */
    inline void stop(void) { wxMutexLocker lock(*mMutex); mRun = false; }

	///// NEVER CALL THESE FUNCTIONS FROM ANOTHER THREAD /////
	/** \brief Initialize the audio stuff with the given mode (0,1,2)
	 * 	\param mode The mode the audio should be started in (defines the quality)
	 */
    void startSound(const int &mode);
    
    /** \brief Close all audio devices and the speex en-/decoder */
    void stopSound(void);    
    ///// UNTIL HERE /////

	/** \brief Check if the voice activation (VAD) is enabled
	 * 	\return True if the voice activation is enabled
	 */
    bool isVADEnabled(void) { wxMutexLocker lock(*mMutex); return mVAD; }
    
    /** \brief Enable/Disable the voice activation 
     * 	\param yes True to enable, false to disable the voice activation
     */
    void enableVAD(bool yes = true);

	/** \brief Specify if the key to activate speaking is pressed or not
	 * 	\param yes True if pressed, false if released
	 */
    void keyPressed(bool yes = true) { wxMutexLocker lock(*mMutex); mKeyPressed = yes; }

	/** \brief Connect to the specified server
	 * 	\param server The server to connect to
	 * 	\param port The port on the server to connect to
	 * 	\param nick The nick to use
	 * 	\param password Specify a password if one is needed
	 */
    void connect(const std::string &server, const ushort &port, const std::string &nick, const std::string &password = "");
    
    /** \brief Disconnect from the server */
    void disconnect(void);
    
    /** \brief Check if the Thread is still connected to the server
     * 	\return True if it is connected
     */
    bool isConnected(void);

	///// NEVER CALL THIS FUNCTION FROM ANOTHER THREAD /////
	/** \brief Decode an incoming voice stream from the given user
	 * 	\param userId The ID of the speaking user
	 * 	\param dec The voice phrase to decode
	 */
    void decode(unsigned short int userId, char* dec);
    
    /** \brief Decode an incoming voice stream from the given user
	 * 	\param s_userId The ID of the speaking user
	 * 	\param dec The voice phrase to decode
	 */
    void decode(const char* s_userId, const char* dec);    
    ///// UNTIL HERE /////

	/** \brief Send a string to the server
	 * 	\param msg The string to send
	 */
    void send(const std::string &msg);
    
    /** \brief Send an OutStream to the server
     * 	\brief out The OutStream to send
     */
    void send(openSpeak::OutStream out) { send(out.asString()); }

	///// NEVER CALL THIS FUNCTION FROM ANOTHER THREAD /////
	/** \brief Get the current State of the connection
	 * 	\return The State of the connection
	 */
    ushort getState(void);
    
    /** \brief Set the State of the connection
     * 	\param state The new State of the connection
     * 	\see State
     */
    void setState(const ushort &state);
    
    /** \brief Virtual callback function called by the UDPConnection when recieving a packet
     * 	\param pkt The recieved packet
     */
    virtual void onPacket(openSpeak::Packet *pkt);    
    ///// UNTIL HERE /////

 protected:
	/** \brief The loop that gets run when Entry() gets called */
    void _loop(void);
    
    /** \brief Check (aka ping) the server for a timeout */
    void _checkServer(void);

	/** \brief Process incoming sound 
	 * 	\param buffer The incoming sound
	 * 	\param Object The Thread (needed because of static)
	 */
    static void processInputSound(short* buffer, void* Object);

    static wxMutex 				*mMutex;			/**< A mutex to block functions to avoid interthread reading/writing problems */

    openSpeak::UDPConnection    *mConnection;		/**< The connection to the server */
    AudioWrapper                *mAudio;			/**< The audio i/o class */
    SpeexWrapper                *mSpeex;			/**< The speex en-/decoder */
    UserMgr                  	*mUsers;			/**< The UserMgr to handle all users on the server */
    openSpeak::Clock            *mClock;			/**< The main Clock for the Thread */
    openSpeak::CommonTimer      *mTimer;			/**< The connection timer using the Clock */
    
    double                      mLastCheck;			/**< Time of the last call to _checkServer() */
    double						mLastPacket;		/**< Time when the last packet arrived from the server */
    ushort                      mState;				/**< The current State of the connection */
    
    char* 						mEncodedBuffer;		/**< A char* buffer to save encoded frames */

    bool                        mRun;				/**< Defines if the Thread is still running or shutting down */
    bool						mVAD;				/**< Defines if the voice activation is enabled or disabled */
    bool						mKeyPressed;		/**< Defines if the voice activating key is currently pressed */
    bool						mAudioRunning;		/**< Defines if the audio is running atm */
    bool                        mSpeaking;			/**< Defines if someone is speaking atm */
};

/** \brief Define to ease up the usage of the Thread class */
#define THREAD (Thread::getSingleton())

#endif
