/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_PORTAUDIO_WRAPPER_H__
#define __OS_PORTAUDIO_WRAPPER_H__

#include "openSpeak.h"

/** \class AudioWrapper
 *  \brief This is here that sound is sent & fetch from the audio device
 *  The AudioWrapper class interacts with PortAudio, it implements a PortAudio
 *  Callback Funcion and functions to control the audio behavior.
 */
class AudioWrapper
{
public:
    /** \brief The constructor of the AudioWrapper class.
     *  It only initialize a few variables, most of the work will be made at
     *  the Start() function call
     */
	AudioWrapper(void);

    /** \brief The destructor of the AudioWrapper class.
     *  It just stops the sound using the Stop() function
     */
	virtual ~AudioWrapper(void);

    /** \brief Initializes the sound device
     *  \param mode The speex mode we are using. It has no effect on the function, it is used only for debug messages.
     *  \param sampleRate The sample rate we should use, it must be the saple rate that speex uses for the given mode
     *  \param frameSize The frame size we should use, it must be the frame size that speex uses for the given mode
     *  \param ProcessInputCallback The callback function used to process input sound
     *  \param Object The object used by the callback function used to process input sound
     *  Initializes the sound device according to te parameters.
     *  Calls to this function may fail if no audio device is available. Other problems may arise, and may prevent the
     *  opening of the audio device. In such case, an EXCEPTION is raised.
     *  Also, this function starts the AudioMixer.
     */
	virtual bool Start(int mode, int sampleRate, int frameSize, void(*ProcessInputCallback)(short* , void* ), void* Object);

    /** \brief Stops the sound device.
     *  Closes the opened audio streams, free allocated memory, delete the AudioMixer.
     */
	virtual bool Stop(void);

    /** \brief Returns true if the AudioWrapper is running (the Start() function has been called)
     */
	inline bool isRunning(void) { return mRunning; }

    /** \return the AudioMixex object the audiowrapper uses
     */
    inline AudioMixer* getAudioMixer(void) { return mAMixer; }

    /** \brief Used for bloking writing on the audio device.
     *  \param  buffer the buffer to be sent to the audio device
     *  DEPRECIATED
     *  \return true
     */
    bool play(int* buffer);


protected:
    /** \brief Parameters for the PortAudio input stream
     */
	PaStreamParameters mInputParameters;

    /** \brief Parameters for the PortAudio output stream
     */
    PaStreamParameters mOutputParameters;

    /** \brief PortAudio stream strutured passed to initialize the audio device
     */
	PaStream *mStream;

    /** \brief PortAudio error type
     */
	PaError mErr;

    /** \brief The number of samples per seconds we use (relative to speex mode) */
    int mSampleRate;

    /** \brief The number of samples used in a frame (relative to speex mode) */
    int mFramesize;

    /** \brief Contains true if the AudioWapper is started (the Start() function has been called) */
	bool mRunning;

    /** \brief The AudioMixer instance we use */
    AudioMixer *mAMixer;

    /** \brief The number of samples in a frame */
    int mNumSamples;

    /** \brief The number of bytes a frame takes */
    int mNumBytes;

    /** \brief The callback function used to process input sound */
    void(*mProcessInputCallback)(short* buffer, void* Object);

    /** \brief The object used by the callback function used to process input sound */
    void* mCallbackObject;

    /** \brief The PortAudio callback function
     *  \param input and
     *  \param output are arrays of interleaved samples, the format, packing and number of channels used by the buffers are determined by mInputParameters and mOutputParameters
     *  \param frameCount The number of sample frames to be processed by the stream callback. Not used because we're using a unique size for all frames.
     *  \param timeInfo The time in seconds when the first sample of the input buffer was received at the audio input, the time in seconds when the first sample of the output buffer will begin being played at the audio output, and the time in seconds when the stream callback was called. Not used.
     *  \param statusFlags Flags indicating whether input and/or output buffers have been inserted or will be dropped to overcome underflow or overflow conditions. Not used.
     *  \param userData PortAudio gives us back the current AudioMixer object we passed to it in the Start() function
     *  \return always paContinue;
     */    
    static int PACallback(  const void *input,
                            void *output,
                            unsigned long frameCount,
                            const PaStreamCallbackTimeInfo *timeInfo,
                            PaStreamCallbackFlags statusFlags,
                            void *userData);

    /* \brief Initialise PA audio devices 
     */
    void initDevices(void);

};

#endif

