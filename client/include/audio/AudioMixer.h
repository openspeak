/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_AUDIOMIXER_H__
#define __OS_AUDIOMIXER_H__

#include "openSpeak.h"

#define SOUND_LEVEL 2147483647
#define MAX_INT 2147483647

typedef std::map< unsigned short int, int > UserBufferFrame;

/** \class AudioMixer
 *  \brief Mixes many audio streams, normalizes the result, and plays it.
 *  The AudioMixer class has an internal audio buffer where others smaller
 *  audio buffer get mixed.
 *  Once the internal buffer has reached its limit, it plays it until it
 *  gets empty.
 *  Just before a frame is played, it's normalized.
 */
class AudioMixer
{
public :
    /** \brief The constructor of the AudioMixer class
     *  \param numSamples Number of samples per frame
     *  \param frameDuration Duration, in seconds, of a frame
     */
    AudioMixer(int numSamples, double frameDuration);

    /** \brief The destructor of the AudioMixer class
     */
    ~AudioMixer();

    /** \brief Mixes a frame with the internal buffer
     *  \param userId The ID of the user whose this speech frame belongs
     *  \param buffer The buffer to mix with the internal buffer
     */
    int mix(unsigned short int userId, short *buffer);

    /** \brief Notifies the audio mixer that a user stops speaking
     *  \param userId The ID of the user who stops speaking
     */
    void userStopSpeaking(unsigned short int userId);

    /** \brief returns tha audio buffer to be played or 0
     */
    int* getBufferToPlay(void);

    /** \brief Returns true if the thread is playing sound
     */
    bool isPlaying(void);

    /** \brief If the current Frame is not normalize, then do it.
     */
    void normalizeCurrentFrame(void);

private :

    /** \brief Finds the current mixing buffer's frame number (and not it's buffer start!) of a user
     *  \param userId The ID of the user
     */
    int findUserBuff(unsigned short int userId);

    /** \brief Returns a pointer to the buffer start of a frame in the internal buffer
     *  \param frame The frame number we are looking for buffer's start
     */
    int* getFrameBuff(int frame);

    /** \brief Normalizes the buffer's volume
     *  \param buffer The buffer we want to normalize
     *  The buffer must contain "mExtBuffSamples" samples.
     */
    void normalize(int* buffer);

    /** \brief Detects the greatest amplitude sample in the given buffer
     *  \param buffer The buffer we want to analize
     *  The buffer must contain "mExtBuffSamples" samples.
     */
    int detectPeak(int* buffer);

    /** \brief Sets every variables to proper values according BufferLength
     *  \param BufferLength Buffer Length in seconds
     *  Should only be used when buffer is not being played and empty
     */
    void resetBufferProperties(double BufferLenght);

    /** \brief Free memory allocated to the internal buffer and data to control it
     */
    void freeMemory(void);

    /** \brief The duration, in seconds, of a frame
     */
    double mFrameDuration;

    /** \brief The audio wrappe who initialized the mixer */
    AudioWrapper* mAudio;

    /** \brief Length of sound to store before playing, in seconds */
    double mBufferLenght;

    /** \brief Number of samples in an External Buffer */
    int mExtBuffSamples;

    /** \brief We use a long int as internal buffer because short+short may result in a long */
    int *mInternalBuffer;

    /** \brief How many frames do we need for mBufferLenght */
    int mBufferFrames;

    /** \brief How many ExternalBuffer fit in the InternalBuffer */
    int mBufferUsableFrames;

    /** \brief Which Frame of the InternalBuffer a user may use for next samples */
    UserBufferFrame mUserBufferFrame;

    /** \brief Next frame that should be sent to the audio device */
    int mCurrentFrame;

    /** \brief mFrameList[0] points to the fist element of the internal buffer, the last is mFrameList[mBufferFrames] */
    int** mFrameList;

    /** \brief True if the thread sending audio to the AD is running. False if not. */
    bool mRun;

    /** \brief mMixedFrames[int frame] is true if there is sond to be played in the given frame number */
    bool *mMixedFrames;

    /** \brief Last Amplification factor used to normalize output sound */
    int mLastAmp;

    /** \brief mIsNormalized is true if the next stream to be played is normalized */
    bool mIsNormalized;

    /** \brief mBufferUnderflow is true if buffer length was not sufficient for the last played sequence */
    bool mBufferUnderflow;

};

#endif
