/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */
#ifndef __OS_GUI_HELPER_H__
#define __OS_GUI_HELPER_H__

#include "openSpeak.h"

/** \brief Helper function to convert between wxString and std::string
 * 	\param input The wxString to convert
 * 	\param The conversion method to use
 * 	\return The converted std::string
 */
std::string wx2std(const wxString& input, wxMBConv* conv = wxConvCurrent);

/** \brief Helper function to convert between std::string and wxString
 * 	\param input The std::string to convert
 * 	\param conv The conversion method to use
 * 	\return The converted wxString
 */
wxString std2wx(const std::string& input, wxMBConv* conv = wxConvCurrent);


#endif
