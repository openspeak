#ifndef __OS_EVENTS_H__
#define __OS_EVENTS_H__

#include "openSpeak.h"

/** \brief Declare all custom event types */
BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_EVENT_TYPE(EVT_EXCEPTION, 7777)
    DECLARE_EVENT_TYPE(EVT_ADDUSER, 7780)
    DECLARE_EVENT_TYPE(EVT_REMOVEUSER, 7781)
    DECLARE_EVENT_TYPE(EVT_CHANGEUSER, 7782)
    DECLARE_EVENT_TYPE(EVT_MESSAGE, 7790)
    DECLARE_EVENT_TYPE(EVT_CONNECT, 7800)
    DECLARE_EVENT_TYPE(EVT_DISCONNECT, 7801)
END_DECLARE_EVENT_TYPES()

/** \class StringEvent
  *	\brief Event class which provides one string variable
  *
  *	The StringEvent class provides one string variable which can be read by the
  * callback function.
  */
class StringEvent : public wxCommandEvent
{
 public:
	/** \brief The constructor of the StringEvent class
	  *	\param str The initial value of the string variable
	  *	\param evttype The type of the event
	  *	\param id The id of the recieving window/class
	  */
    StringEvent(std::string str, wxEventType evttype = wxEVT_NULL, int id = wxID_ANY ) : wxCommandEvent(evttype, id), mString(str) { }

	/** \brief The virtual destructor of the StringEvent class */
    virtual ~StringEvent(void) { }

	/** \brief Needed clone function */
    virtual wxEvent *Clone() const {return new StringEvent(*this);}

	/** \brief Get the value of the string variable
	  *	\return The value of the string variable
	  */
    inline std::string getString(void) { return mString; }

 protected:
    std::string mString;	/**< The string variable */
};


/** \class StringStringEvent
  *	\brief Event class which provides two string variables
  *
  *	The Event class provides two string variables which can
  *	be read by the recieving window/class
  */
class StringStringEvent : public wxCommandEvent
{
 public:
	/** \brief The constructor of the StringStringEvent class
	  *	\param string1 The initial value of the first variable
	  *	\param string2 The initial value of the second variable
	  *	\param evttype The type of the event
	  *	\param id The id of the recieving window/class
	  */
    StringStringEvent(std::string string1, std::string string2, wxEventType evttype = wxEVT_NULL, int id = wxID_ANY ) : wxCommandEvent(evttype, id), mStringOne(string1), mStringTwo(string2) { }

	/** \brief The virtual destructor of the StringStringEvent class */
    virtual ~StringStringEvent(void) { }

	/** \brief Needed clone function */
    virtual wxEvent *Clone() const {return new StringStringEvent(*this);}

	/** \brief Get the value of the first string variable
	  *	\return The value of the first string variable
	  */
    inline std::string getStringOne(void) { return mStringOne; }

	/** \brief Get the value of the second string variable
	  *	\return The value of the second string variable
	  */
    inline std::string getStringTwo(void) { return mStringTwo; }

 protected:
    std::string mStringOne;		/**< The first string variable */
    std::string mStringTwo;		/**< The second string variable */
};

/** \class MessageEvent
  *	\brief Event class which provides variables to print correct chat messages
  *
  *	The MessageEvent class provides the recieving window/class with all
  *	needed informations to correctly display the chat message.
  */
class MessageEvent : public wxCommandEvent
{
 public:
	/** \brief The constructor of the MessageEvent class
	  *	\param usrid The ID of the sending user
	  *	\param str The chat message
	  *	\param evttype The type of the event
	  *	\param id The id of the recieving window/class
	  */
    MessageEvent(ushort usrid, std::string str, wxEventType evttype = wxEVT_NULL, int id = wxID_ANY ) : wxCommandEvent(evttype, id), mID(usrid), mString(str) { }

	/** \brief The virtual destructor of the MessageEvent class */
    virtual ~MessageEvent(void) { }

	/** \brief Needed clone function */
    virtual wxEvent *Clone() const {return new MessageEvent(*this);}

	/** \brief Get the message
	  *	\return The message
	  */
    inline std::string getString(void) { return mString; }

	/** \brief Get the ID
	  *	\return The ID
	  */
    inline ushort getID(void) { return mID; }

 protected:
    ushort mID;				/**< The ID of the user sending the message */
    std::string mString;	/**< The message */
};

typedef void (wxEvtHandler::*StringEventFunction)(StringEvent&);
typedef void (wxEvtHandler::*StringStringEventFunction)(StringStringEvent&);
typedef void (wxEvtHandler::*MessageEventFunction)(MessageEvent&);

#define EVT_EXCEPTION(fn) \
    DECLARE_EVENT_TABLE_ENTRY( EVT_EXCEPTION, wxID_ANY, wxID_ANY, \
    (wxObjectEventFunction) (wxEventFunction) (wxCommandEventFunction) (wxNotifyEventFunction) \
    wxStaticCastEvent( StringEventFunction, & fn ), (wxObject *) NULL ),

#define EVT_ADDUSER(fn) \
    DECLARE_EVENT_TABLE_ENTRY( EVT_ADDUSER, wxID_ANY, wxID_ANY, \
    (wxObjectEventFunction) (wxEventFunction) (wxCommandEventFunction) (wxNotifyEventFunction) \
    wxStaticCastEvent( StringEventFunction, & fn ), (wxObject *) NULL ),

#define EVT_REMOVEUSER(fn) \
    DECLARE_EVENT_TABLE_ENTRY( EVT_REMOVEUSER, wxID_ANY, wxID_ANY, \
    (wxObjectEventFunction) (wxEventFunction) (wxCommandEventFunction) (wxNotifyEventFunction) \
    wxStaticCastEvent( StringEventFunction, & fn ), (wxObject *) NULL ),

#define EVT_CHANGEUSER(fn) \
    DECLARE_EVENT_TABLE_ENTRY( EVT_CHANGEUSER, wxID_ANY, wxID_ANY, \
    (wxObjectEventFunction) (wxEventFunction) (wxCommandEventFunction) (wxNotifyEventFunction) \
    wxStaticCastEvent( StringStringEventFunction, & fn ), (wxObject *) NULL ),

#define EVT_MESSAGE(fn) \
    DECLARE_EVENT_TABLE_ENTRY( EVT_MESSAGE, wxID_ANY, wxID_ANY, \
    (wxObjectEventFunction) (wxEventFunction) (wxCommandEventFunction) (wxNotifyEventFunction) \
    wxStaticCastEvent( MessageEventFunction, & fn ), (wxObject *) NULL ),

#define EVT_CONNECT(fn) \
    DECLARE_EVENT_TABLE_ENTRY( EVT_CONNECT, wxID_ANY, wxID_ANY, \
    (wxObjectEventFunction) (wxEventFunction) (wxCommandEventFunction) (wxNotifyEventFunction) \
    wxStaticCastEvent( StringEventFunction, & fn ), (wxObject *) NULL ),

#define EVT_DISCONNECT(fn) \
    DECLARE_EVENT_TABLE_ENTRY( EVT_DISCONNECT, wxID_ANY, wxID_ANY, \
    (wxObjectEventFunction) (wxEventFunction) wxStaticCastEvent( wxCommandEventFunction, & fn ), (wxObject *) NULL ),

#endif
