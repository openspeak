/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_GUI_EDIT_FAVORITES_DIALOG_H__
#define __OS_GUI_EDIT_FAVORITES_DIALOG_H__

#include "../openSpeak.h"

namespace GUI
{

/** \class EditFavortiesDialog
  *	\brief Dialog to change or add a favorite server
  *
  *	The EditFavoritesDialog provides a mask to change a favorite server
  *	or add a new one.
  */
class EditFavoritesDialog : public wxDialog
{
 public:
	/** \brief The constructor of the EditFavorites class
	  *	\param edit True if the dialog is used to edit a favorite server
	  *	\param parent The parent FavoritesWindow
	  *	\param fav The favorite to edit or null
	  */
    EditFavoritesDialog(bool edit, FavoritesWindow *parent, Favorite* fav = 0);

	/** \brief The destructor of the EditFavorites class */
    ~EditFavoritesDialog(void);

	/** \brief Callback function for the Ok button */
    void onOK(wxCommandEvent &event);

	/** \brief Callback function for the Cancel button */
    void onCancel(wxCommandEvent &event) { Destroy(); }

 protected:
    DECLARE_EVENT_TABLE()			/**< Declare an event table for wxw */

    bool mEditing;					/**< Defines if we're editing or adding */

    FavoritesWindow *mFavWindow;	/**< The parent window */

    Favorite *mFav;					/**< The favorite we're editing */

    wxTextCtrl *mName;				/**< Text control for the name of the favorite */
	wxTextCtrl *mServer;			/**< Text control for the serverip/url */
	wxTextCtrl *mPort;				/**< Text control for the serverport */
	wxTextCtrl *mPass;				/**< Text control for the serverpassword */

};

}

#endif
