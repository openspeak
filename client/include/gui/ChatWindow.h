/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_GUI_CHAT_WINDOW_H__
#define __OS_GUI_CHAT_WINDOW_H__

#include "openSpeak.h"

namespace GUI
{

/** \class ChatWindow
  *	\brief Displays a chat dialog between two users
  *
  *	The ChatWindow class is used to display a text-based chat dialog
  *	Between two users on the current server. It enables the user to send
  *	and recieve messages.
  */
class ChatWindow : public wxDialog
{
 public:
	/** \brief GUI Events for the ChatWindow class */
    enum ChatEvents
    {
        SEND,
        CLOSE,
        TEXT
    };

 public:
	/** \brief The constructor of the ChatWindow class
	  *	\param parent The main window of the application
	  *	\param id The id of the chat partner
	  *	\param username The name of the chat partner
	  */
    ChatWindow(MainWindow *parent, const ushort &id, const std::string &username);

	/** \brief The destructor of the ChatWindow class */
    ~ChatWindow(void);

	/** \brief Add a message recieved from the chat partner 
	  *	\param msg The message
	  */
    void addMsg(const std::string &msg);

	/** \brief Add chat related texts to the window
	  *	\param text The text to add
	  */
    void addText(const std::string &text);

	/** \brief Disable the send button after a disconnect of one of the users */
    void disable(void);

	/** \brief Callback function for the Send button */
    void onSend(wxCommandEvent &event);

	/** \brief Callback function for the Close button */
    void onCloseBtn(wxCommandEvent &event) { Close(); }

	/** \brief Callback function for close events sent by wxw */
    void onClose(wxCloseEvent &event);

 protected:
	/** \brief Declares an event table for wxw */
    DECLARE_EVENT_TABLE()

    MainWindow *mWindow;	/**< The main window of the application */

    wxTextCtrl *mSend;		/**< The text control for own messages */
	wxTextCtrl *mGet;		/**< The text control for recieved messages */

    wxString mName;			/**< The nickname of the chat partner */

    ushort mID;				/**< The ID of the chat partner */
};

}

#endif
