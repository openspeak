/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_GUI_MAIN_WINDOW_H__
#define __OS_GUI_MAIN_WINDOW_H__

#include "openSpeak.h"

namespace GUI
{

/** \brief A map to save all open ChatWindows with their respective users */
typedef std::map<ushort, ChatWindow*> ChatWindowMap;

/** \class MainWindow
 * 	\brief The main window of the application
 * 
 * 	The MainWindow class is the first thing someone will see when
 *  starting the application.
 * 	It's displaying the server overview and through the menubar
 * 	all possible things someone can do
 */
class MainWindow : public wxFrame, public KeyListener
{
 public:
	
	/** \brief Provides the ID's for all items of the menubar */
	enum Events
	{
        UPDATE = 0,
        CONNECT,
        DISCONNECT,
        FAVORITES,
        SETTINGS,
        CHAT,
        QUIT = wxID_EXIT,
        ABOUT = wxID_ABOUT
	};

	/** The constructor of the MainWindow class
	 * 	\param title The title of the window
	 */
    MainWindow(const wxString &title);
    
    /** \brief The virtual destructor of the MainWindow class */
    virtual ~MainWindow(void);

	/** \brief Callback function for the close event */
    void OnClose(wxCloseEvent &event);
    
    /** \brief Callback function for the Exit menubar item */
	void onBarExit(wxCommandEvent &event);
	
	/** \brief Callback function for the QuickConnect menubar item */
    void onConnect(wxCommandEvent &event);
    
    /** \brief Callback function for the Disconnect menubar item */
    void onDisconnect(wxCommandEvent &event);
    
    /** \brief Callback function for the Settings menubar item */
    void onSettings(wxCommandEvent &event);
    
    /** \brief Callback function for the About menubar item */
    void onAbout(wxCommandEvent &event);
    
    /** \brief Callback function for the Favorites menubar item */
    void onFavorites(wxCommandEvent &event);
    
    /** \brief Callback function for the Chat contextmenu item */
    void onChat(wxCommandEvent &event);
    
    /** \brief Callback function for the contextmenu event */
    void onContextMenu(wxContextMenuEvent &event);
    
    /** \brief Callback function for keydown events*/
    virtual void onKeyDown(wxKeyEvent &event);
    
    /** \brief Callback function for keyup events */
    virtual void onKeyUp(wxKeyEvent &event);

	/** \brief Callback function for the custom exception event */
    void onException(StringEvent &event);
    
    /** \brief Callback function for the custom adduser event */
    void onAddUser(StringEvent &event) { addUser(event.getString()); }
    
    /** \brief Callback function for the custom removeuser event */
    void onRemoveUser(StringEvent &event) { removeUser(event.getString()); }
    
    /** \brief Callback function for the custom changeuser event */
    void onChangeUser(StringStringEvent &event) { changeUser(event.getStringOne(), event.getStringTwo()); }
    
    /** \brief Callback function for the custom message event */
    void onMessage(MessageEvent &event) { addMessage(event.getID(), event.getString()); }
    
    /** \brief Callback function for the custom connect event */
    void onEvtConnect(StringEvent &event) { refreshGUI(); }
    
    /** \brief Callback function for the custom disconnect event */
    void onEvtDisconnect(wxCommandEvent &event);

	/** \brief Add a user to the treectrl
	 * 	\param name The name of the user
	 */
    void addUser(const std::string &name);
    
    /** \brief Remove a user from the treectrl
     * 	\param name The name of the user
     */
    void removeUser(const std::string &name);
    
    /** \brief Change the name of a user in the treectrl
     * 	\param name The old name of the user
     * 	\param newname The new name
     */
    void changeUser(const std::string &name, const std::string &newname);

	/** \brief Add a message to the correct chat window
	 * 	\param id The id of the user sending that message
	 * 	\param msg The message
	 */
    void addMessage(const ushort &id, const std::string &msg);
    
    /** \brief Remove a ChatWindow
     * 	\param win The window to remove
     */
    void removeChat(ChatWindow *win);

	/** \brief Change the title of the treecontrol root
	 * 	\param title The new title of the root
	 */
    void changeRoot(const wxString &title);
    
    /** \brief Refresh the gui depending on the current connection state */
    void refreshGUI(void);

  protected:

	/** \brief Get the ID of a specific item in the treecontrol
	 * 	\param root The root item under which to search
	 * 	\param name The name of the item to search for
	 * 	\return The id of the item
	 */
    wxTreeItemId getItemId(wxTreeItemId root, const wxString &name);

	/** \brief Declare the event table */
    DECLARE_EVENT_TABLE()

    KeyTreeCtrl  *mTree;		/**< The treecontrol used to display everyone on the server */

    wxMenuBar   *mMenuBar;		/**< The menubar on top of the window */
    wxMenu      *mFileMenu;		/**< One of the menus in the menubar */

    ChatWindowMap mChats;		/**< All open ChatWindow's */
};

}

#endif
