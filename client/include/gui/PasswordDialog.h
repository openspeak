/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_GUI_PASSWORD_DIALOG_H__
#define __OS_GUI_PASSWORD_DIALOG_H__

#include "openSpeak.h"

namespace GUI
{


/** \class PasswordDialog
 * 	\brief Little dialog to get a password and send it to the server
 * 	
 * 	The PasswordDialog class is used to display a little dialog
 * 	which enables the user to enter a password needed to gain access
 * 	to the server.
 */
class PasswordDialog : public wxDialog
{
 public:
 	/** \brief The constructor of the PasswordDialog class
 	 * 	\param title The title of the dialog
 	 */
    PasswordDialog(const std::string &title);
    
    /** \brief The destructor of the PasswordDialog class */     
    ~PasswordDialog(void);

	/** \brief Callback function for the OK button */
    void onOK(wxCommandEvent &event);
    
    /** \brief Callback function for the Cancel button */
    void onCancel(wxCommandEvent &event) { Destroy(); }

	/** \brief Callback function for key events */
    void onKey(wxKeyEvent &event);

 protected:
 	/** \brief Declare the event table */
    DECLARE_EVENT_TABLE()

	/** \brief The textcontrol containing the password */
    wxTextCtrl *mPassword;
};

}

#endif
