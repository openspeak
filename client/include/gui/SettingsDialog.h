/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */
#ifndef __OS_GUI_SETTINGS_DIALOG_H__
#define __OS_GUI_SETTINGS_DIALOG_H__

#include "openSpeak.h"

namespace GUI
{

/** \class SettingsDialog
 * 	\brief Dialog to adjust different settings/options
 * 
 * 	The SettingsDialog provides the user with an easy interface
 * 	to change the different options in the config file.
 */
class SettingsDialog : public wxDialog, public KeyListener
{
public:
	/** \brief Const ID for the Catch button */
	static const ushort BUTTONCATCH = 3;

	/** \brief The constructor of the SettingsDialog class */
    SettingsDialog(void);
    
    /** \brief The virtual destructor of the SettingsDialog class */
    virtual ~SettingsDialog(void);

	/** \brief Callback function for the Cancel button */
    void onCancel(wxCommandEvent &event);
    
    /** \brief Callback function for the Save button */
    void onSave(wxCommandEvent &event);
    
    /** \brief Callback function for the Catch button */
    void onCatch(wxCommandEvent &event);
    
    /** \brief Callback function for keyup events */
    virtual void onKeyUp(wxKeyEvent &event) { }
    
    /** \brief Callback function for keydown events */
    virtual void onKeyDown(wxKeyEvent &event);

protected:
	/** \brief Returns a string with the name of the key
	 * 	\param key The key to get the name
	 * 	\return The "name" of the key
	 */
    wxString getStringFromKey(const ushort &key);

    wxTextCtrl 	*mNick;		/**< Textcontrol containing the nickname */
    wxRadioBox 	*mRadio;	/**< Radiobox for voice activation */
    wxTextCtrl 	*mKeyText;	/**< Textcontrol for the key */

    bool mButtonPressed;	/**< Is the Catch button pressed? */
    ushort mKey;			/**< The pressed key */

	/** \brief Declare the event table */
    DECLARE_EVENT_TABLE()
};

}

#endif
