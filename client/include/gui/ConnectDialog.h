/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_GUI_CONNECT_DIALOG_H__
#define __OS_GUI_CONNECT_DIALOG_H__

#include "openSpeak.h"

namespace GUI
{

/** \class ConnectDialog
  *	\brief Dialog to connect to a specified server
  *
  *	The ConnectDialog class provides a dialog in which the 
  *	user is able to enter connection informations (ip/url
  *	port, password) and finally to connect to the server.
  */
class ConnectDialog : public wxDialog
{
public:
	/** \brief The constructor of the ConnectDialog class */
    ConnectDialog(void);

	/** \brief The destructor of the ConnectDialog class */
    ~ConnectDialog(void);

	/** \brief Callback function for the Cancel button */
    void onCancel(wxCommandEvent &event) { Destroy(); }

	/** \brief Callback function for the OK button */
    void onOK(wxCommandEvent &event);

	/** \brief Callback function for Key events sent by wxw */
    void onKey(wxKeyEvent &event);

protected:
    wxTextCtrl *mServer;	/**< Text control for the serverip/-url */
    wxTextCtrl *mPort;		/**< Text control for the serverport */
    wxTextCtrl *mPass;		/**< Text control for the serverpassword */

    DECLARE_EVENT_TABLE()	/**< Declare an event table for wxw */

};

}

#endif
