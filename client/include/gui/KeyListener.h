/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */
  
#ifndef __OS_GUI_KEY_LISTENER_H__
#define __OS_GUI_KEY_LISTENER_H__

#include "openSpeak.h"

namespace GUI
{
	
/** \class KeyListener
 * 	\brief Abstract virtual class to catch key events
 * 
 * 	The KeyListener class is used to enable classes to catch key events
 * 	(up/down) together with the KeyTreeCtrl and the KeyButton classes.
 */
class KeyListener
{
 public:
 	/** \brief The virtual destructor of the KeyListener class */
	virtual ~KeyListener(void) { }
	
	/** \brief Callback function for keydown events */
	virtual void onKeyDown(wxKeyEvent &event) = 0;
	
	/** \brief Callback function for keyup events */
	virtual void onKeyUp(wxKeyEvent &event) = 0;
};

/** \class KeyTreeCtrl
 * 	\brief Treecontrol with ability to catch key events through the KeyListener class
 * 	
 * 	The KeyTreeCtrl is an extended wxTreeCtrl with the ability to catch key
 * 	events and send them to a registered KeyListener.
 */
class KeyTreeCtrl : public wxTreeCtrl
{
 public:
 	/** \brief The constructor of the KeyTreeCtrl class
 	 * 	\param parent The parent window
 	 * 	\param pos The position on the window
 	 * 	\param size The size of the treecontrol
 	 */
	KeyTreeCtrl(wxWindow *parent, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize) 
		: wxTreeCtrl(parent, -1, pos, size), mListener(0) { }
	
	/** \brief Specify the listener which catches the key events
	 * 	\param list The listener which catches the events
	 */
	void setListener(KeyListener *list) { if (list) mListener = list; }
	
	/** \brief Callback event for keyup events */
	void onKeyUp(wxKeyEvent &event) { if (mListener) mListener->onKeyUp(event); }
	
	/** \brief Callback event for keydown events */
	void onKeyDown(wxKeyEvent &event) { if (mListener) mListener->onKeyDown(event); }
	
 protected:
 	/** \brief Declare the event table for wxw */
	DECLARE_EVENT_TABLE()
	
	/** \brief The listening class */
	KeyListener *mListener;
};	


/** \class KeyButton
 * 	\brief Button with the ability to catch key events thorugh the KeyListener class
 * 	
 * 	The KeyButton class is an extended wxButton with the ability to catch key
 * 	events and send them to a registered KeyListener class.
 */
class KeyButton : public wxButton
{
 public:
 	/** \brief The constructor of the KeyButton class
 	 * 	\param parent The parent window
 	 * 	\param id The ID of the button to catch button events
 	 * 	\param label The label of the button
 	 * 	\param pos The position on the window
 	 * 	\param size The size of the button
 	 */
	KeyButton(wxWindow *parent, wxWindowID id, const wxString &label, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize)
		: wxButton(parent, id, label, pos, size), mListener(0) { }
	
	/** \brief Specify the listener which catches the key events
	 * 	\param list The listener which catches the events
	 */
	void setListener(KeyListener *list) { if (list) mListener = list; }
	
	/** \brief Callback event for keyup events */
	void onKeyUp(wxKeyEvent &event) { if (mListener) mListener->onKeyUp(event); }
	
	/** \brief Callback event for keydown events */
	void onKeyDown(wxKeyEvent &event) { if (mListener) mListener->onKeyDown(event); }
 
 protected:
 	/** \brief Declare the event table for wxw */
	DECLARE_EVENT_TABLE()
	
	/** \brief The listening class */
	KeyListener *mListener;
};
		
}

#endif
