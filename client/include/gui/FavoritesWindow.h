/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_GUI_FAVORITES_WINDOW_H__
#define __OS_GUI_FAVORITES_WINDOW_H__

#include "openSpeak.h"

namespace GUI
{

	/** \class FavoritesWindow
	 * 	\brief Window to display all added favorite servers
	 * 
	 * 	The FavoritesWindow is used to add, edit and remove favorite servers
	 * 	to be able to easily connect to many often used servers.
	 */
	class FavoritesWindow : public wxDialog
	{
	 public:

		/** \brief Some ID's for the buttons and the listbox of the FavoritesWindow */
		enum Events
		{
			CONNECT = 0,
			ADD,
			EDIT,
			REMOVE,
			CLOSE,
			LISTBOX
		};

		/** \brief The constructor of the FavoritesWindow class
		 * 	\param parent The parent frame
		 */
		FavoritesWindow(wxFrame *parent);
		
		/** \brief The destructor of the FavoritesWindow class */
		~FavoritesWindow(void);

		/** \brief Add a favorite to the file and the list
		 * 	\param fav The Favorite to add
		 */
		void addFavorite(Favorite *fav) { mFavMgr->addFavorite(fav); mList->Append( std2wx(fav->name), fav); }
		
		/** \brief Replace a favorite with another one
		 * 	\param fav The new favorite
		 */
		void setFavorite(Favorite *fav);

		/** \brief Callback function for the Connect button */
		void onConnect(wxCommandEvent &event);    
		/** \brief Callback function for the Add button */    
		void onAdd(wxCommandEvent &event);
		/** \brief Callback function for the Edit button */
		void onEdit(wxCommandEvent &event);
		/** \brief Callback function for the Remove button */
		void onRemove(wxCommandEvent &event);
		/** \brief Callback function for the Close button */
		void onClose(wxCommandEvent &event);

	 protected:
		wxListBox *mList;			/**< The listbox with the favorites */
		
		FavoritesMgr *mFavMgr;		/**< The manager which handles all favorites */

		/** \brief Create the event table */
		DECLARE_EVENT_TABLE()
	};

}

#endif
