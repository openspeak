/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __SPEEXPP_WRAPPER_H__
#define __SPEEXPP_WRAPPER_H__

#include "openSpeak.h"

/** \enum Mode
 * 	\brief Enum for the three different quality modes
 * 
 * 	The three modes are named after their speex equivalent and are also
 * 	used to change properties of the AudioWrapper.
 */
enum Mode
{
	NARROW_BAND = 0,
	WIDE_BAND,
	ULTRA_WIDE_BAND
};

/** \brief Used to map a userid to a SpeexBits struct */
typedef std::map< unsigned short int, SpeexBits > SpeexBitsUserMap;


/** \class SpeexWrapper
 *	\brief Class used to wrap around the C functions of Speex
 * 
 * 	The SpeexWrapper class is a wrapper around the C functions of the
 * 	Speex voice encoding library. It's used to compress the sound before
 * 	sending it over the network.
 */
class SpeexWrapper
{
 public:
 	/** \brief The constructor of the SpeexWrapper class
 	 * 	\param mode The quality of the Speex codec
 	 */
    SpeexWrapper(const int &mode);
    
    /** \brief The destructor of the SpeexWrapper class */
    ~SpeexWrapper(void);

	/** \brief Encodes a given soundframe using Speex
	 * 	\param dec The frame to encode
	 * 	\param enc A buffer in which the encoded sound will go
	 * 	\return The size of the encoded frame
	 */
    int encode(short* dec, char* enc);
    
    /** \brief Decode a given frame using Speex
     * 	\param dec The frame to decode
     * 	\param userId The user from which the frame came
     * 	\return The decoded frame
     */
    short* decode(char* dec, unsigned short int userId);

	/** \brief Get the size of a frame
	 * 	\return The size of a frame
	 */
    inline unsigned short getFrameSize(void) { return mFrameSize; }
    
    /** \brief Get the sampling rate
     * 	\return The sampling rate
     */
    inline unsigned short getSampleRate(void) { return mSampleRate; }

	/** \brief Returns if the voice activation is enabled
	 * 	\return True if enabled
	 */
    inline bool isVADEnabled(void) { return mVAD; }
    
    /** \brief Enable/Disable the voice activation
     * 	\param yes True to enable, false to disable
     */
    inline void enableVAD(bool yes) { mVAD = yes; }

 protected:
 	/** \brief Get the users SpeexBits struct
 	 * 	\param userId The ID of the user
 	 * 	\return The SpeexBits struct of the user
 	 */
    SpeexBits getUserSpeexBits(unsigned short int userId);
    
    /** \brief Reset the SpeexBits struct of a user
     * 	\param userId The ID of the struct to reset
     */
    void resetUserSpeexBits(unsigned short int userId);
    
    /** \brief Reset all SpeexBits */     
    void resetAllUsersSpeexBits(void);

    void 					*mEncoder;			/**< The Speex Encoder */
    void 					*mDecoder;			/**< The Speex Decoder */
    SpeexBits 				mEncBits;			/**< The SpeexBits to use when encoding */
    SpeexBitsUserMap 		mSpeexBitsUserMap;	/**< A map with SpeexBits for each user to decode stuff */
    SpeexPreprocessState 	*mPreProcess;		/**< The Speex Preprocessor */

    unsigned short 			mFrameSize;			/**< The size of one soundframe */
    int 					mSampleRate;		/**< The sampling rate */
    bool 					mVAD;				/**< Defines if the voice activation is enabled */
};

#endif
