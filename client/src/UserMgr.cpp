/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"

#ifdef OS_PLATFORM_WIN32
#   include <direct.h>
#else
#   include <sys/stat.h>
#endif

using namespace openSpeak;

UserMgr::UserMgr()
{

}

UserMgr::~UserMgr()
{
    mUsers.clear();
}

void UserMgr::addUser(const uint &id, const std::string &name)
{
    UserMap::const_iterator it = mUsers.find(id);
    if (it != mUsers.end())
    {
        changeUser(id, name);
        return;
    }
    else
    {
        User *tmp = new User;
        tmp->name = name;
        tmp->id = id;
        mUsers.insert(std::make_pair(id, tmp));
        StringEvent evt(name, EVT_ADDUSER);
        wxGetApp().getWindow()->AddPendingEvent(evt);
    }
}

void UserMgr::removeUser(const uint &id)
{
    UserMap::iterator it = mUsers.find(id);
    if (it != mUsers.end())
    {
        StringEvent evt(it->second->name, EVT_REMOVEUSER);
        wxGetApp().getWindow()->AddPendingEvent(evt);
        delete it->second;
        mUsers.erase(it);
    }
}

void UserMgr::changeUser(const uint &id, const std::string &name)
{
    UserMap::const_iterator it = mUsers.find(id);
    if (it != mUsers.end())
    {
        StringStringEvent evt(it->second->name, name, EVT_CHANGEUSER);
        wxGetApp().getWindow()->AddPendingEvent(evt);
        it->second->name = name;
    }
    else
    {
        addUser(id, name);
        return;
    }
}

User* UserMgr::getUser(const uint &id)
{
    UserMap::const_iterator it = mUsers.find(id);
    if (it != mUsers.end())
        return it->second;
    else
        return 0;
}

User* UserMgr::getUser(const std::string &name)
{
    UserMap::const_iterator it;
    for (it = mUsers.begin(); it != mUsers.end(); ++it)
    {
        if (it->second->name == name)
            return it->second;
    }
    return 0;
}

ushort UserMgr::getUserID(const std::string &name)
{
    UserMap::const_iterator it;
    for (it = mUsers.begin(); it != mUsers.end(); ++it)
    {
        if (it->second->name == name)
            return it->first;
    }
    return 0;
}
