/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"

using namespace openSpeak;

#define CHANNELS (1)

AudioWrapper::AudioWrapper()
{
    mSampleRate = mFramesize = 0;
    mRunning = false;
    mAMixer = 0;
}

AudioWrapper::~AudioWrapper()
{
	LOG_DEBUG("Destructing the AudioWrapper")
    if (mRunning)
        Stop();
}


bool AudioWrapper::Start(int mode, int sampleRate, int frameSize, void(*ProcessInputCallback)(short* , void* ), void* Object)
{

	LOG_DEBUG("Initialising of the AudioWrapper")
    mSampleRate = sampleRate;

    mProcessInputCallback = ProcessInputCallback;
    mCallbackObject = Object;

    double frameDuration = (double) frameSize / (double) sampleRate;
    LOG_DEBUG("Audio: mode="+StringUtils::toString(mode)+", sample rate="+
                StringUtils::toString(sampleRate)+"Hz, frame size="+StringUtils::toString(frameSize)+
                ", frame time="+StringUtils::toString(frameDuration) + "s")

    if (!mFramesize)
        mFramesize = frameSize;

    mNumSamples = mFramesize * CHANNELS;

    mNumBytes = mNumSamples * sizeof(short);

    mAMixer = new AudioMixer(mNumSamples, frameDuration);

    mErr = Pa_Initialize();

    if (mErr != paNoError) EXCEPTION(Pa_GetErrorText(mErr))

	initDevices();

    mErr = Pa_OpenStream(&mStream, &mInputParameters, &mOutputParameters, mSampleRate, mFramesize, paClipOff, this->PACallback, (void*)this);
    if (mErr != paNoError) EXCEPTION(Pa_GetErrorText(mErr))

    mErr = Pa_StartStream( mStream );
    if (mErr != paNoError) EXCEPTION(Pa_GetErrorText(mErr))

    return mRunning = true;
}


bool AudioWrapper::Stop()
{
	LOG_DEBUG("Stopping the AudioWrapper")
    mErr = Pa_CloseStream(mStream);
    if (mErr != paNoError) EXCEPTION(Pa_GetErrorText(mErr))

    delete mAMixer;
    mAMixer = 0;

    if (mRunning)
        Pa_Terminate();
    mRunning = false;
    return true;
}

int AudioWrapper::PACallback(   const void *input,
                                void *output,
                                unsigned long frameCount,
                                const PaStreamCallbackTimeInfo *timeInfo,
                                PaStreamCallbackFlags statusFlags,
                                void *userData) 
{

    AudioWrapper* myAudioWrapper =  static_cast<AudioWrapper*> (userData);

    int* tempBuff = myAudioWrapper->mAMixer->getBufferToPlay();
    int i, NumSamples = myAudioWrapper->mNumSamples;

    myAudioWrapper->mProcessInputCallback((short*) input, myAudioWrapper->mCallbackObject);

    if (tempBuff)
        for (i = 0; i < NumSamples; i++)
        /* converts the (int*) tempBuff into a (short*) avoiding / 65536
           NB : on a big-endian system, replace [2*i+1] by [2*i] */
            ((short*)output)[i] = ((short*)tempBuff)[2*i+1];
    else 
        for (i = 0; i < NumSamples; i++)
            ((short*)output)[i] = 0;

    return paContinue;
}

void AudioWrapper::initDevices(void) {
    #if defined( OS_PLATFORM_LINUX )
        //On Linux we preffer Alsa :)
        int AlsaHostApiID = Pa_HostApiTypeIdToHostApiIndex(paALSA);
        if (AlsaHostApiID >= 0) {
        	mInputParameters.device = Pa_GetHostApiInfo(AlsaHostApiID)->defaultInputDevice;
            mOutputParameters.device = Pa_GetHostApiInfo(AlsaHostApiID)->defaultOutputDevice;
            LOG_DEBUG("Audio : Chosen Input Device is Alsa device #" + StringUtils::toString(mInputParameters.device) + ", " + 
                            StringUtils::toString(Pa_GetDeviceInfo(mInputParameters.device)->name))
            LOG_DEBUG("Audio : Chosen Output Device is Alsa device #" + StringUtils::toString(mOutputParameters.device) + ", " + 
                            StringUtils::toString(Pa_GetDeviceInfo(mOutputParameters.device)->name))
        } else {
	        mOutputParameters.device = Pa_GetDefaultOutputDevice();
	        mInputParameters.device = Pa_GetDefaultInputDevice();
            LOG_DEBUG("Audio : Chosen Input Device is Non-Alsa device #" + StringUtils::toString(mInputParameters.device) + ", " + 
                            StringUtils::toString(Pa_GetDeviceInfo(mInputParameters.device)->name))
            LOG_DEBUG("Audio : Chosen Output Device is Non-Alsa device #" + StringUtils::toString(mOutputParameters.device) + ", " + 
                            StringUtils::toString(Pa_GetDeviceInfo(mOutputParameters.device)->name))
        }
    #else
    	mOutputParameters.device = Pa_GetDefaultOutputDevice();
        mInputParameters.device = Pa_GetDefaultInputDevice();
    #endif
    
    if (mInputParameters.device < 0)
	   EXCEPTION("No Input Device is available !");

    mInputParameters.channelCount = CHANNELS;
    mInputParameters.sampleFormat = paInt16;
    mInputParameters.suggestedLatency = Pa_GetDeviceInfo(mInputParameters.device)->defaultHighInputLatency;
    mInputParameters.hostApiSpecificStreamInfo = NULL;

    if (mOutputParameters.device < 0)
        EXCEPTION("No Output Device is available !");
    
    mOutputParameters.channelCount = CHANNELS;
    mOutputParameters.sampleFormat = paInt16;
    mOutputParameters.suggestedLatency = Pa_GetDeviceInfo(mOutputParameters.device)->defaultHighOutputLatency;
    mOutputParameters.hostApiSpecificStreamInfo = NULL;
}

