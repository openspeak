/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"

using namespace openSpeak;

#define INC_BUFFER(x) x = (x + 1) % mBufferUsableFrames;

AudioMixer::AudioMixer(int numSamples, double frameDuration) {

    LOG_DEBUG("AudioMixer: Starting up")

    mExtBuffSamples = numSamples;
    mFrameDuration = frameDuration;

    mCurrentFrame = 0;
    mInternalBuffer = 0;
    mFrameList = 0;
    mMixedFrames = 0;
    mRun = false;
    mIsNormalized = false;
    mLastAmp = 0;
    
    resetBufferProperties(0.2);

    LOG_DEBUG("AudioMixer: Started.")

}

int* AudioMixer::getBufferToPlay()
{
    int* BufferToPlay;
    if (mMixedFrames[mCurrentFrame]) { // If the current frame is playable
        if (!mRun) {
            mRun = true;
            LOG_DEBUG("AudioMixer: Buffer Full, playing it")
        }
        int* lastFrameBuffer = getFrameBuff(mCurrentFrame);
        mMixedFrames[mCurrentFrame] = false;
        INC_BUFFER(mCurrentFrame)
        if (mIsNormalized)
            mIsNormalized = false;
        else
            normalize(lastFrameBuffer);
        BufferToPlay = lastFrameBuffer;
    }
    else { // If the current frame is empty
        INC_BUFFER(mCurrentFrame)
        if (mUserBufferFrame.empty()) { // Then it means the buffer is empty because noone is speaking
            if (mRun) {
                mRun = false;
                LOG_DEBUG("AudioMixer: Buffer empty, stop playing")
            }
            if (mBufferUnderflow)  // Was the buffer long enough?
                resetBufferProperties(mBufferLenght + 0.04); // Raise the buffer length a little bit
        }
        else { // Then there is still users speaking
            int i;
            mBufferUnderflow = true;
            for (i = 0; i < mBufferUsableFrames; i++) // Browse the buffer to see if it has frames to play
                if (mMixedFrames[(mCurrentFrame + i) % mBufferUsableFrames]) {//Then we are not having a buffer underrun
                    mBufferUnderflow = false;
                    break;
                }
        }
        BufferToPlay = 0;
    }
    return BufferToPlay;
}

int AudioMixer::mix(unsigned short int userId, short *buffer) {

    int userFrame = findUserBuff(userId);      //Find (and increment) the user buffer start

    int* currentBuffer = getFrameBuff(userFrame);

    int i;
    if (mMixedFrames[userFrame]) { // This frame has already been used
        for (i = 0; i < mExtBuffSamples; i++)
            currentBuffer[i] += buffer[i];
    }
    else {
        for (i = 0; i < mExtBuffSamples; i++) // The user is the first to speak on this frame
            currentBuffer[i] = buffer[i];
        mMixedFrames[userFrame] = true;
    }

    return 0;
}

int AudioMixer::findUserBuff(unsigned short int userId) {
    int BufferNr;
    UserBufferFrame::const_iterator tempMap(mUserBufferFrame.find(userId));
    if (tempMap != mUserBufferFrame.end()) {
        BufferNr = tempMap->second;
        INC_BUFFER(mUserBufferFrame[userId])
    }
    else {
        LOG_DEBUG("AudioMixer: user "+StringUtils::toString(userId)+" started speaking")
        BufferNr = (mUserBufferFrame[userId] = (mCurrentFrame + mBufferFrames) % mBufferUsableFrames);
    }
    return BufferNr;
}

void AudioMixer::userStopSpeaking(unsigned short int userId) {

    UserBufferFrame::const_iterator tempMap(mUserBufferFrame.find(userId));
    if (tempMap != mUserBufferFrame.end())
        mUserBufferFrame.erase(userId);
    LOG_DEBUG("AudioMixer: user "+StringUtils::toString(userId)+" stopped speaking")
    return;
}

int* AudioMixer::getFrameBuff(int frame) {
    return mFrameList[frame];
}

void AudioMixer::normalize(int* buffer) {
//    printf("Amp: %d\n", mLastAmp);
    int peak = detectPeak(buffer);
    int frameBestAmp = SOUND_LEVEL / peak;
    int threshold, i;

    if (mLastAmp == 0) mLastAmp = frameBestAmp;

    if (frameBestAmp < mLastAmp) {   // Then we should lower the volume
        int volumeStep = (mLastAmp - frameBestAmp) / mExtBuffSamples;
        int sampleVolume = mLastAmp;
        for (i = 0 ; i < mExtBuffSamples ; i++) {
            sampleVolume -= volumeStep;
            threshold = MAX_INT / sampleVolume;
            if (buffer[i] > threshold)
                buffer[i] = MAX_INT;
            else if (-buffer[i] > threshold)
                buffer[i] = -MAX_INT;
            else
                buffer[i] *= sampleVolume;
        }
        mLastAmp = frameBestAmp;
    }
    else {
        for (i = 0 ; i < mExtBuffSamples ; i++)
                buffer[i] *= mLastAmp;
    }
}

int AudioMixer::detectPeak(int* buffer) {
    int sample, peak = 1;
    for (sample = 0 ; sample < mExtBuffSamples ; sample ++) {
        if (abs(buffer[sample]) > peak) peak = abs(buffer[sample]);
    }
    return peak;
}


AudioMixer::~AudioMixer () {
    freeMemory();
}

bool AudioMixer::isPlaying(void) {
    return mRun;
}

void AudioMixer::normalizeCurrentFrame(void) {
    if (mIsNormalized)
        return;
    if (mMixedFrames[mCurrentFrame]) {
        mIsNormalized = true;
        normalize(getFrameBuff(mCurrentFrame));
    }
    return;
}

void AudioMixer::resetBufferProperties(double BufferLength) {

    if (mRun)
        EXCEPTION("Trying to reset Buffer Properties while it is being played")
    //Let's clean memory first.
    freeMemory();

    mBufferUnderflow = false;

    mBufferLenght = BufferLength;

    mBufferFrames = (int) (mBufferLenght / mFrameDuration);
    mBufferUsableFrames = mBufferFrames * 2;

    int internalBufferSize = mBufferUsableFrames * mExtBuffSamples;
    mInternalBuffer = (int*) malloc(internalBufferSize * sizeof(int));

    mFrameList = (int**) malloc(mBufferUsableFrames * sizeof(int*));

    mMixedFrames = (bool*) malloc(mBufferUsableFrames * sizeof(bool));

    if ((!mInternalBuffer) || (!mFrameList) || (!mMixedFrames))
        EXCEPTION("Out of memory !")

    int i;

    for (i=0 ; i < mBufferUsableFrames; i++) {
        mFrameList[i] = &mInternalBuffer[mExtBuffSamples * i];
        mMixedFrames[i] = false;
    }

    LOG_DEBUG("AudioMixer: Buffer Length set to " + StringUtils::toString(BufferLength) + "s")
}

void AudioMixer::freeMemory(void) {
    if (mInternalBuffer)
        free(mInternalBuffer);
    if (mFrameList)
        free(mFrameList);
    if (mMixedFrames)
        free(mMixedFrames);
    return;
}
