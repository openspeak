/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"

using namespace openSpeak;

wxMutex *Thread::mMutex = 0;

Thread::Thread() : wxThread(wxTHREAD_JOINABLE)
{
	LOG_DEBUG("Thread: Starting up")
    Create();
    mMutex = new wxMutex();
    mRun = true;
    mSpeex = 0;
    mAudio = 0;
    mConnection = 0;
    mClock = 0;
    mTimer = 0;
    mLastCheck = mLastPacket = 0.0;

    mKeyPressed = mAudioRunning = mVAD = false;
    mState = STARTING;
    mUsers = new UserMgr();
}

Thread::~Thread()
{
	LOG_DEBUG("Thread: Shutting down")
	mMutex->Unlock();
    if (isConnected())
		disconnect();
	if (mAudioRunning)
		stopSound();
    if (mTimer)
        delete mTimer;
    if (mClock)
        delete mClock;
    delete mUsers;
	delete mMutex;
}

void* Thread::Entry()
{
    bool mSpeaking = false;      //Is the user speaking ?
    while(mRun)
        _loop();

    return 0;
}

void Thread::startSound(const int &mode)
{
    if (mAudioRunning)
        stopSound();

    mAudio = new AudioWrapper();
    mSpeex = new SpeexWrapper(mode);

    mEncodedBuffer = new char[mSpeex->getFrameSize()];

    mAudio->Start(mode, mSpeex->getSampleRate(), mSpeex->getFrameSize(), &this->processInputSound, (void*) this);
    mSpeex->enableVAD(mVAD);
    mAudioRunning = true;

    LOG_DEBUG("Thread: Started Audio")
}

void Thread::stopSound()
{
    if (!mAudioRunning) return;
    if (mAudio)
    {
        mAudio->Stop();
        delete mAudio;
        mAudio = 0;
    }
    if (mSpeex)
    {
        delete mSpeex;
        mSpeex = 0;
    }
    mAudioRunning = false;
    delete mEncodedBuffer;
    LOG_DEBUG("Thread: Stopped Audio")
}

void Thread::enableVAD(bool yes)
{
    wxMutexLocker lock(*mMutex);
    mVAD = yes;
    if (mSpeex)
        mSpeex->enableVAD(yes);
}

void Thread::connect(const std::string &server, const ushort &port, const std::string &nick, const std::string &password)
{
    if (isConnected())
        mConnection->disconnect();
    if (mTimer)
    	delete mTimer;

    wxMutexLocker lock(*mMutex);

    mConnection = new UDPConnection();
    mTimer = new CommonTimer(mClock);
    mConnection->connect(server, port, nick, password);
    mConnection->addListener(this);
    Callbacks::initCallbacks(mConnection);
    mLastCheck = mLastPacket = mTimer->getTime();
	mState = CHALLENGING;
	LOG_DEBUG("Thread: Opened Connection to server " + server + " (Port: " + StringUtils::toString(port) + ")")
}

void Thread::disconnect()
{
    if (!isConnected())
        return;
    wxMutexLocker lock(*mMutex);

    OutStream out;
    out << ID_DISCONNECTING;
    mConnection->forceSend(out);

    mConnection->disconnect();
    delete mConnection;
    mConnection = 0;
    delete mTimer;
    mTimer = 0;
    mState = DISCONNECTED;
    
   	if (mAudioRunning)
    	stopSound();
    	
    if (mRun)
    {
    	wxCommandEvent evt(EVT_DISCONNECT);
    	wxGetApp().getWindow()->AddPendingEvent(evt);
    }
    LOG_DEBUG("Thread: Disconnected from the server")
}

bool Thread::isConnected(void)
{
    wxMutexLocker lock(*mMutex);
    return mConnection && mConnection->isConnected();
}


void Thread::decode(unsigned short int userId, char* dec)
{
    if (dec == 0) {
        mAudio->getAudioMixer()->userStopSpeaking(userId); // Means that the remote user stop speaking
    }
    else {
        short* buffer = mSpeex->decode(dec, userId);
        if (!buffer) return;
        mAudio->getAudioMixer()->mix(userId, buffer);
        delete[] buffer;
    }
}

void Thread::decode(const char* s_userId, const char* dec)
{
    decode(StringUtils::toUShort(s_userId), const_cast<char*>(dec));
}

void Thread::send(const std::string &msg)
{
    wxMutexLocker lock(*mMutex);
    mConnection->send(msg);
}

ushort Thread::getState(void)
{
    return mState;
}

void Thread::setState(const ushort &state)
{
    if (mState < state)
        mState = state;
}

void Thread::onPacket(Packet *pkt)
{
    mLastPacket = mTimer->getTime();
}

void Thread::_loop()
{
/* Initialize some important things we'll always need */
    if (mClock)
        delete mClock;
    mClock = new Clock();

    try
    {

/* Run the main loop */
    while (1)
    {
		this->Sleep(1);
        mClock->makeStep();
        wxMutexLocker lock(*mMutex);

    /* Check if we want to exit the loop */
        if (TestDestroy() || !mRun)
            break;

    /* Do the network stuff */
        if (mConnection && mConnection->isConnected())
        {
        /* Update the connection and check the server if necessary */
            while(mConnection->select())
            	mConnection->process();
            if (mTimer->getTime() - mLastCheck >= 2.5 && mState > DISCONNECTED)
                _checkServer();
        }
    /* Do the audio stuff */
        if (mAudioRunning)
            mAudio->getAudioMixer()->normalizeCurrentFrame();
    }

    }
    catch (openSpeak::Exception &e)
    {
    /* Unlock the mutex, print the exception, send it to the gui and disconnect */
        mMutex->Unlock();
        e.print();
        StringEvent evt(e.getException(),EVT_EXCEPTION);
        wxGetApp().getWindow()->AddPendingEvent(evt);
    }

    disconnect();
    stopSound();
    LOG_DEBUG("Thread: Restarting/Stopping Loop")
}

void Thread::processInputSound(short* buffer, void* Object) {

    if ((!buffer) || (!Object)) return;

    Thread* myThread = static_cast<Thread*> (Object);

    int encodedFrameSize = myThread->mSpeex->encode(buffer, myThread->mEncodedBuffer);
    if (encodedFrameSize && (myThread->mVAD || myThread->mKeyPressed))
    {
/* Create the audio package to be sent */
        OutStream out;
        out << ID_SOUND << std::string(myThread->mEncodedBuffer, encodedFrameSize);
        myThread->mConnection->send(out);
        myThread->mSpeaking = true; //We are speaking ATM
    }
    else if (myThread->mSpeaking)
    {
/* Create empty audio package to be sent to notify others that we stop speaking */
        OutStream out;
        out << ID_STOP_SOUND;
        myThread->mConnection->send(out);
        myThread->mSpeaking = false; //We don't speak anymore
    }
}

void Thread::_checkServer()
{
/* Check if the server hasn't answered for a while */
    if (mTimer->getTime() - mLastPacket >= 5.0)
    {
        if (mState == CONNECTED)
        	EXCEPTION("Disconnecting from the server because of a server timeout!")
        else
        	EXCEPTION("Can't connect to the server!")
    }
/* Ping the server if it has */
    OutStream out;
    out << ID_PING;
    mConnection->send(out);
    mLastCheck = mTimer->getTime();
}
