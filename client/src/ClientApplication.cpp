/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"

#include "../../logo.xpm"

#define CHECK_OPTION(name,def) 	if (!mConfig->optionExists(name)) \
								{ \
									if (!mCommand->getStringValue(name).empty()) \
										mConfig->setOption(name, mCommand->getStringValue(name));  \
									else \
										mConfig->setOption(name, def); \
								}

using namespace openSpeak;

ClientApplication::~ClientApplication()
{
	if (mCommand)
		delete mCommand;
	if (Log::getSingleton())
	{
		LOG_SILENT("*** Closing Log ***")
		delete Log::getSingleton();
	}
}

bool ClientApplication::OnInit()
{
/* Initialise the commandline parser */
    mCommand = new CmdLineParser("openSpeak Client", "v0.1 SVN");

    CmdLineOption ServerOptions[] = {
        {"help", 'h', OPTION_HELP, NULL, "Print this help message", ""},
        {"version", 'v', OPTION_VERSION, NULL, "Print version informations", ""},
		{"connect", 'c', OPTION_ARG_STRING, NULL, "Connect to a specific server (ip:port@password)", "SERVER"},
		{"silent", 's', OPTION_ARG_NONE, NULL, "Stop logging to stdout", ""},
        {"name", 'n', OPTION_ARG_STRING, NULL, "Set the nickname the client will use", "NICK"},
        {"voice_activation", 'a', OPTION_ARG_NONE, NULL, "Output audio stream will be automatically open when you speak", ""},
        {"key", 'k', OPTION_ARG_STRING, NULL, "Set which key must be pressed to open output audio stream.", "KEY"},
		{NULL}
    };

    mCommand->addOption(ServerOptions);

#if defined( OS_PLATFORM_WIN32 )
	std::string path = "./";
	WIN32_FIND_DATA FileData;
	if (FindFirstFile(std::string(path + "log/*").c_str(), &FileData) == INVALID_HANDLE_VALUE)
    {
    	if(_mkdir(std::string(path + "log/").c_str()) == -1)
    	{
        	std::cerr << "Couldn't create log directory!\n";
        	return false;
    	}
    }
#elif defined( OS_PLATFORM_LINUX )
	std::string path = getenv("HOME") + std::string("/.openspeak/");
	if (opendir(std::string(path).c_str()) == NULL)
	{
		if (mkdir(path.c_str(), S_IRWXU | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH) == -1)
		{
			std::cerr << "Couldn't create directory ~/.openspeak\n";
			return false;
		}                
	}
	if (opendir(std::string(path + "log/").c_str()) == NULL)
	{
		if (mkdir(std::string(path + "log/").c_str(), S_IRWXU | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH) == -1)
		{
			std::cerr << "Couldn't create directoriy ~/.openspeak/log\n";
			return false;
		}
	}
#endif

    mPath = path;

    new Log(path + "log/client.log", mCommand->getSwitch("silent"));
    LOG_SILENT("**** Started Log ****");
    
/* Convert the wxChar** commandline to a char** */
#if !wxUSE_WCHAR_T
	char** cargv = (char**)argv;
#else
	char** cargv = new char*[argc];
	for (int i = 0; i < argc; ++i)
	{
		cargv[i] = new char[wcslen(argv[i])];
		wcstombs(cargv[i],argv[i], wcslen(argv[i])+1);	// +1 seems to be for '\0' but I'm not sure :)
	}
#endif	
/* Parse the cmdline */
	if (int error = mCommand->parseLine(argc, cargv) != SUCCESS)
    {
    	LOG_DEBUG("Startup: Parsing the commandline failed (Code: "+StringUtils::toString(error)+")")
        std::cerr << "Incorrect Argument!\nPlease run "<< cargv[0] <<" --help\n";
        return false;
    }    
/* Clean up cargv */
#if wxUSE_WCHAR_T
	for (int i = 0; i < argc; ++i)
		delete[] cargv[i];
	delete[] cargv;
	cargv = 0;
#endif
	LOG_DEBUG("Startup: Parsed the commandline")
	
	LOG_DEBUG("Startup: Loading config")
	mConfig = new Config(path + "settings.client.xml");	
    
    try 
    {
		bool checkcfg = false;
    	if (!mConfig->parse()) 
        {
    /* If the file doesnt exist it gets created and we can add some informations */
    	    LOG_DEBUG("Startup: Couldn't find config file, creating a new one")
			checkcfg = true;
		}
		else if (!mConfig->checkOptions("name", "voice_activation", "key", ""))
		{
			LOG_DEBUG("Startup: Variable missing in config file. Adding")
			checkcfg = true;
		}

		if (checkcfg)
		{
			CHECK_OPTION("name", "openSpeakUser")
			CHECK_OPTION("voice_activation", "0")
			CHECK_OPTION("key", "0")
       	}

    }
    catch (openSpeak::Exception &e)
    {
  	/* Log a error message */
     	LOG_FATAL(e.getException())
	    wxMessageBox(std2wx(e.getException()), _T("ERROR!"), wxOK | wxICON_EXCLAMATION, NULL);
       	return false;
    }    

	SetAppName(_T("openSpeak"));	

/* Create the GUI */
	LOG_DEBUG("Startup: Creating the MainWindow")
	mWindow = new GUI::MainWindow(_T("openSpeak - The opensource VoIP application"));
	wxIcon icon(openspeak_logo);
	mWindow->SetIcon(icon);
	mWindow->Show(true);
	SetTopWindow(mWindow);

/* Create and run the other thread */
	LOG_DEBUG("Startup: Creating the IP/Audio/Stuff Thread")
    mThread = new Thread();
    mThread->Run();
    mThread->enableVAD(StringUtils::toBool(getOption("voice_activation")));
	
/* Check if we have to connect to a server */
	if (!mCommand->getStringValue("connect").empty())
	{
		std::string::size_type index = 0;
		std::string server, port, password, connect = mCommand->getStringValue("connect");
		if ((index = connect.find_first_of(':')) == std::string::npos)
		{
			port = "23042";
			if ((index = connect.find_first_of('@')) == std::string::npos)
			{
				server = connect;
				password = "";
			}
			else
			{
				server = connect.substr(0, index);
				password = connect.substr(index+1);
			}
		}
		else
		{
			server = connect.substr(0,index);
			std::string::size_type pIn = index;
			if ((index = connect.find_first_of('@')) == std::string::npos)
			{
				port = connect.substr(pIn+1);
				password = "";
			}
			else
			{
				port = connect.substr(pIn,index-pIn);
				password = connect.substr(index+1);
			}
		}		
		mThread->connect(server, StringUtils::toUShort(port), password);
	}
			

	LOG_DEBUG("Startup: Finished")
	return true;
}

int ClientApplication::OnExit()
{
	LOG_DEBUG("Shutdown: Stopping Thread")
    mThread->stop();
    mThread->Wait();
    delete mThread;
    LOG_DEBUG("Shutdown: Deleting Config and CmdLine")
    mConfig->save();
    delete mConfig;
    return 0;
}

std::string ClientApplication::getOption(const std::string &option)
{
	if (!mCommand->getStringValue(option).empty())
		return mCommand->getStringValue(option);
	else
		return mConfig->getOption(option);
}

