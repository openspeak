/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"

namespace Callbacks
{
    using namespace openSpeak;

    void initCallbacks(UDPConnection *udp)
    {
    /* Add all necessary callbacks */
        udp->addCallback(ID_WELCOME, &onWelcome);
        udp->addCallback(ID_OK, &onOK);
        udp->addCallback(ID_ERROR, &onError);
        udp->addCallback(ID_CHANGE, &onChange);
        udp->addCallback(ID_NEED_PASSWORD, &onNeedPassword);
        udp->addCallback(ID_NUMBER_OF_USERS, &onUserNumber);
        udp->addCallback(ID_USER, &onUser);
		udp->addCallback(ID_NEW_USER, &onNewUser);
		udp->addCallback(ID_DISCONNECTING_USER, &onUserDisconnect);
		udp->addCallback(ID_FINISHED_CONNECTING, &onFinished);
		udp->addCallback(ID_SERVER_CLOSING, &onServerClosing);
		udp->addCallback(ID_MESSAGE, &onMessage);
		udp->addCallback(ID_SOUND, &onSound);
		udp->addCallback(ID_STOP_SOUND, &onStopSound);
		udp->addCallback(ID_SERVERSETTING, &onServerSetting);
		udp->addCallback(ID_PING, &onPing);
		udp->addCallback(ID_PONG, &onPong);
    }

    void onWelcome(UDPConnection *udp, Packet *pkt)
    {
    /* We got a valid server .. we should now send a registration request */
		if (THREAD->getState() >= REGING)
			return;
        OutStream out;
        out << ID_VERSION << OS_PROTOCOL_VERSION;
        udp->send(out);
		THREAD->setState(REGING);
    }

    void onOK(UDPConnection *udp, Packet *pkt)
    {
        if (pkt->data.empty())   return;
        ushort type = StringUtils::toUShort(pkt->data);
        switch (type)
        {
            case NICK_ACCEPTED:
				if (THREAD->getState() == REGING)
					THREAD->setState(REGED);
                break;
            case PASSWORD:
				if (THREAD->getState() <= NEED_PASSWORD)
					THREAD->setState(AUTHENTICATED);
                break;
            default:
                LOG_ERROR("Unknown ID_OK")
                break;
        }
    }

    void onError(UDPConnection *udp, Packet *pkt)
    {
        if (pkt->data.empty())   return;
    /* We catched an error .. quit */
		std::vector<std::string> vec = StringUtils::split(pkt->data);
        ushort type = StringUtils::toUShort(vec[0]);
        switch (type)
        {
            case NICK_IN_USE:
                EXCEPTION("Your nick is already in use! Please choose another one")
            case PASSWORD:
	        {
                GUI::PasswordDialog *dlg = new GUI::PasswordDialog("Your password is incorrect! Please enter the correct one.");
                dlg->Show();
                break;
	        }
            case NOT_REGISTERED:
                EXCEPTION("Server didn't get the registration! Try reconnecting")
            case SERVER_FULL:
                EXCEPTION("The server is full! Try it again in some seconds/minutes!")
            case VERSION_MISMATCH:
                EXCEPTION("We got a different version than the server!\nServer version: " + vec[1])
            case GOT_DISCONNECTED:
                EXCEPTION("We got disconnected from the server because we timed out!")
            case ALREADY_REGISTERED:
                break;
            default:
                EXCEPTION("Unknown ID_ERROR")
        }
    }

    void onChange(UDPConnection *udp, Packet *pkt)
    {
		std::vector<std::string> vec = StringUtils::split(pkt->data);
        if (vec.size() < 3)   return;
        ushort type = StringUtils::toUShort(vec[0]);
        switch (type)
        {
            case NICK:
            {
                User *usr = UserMgr::getSingleton()->getUser(StringUtils::toUShort(vec[1]));
                if (!usr)   return;
                UserMgr::getSingleton()->changeUser(usr->name, vec[2]);
                break;
            }
            case STATUS:
                break;
            case SERVERSETTING:
                break;
            default:
                break;
        }
    }

    void onNeedPassword(UDPConnection *udp, Packet *pkt)
    {
		if (THREAD->getState() >= NEED_PASSWORD)
			return;
        GUI::PasswordDialog *dlg = new GUI::PasswordDialog("You got a wrong password. Please enter the correct one");
        dlg->Show(true);
		if (THREAD->getState() < NEED_PASSWORD)
			THREAD->setState(NEED_PASSWORD);
    }

    void onUserNumber(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
    {
		if (THREAD->getState() >= GETTING_USERS)
			return;
		if (THREAD->getState() < GETTING_USERS)
			THREAD->setState(GETTING_USERS);
    }

    void onUser(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
    {
		if (THREAD->getState() != GETTING_USERS)
			return;
		std::vector<std::string> vec = StringUtils::split(pkt->data);
		if (vec.size() >= 2) 
			UserMgr::getSingleton()->addUser(StringUtils::toUShort(vec[0]), vec[1]);
    }

	void onNewUser(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
	{
		if (THREAD->getState() < CONNECTED)
			return;
		std::vector<std::string> vec = StringUtils::split(pkt->data);
        if (vec.size() >= 2)
            UserMgr::getSingleton()->addUser(StringUtils::toUShort(vec[0]), vec[1]);
	}

	void onUserDisconnect(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
	{
        if (THREAD->getState() != CONNECTED || pkt->data.empty())
            return;
        UserMgr::getSingleton()->removeUser(StringUtils::toUShort(pkt->data));
	}

	void onFinished(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
	{
		if (THREAD->getState() != GETTING_USERS || pkt->data.empty())
			return;
		THREAD->setState(CONNECTED);
		LOG_INFO("State: Connected")
		StringEvent evt("",EVT_CONNECT);
		wxGetApp().getWindow()->AddPendingEvent(evt);
	}

	void onServerClosing(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
	{
		LOG_INFO("State: Disconnected (Reason: Server is shutting down)")
		wxMessageBox(_T("The server is shutting down!"), _T("Disconnecting!"), wxOK | wxICON_INFORMATION, NULL);
		THREAD->disconnect();
	}

	void onMessage(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
	{
		std::string::size_type index = pkt->data.find(' ');
        MessageEvent evt(StringUtils::toUShort(pkt->data.substr(0,index)),pkt->data.substr(index+1), EVT_MESSAGE);
        wxGetApp().getWindow()->AddPendingEvent(evt);
	}

	void onSound(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
	{
		std::string::size_type index = pkt->data.find(' ');
        OutStream out;
        THREAD->decode(pkt->data.substr(0,index).c_str(), pkt->data.substr(index+1).c_str());
	}

	void onStopSound(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
	{
        THREAD->decode(pkt->data.c_str(), 0);
	}

	void onServerSetting(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
	{
		std::vector<std::string> vec = StringUtils::split(pkt->data);
		if (vec.size() < 2) return;
        ushort type = StringUtils::toUShort(vec[0]);
        switch(type)
        {
            case QUALITY:
                THREAD->startSound(StringUtils::toUShort(vec[1]));
                break;
            default:
                break;
        }
	}

	void onPing(openSpeak::UDPConnection *udp, openSpeak::Packet *pkt)
	{
	}

	void onPong(openSpeak::UDPConnection *upd, openSpeak::Packet *pkt)
	{
	}
}
