/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */
  
#include "openSpeak.h"

using namespace openSpeak;

FavoritesMgr::FavoritesMgr()
{
	mDoc = 0;
}

FavoritesMgr::~FavoritesMgr()
{
	if (mDoc)
	{
		save();
		delete mDoc;
		while (!mFavorites.empty())
		{
			delete mFavorites.back();
			mFavorites.pop_back();
		}
	}
	LOG_DEBUG("FavoritesMgr: Shutting down")
}

void FavoritesMgr::initialize()
{
	LOG_DEBUG("FavoritesMgr: Starting up")
	if (mDoc)
		delete mDoc;
	
/* Load the favorites file or create it */
	mDoc = new TiXmlDocument();
	mFile = wxGetApp().getPath() + "favorites.client.xml"; //TODO: Replace wxGetApp() through something without wxw
	if (!mDoc->LoadFile(mFile))
	{
		try
        {
            _createFile();
        }
        catch (...)
        {
            EXCEPTION("Loading of file "+mFile+" failed")
        }
	}
	
/* Get the root element */
    TiXmlElement *root = mDoc->FirstChildElement("favorites");
    if (!root)
        EXCEPTION("the root element of file favorites.client.xml is wrong")
        
/* Loop through all childs and add them to the list */
    TiXmlElement *child = root->FirstChildElement("favorite");
    while (child)
    {
        if (!child->Attribute("id") || !child->Attribute("name") || !child->Attribute("server") || !child->Attribute("port"))
        {
            child = child->NextSiblingElement("favorite");
            continue;
        }
        Favorite *fav = new Favorite();
        fav->name = child->Attribute("name");
        fav->server = child->Attribute("server");
        fav->port = atoi(child->Attribute("port"));
        fav->id = atoi(child->Attribute("id"));
        if (child->Attribute("password"))
            fav->password = child->Attribute("password");
        else
            fav->password = "";
        mFavorites.push_back(fav);
        child = child->NextSiblingElement("favorite");
    }	
}

Favorite* FavoritesMgr::getFavorite(const ushort &id)
{
	if (!mDoc)
		return 0;
		
	FavoritesVector::const_iterator it;
	for (it = mFavorites.begin(); it != mFavorites.end(); ++it)
	{
		if ((*it)->id == id)
			return *it;
	}
	return 0;
}

void FavoritesMgr::addFavorite(Favorite *fav)
{
	if (!fav || !mDoc)
		return;
	
	if (fav->name.empty() || fav->server.empty() || fav->port == 0)
		EXCEPTION("Can't add a incomplete favorite")
	
	LOG_DEBUG("FavoritesMgr: Adding new favorite")
	
	ushort hID = 0;
	FavoritesVector::const_iterator it;
	for (it = mFavorites.begin(); it != mFavorites.end(); ++it)
	{
		if ((*it)->id > hID)
			hID = (*it)->id;
	}
	fav->id = hID +1;
	
	mFavorites.push_back(fav);
	
	TiXmlElement *root = mDoc->FirstChildElement("favorites");
	if (!root)
		EXCEPTION("The favorites file is damaged!")
	TiXmlElement *tmp = new TiXmlElement("favorite");
	tmp->SetAttribute("id", fav->id);
	tmp->SetAttribute("name", fav->name);
	tmp->SetAttribute("server", fav->server);
	tmp->SetAttribute("port", fav->port);
	tmp->SetAttribute("password", fav->password);	
	root->LinkEndChild(tmp);
	save();
}

void FavoritesMgr::addFavorite(const std::string &name, const std::string &server, const ushort &port, const std::string &password)
{
	if (name.empty() || server.empty() || port == 0)
		EXCEPTION("Can't create a favorite without the correct data")
		
	Favorite *fav = new Favorite();
	fav->name = name;
	fav->server = server;
	fav->port = port;
	fav->password = password;
	addFavorite(fav);
}

void FavoritesMgr::removeFavorite(const ushort &id)
{
	if (!mDoc)
		return;
	
	LOG_DEBUG("FavoritesMgr: Removing favorite")
		
/* Remove the favorite from the vector */
	ushort i;
	for (i = 0; i < mFavorites.size(); ++i) 
	{
		if (mFavorites[i]->id == id)
			break;
	}

		
/* Get the root element */
	TiXmlElement *root = mDoc->FirstChildElement("favorites");
	if (!root)
	{
		wxMessageBox(_T("The favorites file is damaged!"), _T("ERROR!"), wxOK | wxICON_EXCLAMATION, NULL);
		return;
	}

/* Search for the correct child and remove it */
	TiXmlElement *child = root->FirstChildElement("favorite");
	while (child)
	{
		if (child->Attribute("id") == StringUtils::toString(id))
		{
			root->RemoveChild(child);
			break;
		}
		child = child->NextSiblingElement("favorite");
	}
	
/* Remove the favorite from the memory and save everything */
	if (mFavorites[i])
	{
		delete mFavorites[i];
		mFavorites.erase(mFavorites.begin() + i);
	}
	save();
}

void FavoritesMgr::removeFavorite(Favorite *fav)
{
	if (!fav || !mDoc)
		return;
	removeFavorite(fav->id);
}

void FavoritesMgr::setFavorite(Favorite *fav)
{
	if (!fav || !mDoc)
		return;
		
	if (fav->name.empty() || fav->server.empty() || fav->port == 0)
		EXCEPTION("Can't change a incomplete favorite")
		
	LOG_DEBUG("FavoritesMgr: Changing favorite")
	
	FavoritesVector::iterator it;
	for (it = mFavorites.begin(); it != mFavorites.end(); ++it)
	{
		if ((*it)->id == fav->id)
			*it = fav;	//TODO: This might or might not work :)
	}
	
/* Save the changes to the xml file */
	TiXmlElement *root = mDoc->FirstChildElement("favorites");
	if (!root)
		EXCEPTION("The favorites file is damaged!")

	TiXmlElement *child = root->FirstChildElement("favorite");
	while (child)
	{
		if (atoi(child->Attribute("id")) == fav->id)
		{
			child->SetAttribute("id", fav->id);
			child->SetAttribute("name", fav->name);
			child->SetAttribute("server", fav->server);
			child->SetAttribute("port", fav->port);
			child->SetAttribute("password", fav->password);
			save();
			break;
		}
		child = child->NextSiblingElement("favorite");
	}
}

bool FavoritesMgr::save()
{
	LOG_DEBUG("FavoritesMgr: Saving favorites")
	return (!mDoc || !mDoc->SaveFile(mFile));
}

void FavoritesMgr::_createFile()
{
	LOG_DEBUG("FavoritesMgr: Creating a new favorites file")
	if (mDoc)
		delete mDoc;
	
/* Check if the file really doesn't exists */
    std::fstream ftest;
	ftest.open(mFile.c_str(), std::ios::in);
	if (ftest.is_open())
		EXCEPTION("file already exists!")
	ftest.close();

/* Create a TiXmlDocument and add the root element */
    mDoc = new TiXmlDocument;
    TiXmlElement *elem = new TiXmlElement("favorites");
    mDoc->LinkEndChild(elem);
    save();
}
