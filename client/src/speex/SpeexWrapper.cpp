/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"
using namespace openSpeak;

SpeexWrapper::SpeexWrapper(const int &mode)
{
	LOG_DEBUG("Initialising of the SpeexWrapper")
    const SpeexMode *smode = 0;
    switch (mode)
    {
        case NARROW_BAND: smode = &speex_nb_mode; break;
        case WIDE_BAND: smode = &speex_wb_mode; break;
        case ULTRA_WIDE_BAND: smode = &speex_uwb_mode; break;
    }
    if (!smode) EXCEPTION("Speexmode not correctly set")

    mVAD = false;

    speex_bits_init(&mEncBits);

    mEncoder = speex_encoder_init(smode);
    mDecoder = speex_decoder_init(smode);

    speex_encoder_ctl(mEncoder, SPEEX_GET_FRAME_SIZE, &mFrameSize);
    speex_encoder_ctl(mEncoder, SPEEX_GET_SAMPLING_RATE, &mSampleRate);
    mPreProcess =  speex_preprocess_state_init(mFrameSize, mSampleRate);

    // aktivates VAD
    static int VAD = 1;
    // static int test= -1;

    speex_preprocess_ctl(mPreProcess, SPEEX_PREPROCESS_SET_VAD, &VAD);
    // speex_preprocess_ctl(mPreProcess,SPEEX_PREPROCESS_GET_VAD, &test);
    // std::cout << test << std::endl;
    speex_encoder_ctl(mEncoder, SPEEX_SET_VAD, &VAD);


}


SpeexWrapper::~SpeexWrapper()
{
	LOG_DEBUG("Destructing the SpeexWrapper")
    speex_encoder_destroy(mEncoder);
    speex_decoder_destroy(mDecoder);
    speex_preprocess_state_destroy(mPreProcess);
    speex_bits_destroy(&mEncBits);
    resetAllUsersSpeexBits();
}

int SpeexWrapper::encode(short* dec, char* enc)
{

    if (!enc)   return 0;

/* Cleanup */
    speex_bits_reset(&mEncBits);
    int speech = 1;

/* Preprocess the audio input and check if someones speaking */
    speech = speex_preprocess(mPreProcess, dec, 0);
    if (!speech) return 0;

    if (mVAD)
    {
        speex_preprocess_ctl(mPreProcess, SPEEX_PREPROCESS_GET_VAD, &speech);
        if (!speech) {
            return 0;
        }
    }

/* Encode the data */
    speex_encode_int(mEncoder, dec, &mEncBits);

/* Get the data from the SpeexBits and return it */
    int encodedFrameSize = speex_bits_nbytes(&mEncBits);
    
    speex_bits_write(&mEncBits, enc, mFrameSize);

    return encodedFrameSize;
}

short* SpeexWrapper::decode(char* dec, unsigned short int userId)
{
    if (!dec) return 0;

    SpeexBits userSpeexBits = getUserSpeexBits(userId);

    speex_bits_read_from(&userSpeexBits, dec, (int)strlen(dec));

    short* buf = new short[mFrameSize];
    speex_decode_int(mDecoder, &userSpeexBits, buf);
    if(!buf) return 0;
    return buf;
}

SpeexBits SpeexWrapper::getUserSpeexBits(unsigned short int userId) {

    SpeexBitsUserMap::const_iterator tempMap(mSpeexBitsUserMap.find(userId));
    if (tempMap != mSpeexBitsUserMap.end())
        return tempMap->second;
    speex_bits_init(&mSpeexBitsUserMap[userId]);
    return mSpeexBitsUserMap[userId];

}



void SpeexWrapper::resetUserSpeexBits(unsigned short int userId){

    SpeexBitsUserMap::const_iterator tempMap(mSpeexBitsUserMap.find(userId));
    if (tempMap != mSpeexBitsUserMap.end()) {
        speex_bits_destroy(&mSpeexBitsUserMap[userId]);
        mSpeexBitsUserMap.erase(userId);
    }
    return;
}

void SpeexWrapper::resetAllUsersSpeexBits(void) {
    SpeexBitsUserMap::const_iterator tempMap;
    for (tempMap = mSpeexBitsUserMap.begin(); tempMap != mSpeexBitsUserMap.end() ; tempMap++)
        speex_bits_destroy(&mSpeexBitsUserMap[tempMap->first]);

    mSpeexBitsUserMap.clear();

    return;
}
