/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"

BEGIN_EVENT_TABLE(GUI::EditFavoritesDialog, wxDialog)
  EVT_BUTTON  (wxID_OK, GUI::EditFavoritesDialog::onOK)
  EVT_BUTTON  (wxID_CANCEL, GUI::EditFavoritesDialog::onCancel)
END_EVENT_TABLE()

using namespace openSpeak;

namespace GUI
{

EditFavoritesDialog::EditFavoritesDialog(bool edit, FavoritesWindow *parent, Favorite *fav) : mEditing(edit), mFavWindow(parent), mFav(fav)
{
	LOG_DEBUG("GUI: Opening EditFavoritesDialog")
/* Decide which title to use */
    wxString title, name, server, port, password;
    if (mEditing)
    {
        if (!fav)
            Destroy();
        title = _T("Edit favorite");
        name = std2wx(fav->name);
        server = std2wx(fav->server);
        port = std2wx(StringUtils::toString(fav->port));
        password = std2wx(fav->password);
    }
    else
    {
        title = _T("Add favorite");
        name = _T("openSpeak Server");
        server = _T("127.0.0.1");
        port = _T("23042");
        password = _T("");
    }


/* Create the Dialog */
    Create(parent, -1, title, wxPoint(200,200), wxSize(250,215));

/* The sizers */
    wxBoxSizer *vertsizer = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *horzsizer = new wxBoxSizer(wxHORIZONTAL);
    wxGridSizer *sizer = new wxGridSizer(4, 2, 5, 5);

/* The name */
    sizer->Add(new wxStaticText(this, -1, _T("Name:"), wxPoint(10,10), wxSize(75,15)));    
    mName = new wxTextCtrl(this, -1, name, wxPoint(100,5), wxSize(150,27));
    sizer->Add(mName);

/* The server */
    sizer->Add(new wxStaticText(this, -1, _T("Server:"), wxPoint(10,50), wxSize(75,15)));
    mServer = new wxTextCtrl(this, -1, server, wxPoint(100,45), wxSize(150,27));
    sizer->Add(mServer);

/* The port */
    sizer->Add(new wxStaticText(this, -1, _T("Port:"), wxPoint(10, 90), wxSize(75,15)));
    mPort = new wxTextCtrl(this, -1, port, wxPoint(100, 85), wxSize(150,27));
    sizer->Add(mPort);

/* The nickname */
    sizer->Add(new wxStaticText(this, -1, _T("Password:"), wxPoint(10,130), wxSize(85,15)));
    mPass = new wxTextCtrl(this, -1, password, wxPoint(100,125), wxSize(150,27), wxTE_PASSWORD);
    sizer->Add(mPass);

/* The cancel and ok buttons */
    wxBoxSizer *buttonsizer = new wxBoxSizer(wxHORIZONTAL);
    buttonsizer->Add(new wxButton(this, wxID_OK, _T("Save"), wxPoint(20,170), wxSize(75,30)));
    buttonsizer->AddSpacer(10);
    buttonsizer->Add(new wxButton(this, wxID_CANCEL, _T("Cancel"), wxPoint(100,170), wxSize(75,30)));

    vertsizer->AddSpacer(15);
    vertsizer->Add(sizer);
    vertsizer->AddSpacer(10);
    vertsizer->Add(buttonsizer, wxSizerFlags().Center());
    vertsizer->AddSpacer(15);

    horzsizer->AddSpacer(15);
    horzsizer->Add(vertsizer);
    horzsizer->AddSpacer(15);

    SetSizer(horzsizer);
    horzsizer->SetSizeHints(this);
}

EditFavoritesDialog::~EditFavoritesDialog()
{
	LOG_DEBUG("GUI: Closing EditFavoritesDialog")
}

void EditFavoritesDialog::onOK(wxCommandEvent &event)
{
	LOG_DEBUG("EditFavorites: Saving changes/Adding favorite")
    std::string name, server, port, password;

    if (mName->GetValue().IsEmpty() || mServer->GetValue().IsEmpty())
    {
        wxMessageBox(_T("Please fill out everything!"), _T("ERROR!"), wxOK | wxICON_EXCLAMATION, NULL);
        return;
    }
    else if (mPort->GetValue().IsEmpty())
    {
        port = "23042";
    }
    else
    {
        port = wx2std(mPort->GetValue());
    }    
    
/* Save all values into std::strings */
    name = wx2std(mName->GetValue());
    server = wx2std(mServer->GetValue());
    password = wx2std(mPass->GetValue());	

	if (mEditing)
	{
	/* Save the changes to the list */
		mFav->name = name;
		mFav->server = server;
		mFav->port = StringUtils::toUShort(port);
		mFav->password = password;
		mFavWindow->setFavorite(mFav);
    }
    else
    {
    /* Create a new favorite to add it to the list */
        Favorite *fav = new Favorite();
        fav->name = name;
        fav->server = server;
        fav->port = StringUtils::toUShort(port);
        fav->password = password;
        mFavWindow->addFavorite(fav);
    }
    Destroy();
}

}
