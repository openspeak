/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"


BEGIN_EVENT_TABLE(GUI::ConnectDialog, wxDialog)
  EVT_BUTTON  (wxID_OK, GUI::ConnectDialog::onOK)
  EVT_BUTTON  (wxID_CANCEL, GUI::ConnectDialog::onCancel)
  EVT_KEY_UP  (GUI::ConnectDialog::onKey)
END_EVENT_TABLE()

using namespace openSpeak;

namespace GUI
{

ConnectDialog::ConnectDialog() : wxDialog(wxGetApp().getWindow(), -1, _T("Connect to a Server"), wxPoint(150,150), wxSize(245,170))
{
	LOG_DEBUG("GUI: Opening ConnectDialog")
/* Get the values we should put in there */
    wxString server, port, password;
    if (wxGetApp().optionExists("server.ip"))
        server = std2wx(wxGetApp().getOption("server.ip"));
    else
        server = _T("127.0.0.1");

    if (wxGetApp().optionExists("server.port"))
        port = std2wx(wxGetApp().getOption("server.port"));
    else
        port = _T("23042");

    if (wxGetApp().optionExists("server.password"))
        password = std2wx(wxGetApp().getOption("server.password"));
    else
        password = _T("");

/* The sizers */
    wxBoxSizer *vertsizer = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *horzsizer = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *buttonsizer = new wxBoxSizer(wxHORIZONTAL);
    wxGridSizer *gridsizer = new wxGridSizer(3,2,5,5);

/* The server */
    gridsizer->Add(new wxStaticText(this, -1, _T("Server: "), wxPoint(10,10), wxSize(75,15)));
    mServer = new wxTextCtrl(this, -1, server, wxPoint(100,5), wxSize(125,27));
    gridsizer->Add(mServer);

/* The port */
    gridsizer->Add(new wxStaticText(this, -1, _T("Port: "), wxPoint(10, 50), wxSize(75,15)));
    mPort = new wxTextCtrl(this, -1, port, wxPoint(100, 45), wxSize(125,27));
    gridsizer->Add(mPort);

/* The nickname */
    gridsizer->Add(new wxStaticText(this, -1, _T("Password: "), wxPoint(10,90), wxSize(85,15)));
    mPass = new wxTextCtrl(this, -1, password, wxPoint(100,85), wxSize(125,27), wxTE_PASSWORD);
    gridsizer->Add(mPass);

/* The cancel and ok buttons */
    buttonsizer->Add(new wxButton(this, wxID_OK, _T("Connect"), wxPoint(10,130), wxSize(75,30)));
    buttonsizer->AddSpacer(10);
    buttonsizer->Add(new wxButton(this, wxID_CANCEL, _T("Cancel"), wxPoint(90,130), wxSize(75,30)));

    mServer->SetFocus();

    vertsizer->AddSpacer(15);
    vertsizer->Add(gridsizer);
    vertsizer->AddSpacer(10);
    vertsizer->Add(buttonsizer, wxSizerFlags().Center());
    vertsizer->AddSpacer(15);

    horzsizer->AddSpacer(15);
    horzsizer->Add(vertsizer);
    horzsizer->AddSpacer(15);

    SetSizer(horzsizer);
    horzsizer->SetSizeHints(this);
}

ConnectDialog::~ConnectDialog()
{
	LOG_DEBUG("GUI: Closing ConnectDialog")
}

void ConnectDialog::onOK(wxCommandEvent &event)
{
	LOG_DEBUG("ConnectDialog: Connect initiated")
/* Check all TextCtrl's for valid values */
    ushort port;
    if (mServer->GetValue().IsEmpty())
    {
        StringEvent evt("Please enter a server to connect to",EVT_EXCEPTION);
        wxGetApp().getWindow()->AddPendingEvent(evt);
    }
    else if (mPort->GetValue().IsEmpty())
    {
        port = 23042;
    }
    else
    {
        port = StringUtils::toUShort(std::string(mPort->GetValue().ToAscii()));
    }

    THREAD->connect(std::string(mServer->GetValue().ToAscii()), port, wxGetApp().getOption("name"), std::string(mPass->GetValue().ToAscii()));
    THREAD->enableVAD(openSpeak::StringUtils::toBool(wxGetApp().getOption("voice_activation")));

/* Save the server we're connecting to */
    if (std::string(mServer->GetValue().ToAscii()) != wxGetApp().getOption("server.ip"))
        wxGetApp().setOption("server.ip", std::string(mServer->GetValue().ToAscii()));
    if (std::string(mPort->GetValue().ToAscii()) != wxGetApp().getOption("server.port"))
        wxGetApp().setOption("server.port", std::string(mPort->GetValue().ToAscii()));
    if (std::string(mPass->GetValue().ToAscii()) != wxGetApp().getOption("server.password"))
        wxGetApp().setOption("server.password", std::string(mPass->GetValue().ToAscii()));

    Destroy();
}

void ConnectDialog::onKey(wxKeyEvent &event)
{
    if (event.GetKeyCode() == WXK_RETURN)
    {
        wxCommandEvent event;
        onOK(event);
    }
}

}
