/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"

using namespace openSpeak;

BEGIN_EVENT_TABLE(GUI::SettingsDialog, wxDialog)
  EVT_BUTTON  (wxID_OK, GUI::SettingsDialog::onSave)
  EVT_BUTTON  (wxID_CANCEL, GUI::SettingsDialog::onCancel)
  EVT_BUTTON  (GUI::SettingsDialog::BUTTONCATCH, GUI::SettingsDialog::onCatch)
END_EVENT_TABLE()

namespace GUI
{

SettingsDialog::SettingsDialog() : wxDialog(wxGetApp().getWindow(), -1, _T("Settings"), wxPoint(200,150), wxSize(350, 600)), mButtonPressed(false), mKey(0)
{
	LOG_DEBUG("GUI: Opening SettingsDialog")
/* The sizers */
    wxBoxSizer *vertsizer = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *horzsizer = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *buttonsizer = new wxBoxSizer(wxHORIZONTAL);
    wxGridSizer *gridsizer = new wxFlexGridSizer(2,2,5,5);

/* The nickname you want to have */
    gridsizer->Add(new wxStaticText(this, -1, _T("Nickname: "), wxPoint(10,10), wxSize(75,15)));
    mNick = new wxTextCtrl(this, -1, std2wx(wxGetApp().getOption("name")), wxPoint(100,5), wxSize(125,27));
    gridsizer->Add(mNick);
    gridsizer->AddSpacer(10);
    gridsizer->AddSpacer(10);

/* The type of voice activation */
    gridsizer->Add(new wxStaticText(this, -1, _T("Voice Activation: "), wxPoint(10,10), wxSize(150,20)));
    wxString str[2] = {_T("Key"), _T("VoiceDetection")};
    mRadio = new wxRadioBox(this, -1, _T("Type: "), wxPoint(10,10), wxSize(130,75), 2, str, 2, wxRA_SPECIFY_ROWS);
    if (!wxGetApp().getOption("voice_activation").empty())
        mRadio->SetSelection(StringUtils::toInt(wxGetApp().getOption("voice_activation")));
    gridsizer->Add(mRadio);
    gridsizer->AddSpacer(10);
    gridsizer->AddSpacer(10);

/* The key you want to press for voice activation */
    gridsizer->Add(new wxStaticText(this, -1, _T("Voice Activation Key: ")));
    wxBoxSizer *box = new wxBoxSizer(wxHORIZONTAL);
    mKeyText = new wxTextCtrl(this, -1, getStringFromKey(StringUtils::toUShort(wxGetApp().getOption("key"))), wxPoint(-1,-1), wxSize(50,27), wxTE_READONLY);
    box->Add(mKeyText);
    box->AddSpacer(5);
    KeyButton *btn = new KeyButton(this, BUTTONCATCH, _T("Catch"), wxPoint(-1,-1), wxSize(60,27));
    btn->setListener(this);
    box->Add(btn);
    gridsizer->Add(box);

/* The two buttons */
    buttonsizer->Add(new wxButton(this, wxID_OK, _T("Save"), wxPoint(10,550), wxSize(75,30)));
    buttonsizer->AddSpacer(10);
    buttonsizer->Add(new wxButton(this, wxID_CANCEL, _T("Cancel"), wxPoint(90,550), wxSize(75,30)));

    vertsizer->AddSpacer(15);
    vertsizer->Add(gridsizer);
    vertsizer->AddSpacer(10);
    vertsizer->Add(buttonsizer, wxSizerFlags().Center());
    vertsizer->AddSpacer(15);

    horzsizer->AddSpacer(15);
    horzsizer->Add(vertsizer);
    horzsizer->AddSpacer(15);

    SetSizer(horzsizer);
    horzsizer->SetSizeHints(this);
}

SettingsDialog::~SettingsDialog()
{
	LOG_DEBUG("GUI: Closing SettingsDialog")
}

void SettingsDialog::onCancel(wxCommandEvent &event)
{
    Destroy();
}

void SettingsDialog::onSave(wxCommandEvent &event)
{
	LOG_DEBUG("Settings: Saving Settings")
    if (wxGetApp().getOption("name") != wx2std(mNick->GetValue()))
    {
        wxGetApp().setOption("name", wx2std(mNick->GetValue()));
        if (THREAD->isConnected())
        {
            OutStream out;
            out << ID_CHANGE << NICK << wx2std(mNick->GetValue());
            THREAD->send(out);
        }
    }

    if (wxGetApp().getOption("voice_activation") != StringUtils::toString(mRadio->GetSelection()))
        wxGetApp().setOption("voice_activation", StringUtils::toString(mRadio->GetSelection()));

    if (mKey != StringUtils::toUShort(wxGetApp().getOption("key")))
        wxGetApp().setOption("key", StringUtils::toString(mKey));
    Destroy();
}

void SettingsDialog::onCatch(wxCommandEvent &evt)
{
    mButtonPressed = true;
    LOG_DEBUG("Settings: Catching key")
}

void SettingsDialog::onKeyDown(wxKeyEvent &evt)
{
    if (!mButtonPressed) return;
    mKeyText->SetValue(getStringFromKey(evt.GetKeyCode()));
    mKey = evt.GetKeyCode();
    mButtonPressed = false;
    LOG_DEBUG("Settings: Catched key")
}

wxString SettingsDialog::getStringFromKey(const ushort &key)
{
    wxString ret;
    switch (key)
    {
        case WXK_BACK: ret = _T("BACK"); break;
        case WXK_TAB: ret = _T("TAB"); break;
        case WXK_RETURN: ret = _T("RETURN"); break;
        case WXK_ESCAPE: ret = _T("ESCAPE"); break;
        case WXK_SPACE: ret = _T("SPACE"); break;
        case WXK_DELETE: ret = _T("DELETE"); break;
        case WXK_START: ret = _T("START"); break;
        case WXK_LBUTTON: ret = _T("LBUTTON"); break;
        case WXK_RBUTTON: ret = _T("RBUTTON"); break;
        case WXK_CANCEL: ret = _T("CANCEL"); break;
        case WXK_MBUTTON: ret = _T("MBUTTON"); break;
        case WXK_CLEAR: ret = _T("CLEAR"); break;
        case WXK_SHIFT: ret = _T("SHIFT"); break;
        case WXK_ALT: ret = _T("ALT"); break;
        case WXK_CONTROL: ret = _T("CONTROL"); break;
        case WXK_MENU: ret = _T("MENU"); break;
        case WXK_PAUSE: ret = _T("PAUSE"); break;
        case WXK_CAPITAL: ret = _T("CAPITAL"); break;
        case WXK_END: ret = _T("END"); break;
        case WXK_HOME: ret = _T("HOME"); break;
        case WXK_LEFT: ret = _T("LEFT"); break;
        case WXK_UP: ret = _T("UP"); break;
        case WXK_RIGHT: ret = _T("RIGHT"); break;
        case WXK_DOWN: ret = _T("DOWN"); break;
        case WXK_SELECT: ret = _T("SELECT"); break;
        case WXK_PRINT: ret = _T("PRINT"); break;
        case WXK_EXECUTE: ret = _T("EXECUTE"); break;
        case WXK_SNAPSHOT: ret = _T("SNAPSHOT"); break;
        case WXK_INSERT: ret = _T("INSERT"); break;
        case WXK_HELP: ret = _T("HELP"); break;
        case WXK_NUMPAD0: ret = _T("NUMPAD0"); break;
        case WXK_NUMPAD1: ret = _T("NUMPAD1"); break;
        case WXK_NUMPAD2: ret = _T("NUMPAD2"); break;
        case WXK_NUMPAD3: ret = _T("NUMPAD3"); break;
        case WXK_NUMPAD4: ret = _T("NUMPAD4"); break;
        case WXK_NUMPAD5: ret = _T("NUMPAD5"); break;
        case WXK_NUMPAD6: ret = _T("NUMPAD6"); break;
        case WXK_NUMPAD7: ret = _T("NUMPAD7"); break;
        case WXK_NUMPAD8: ret = _T("NUMPAD8"); break;
        case WXK_NUMPAD9: ret = _T("NUMPAD9"); break;
        case WXK_MULTIPLY: ret = _T("MULTIPLY"); break;
        case WXK_ADD: ret = _T("ADD"); break;
        case WXK_SEPARATOR: ret = _T("SEPARATOR"); break;
        case WXK_SUBTRACT: ret = _T("SUBTRACT"); break;
        case WXK_DECIMAL: ret = _T("DECIMAL"); break;
        case WXK_DIVIDE: ret = _T("DIVIDE"); break;
        case WXK_F1: ret = _T("F1"); break;
        case WXK_F2: ret = _T("F2"); break;
        case WXK_F3: ret = _T("F3"); break;
        case WXK_F4: ret = _T("F4"); break;
        case WXK_F5: ret = _T("F5"); break;
        case WXK_F6: ret = _T("F6"); break;
        case WXK_F7: ret = _T("F7"); break;
        case WXK_F8: ret = _T("F8"); break;
        case WXK_F9: ret = _T("F9"); break;
        case WXK_F10: ret = _T("F10"); break;
        case WXK_F11: ret = _T("F11"); break;
        case WXK_F12: ret = _T("F12"); break;
        case WXK_F13: ret = _T("F13"); break;
        case WXK_F14: ret = _T("F14"); break;
        case WXK_F15: ret = _T("F15"); break;
        case WXK_F16: ret = _T("F16"); break;
        case WXK_F17: ret = _T("F17"); break;
        case WXK_F18: ret = _T("F18"); break;
        case WXK_F19: ret = _T("F19"); break;
        case WXK_F20: ret = _T("F20"); break;
        case WXK_F21: ret = _T("F21"); break;
        case WXK_F22: ret = _T("F22"); break;
        case WXK_F23: ret = _T("F23"); break;
        case WXK_F24: ret = _T("F24"); break;
        case WXK_NUMLOCK: ret = _T("NUMLOCK"); break;
        case WXK_SCROLL: ret = _T("SCROLL"); break;
        case WXK_PAGEUP: ret = _T("PAGEUP"); break;
        case WXK_PAGEDOWN: ret = _T("PAGEDOWN"); break;
        case WXK_NUMPAD_SPACE: ret = _T("NUMPAD_SPACE"); break;
        case WXK_NUMPAD_TAB: ret = _T("NUMPAD_TAB"); break;
        case WXK_NUMPAD_ENTER: ret = _T("NUMPAD_ENTER"); break;
        case WXK_NUMPAD_F1: ret = _T("NUMPAD_F1"); break;
        case WXK_NUMPAD_F2: ret = _T("NUMPAD_F2"); break;
        case WXK_NUMPAD_F3: ret = _T("NUMPAD_F3"); break;
        case WXK_NUMPAD_F4: ret = _T("NUMPAD_F4"); break;
        case WXK_NUMPAD_HOME: ret = _T("NUMPAD_HOME"); break;
        case WXK_NUMPAD_LEFT: ret = _T("NUMPAD_LEFT"); break;
        case WXK_NUMPAD_UP: ret = _T("NUMPAD_UP"); break;
        case WXK_NUMPAD_RIGHT: ret = _T("NUMPAD_RIGHT"); break;
        case WXK_NUMPAD_DOWN: ret = _T("NUMPAD_DOWN"); break;
        case WXK_NUMPAD_PAGEUP: ret = _T("NUMPAD_PAGEUP"); break;
        case WXK_NUMPAD_PAGEDOWN: ret = _T("NUMPAD_PAGEDOWN"); break;
        case WXK_NUMPAD_END: ret = _T("NUMPAD_END"); break;
        case WXK_NUMPAD_BEGIN: ret = _T("NUMPAD_BEGIN"); break;
        case WXK_NUMPAD_INSERT: ret = _T("NUMPAD_INSERT"); break;
        case WXK_NUMPAD_DELETE: ret = _T("NUMPAD_DELETE"); break;
        case WXK_NUMPAD_EQUAL: ret = _T("NUMPAD_EQUAL"); break;
        case WXK_NUMPAD_MULTIPLY: ret = _T("NUMPAD_MULTIPLY"); break;
        case WXK_NUMPAD_ADD: ret = _T("NUMPAD_ADD"); break;
        case WXK_NUMPAD_SEPARATOR: ret = _T("NUMPAD_SEPARATOR"); break;
        case WXK_NUMPAD_SUBTRACT: ret = _T("NUMPAD_SUBTRACT"); break;
        case WXK_NUMPAD_DECIMAL: ret = _T("NUMPAD_DECIMAL"); break;

        default:
        {
           if ( wxIsprint((int)key) )
               ret.Printf(_T("%c"), (char)key);
           else if ( key > 0 && key < 27 )
               ret.Printf(_("Ctrl-%c"), _T('A') + key - 1);
           else
               ret.Printf(_T("unknown (%d)"), key);
        }
    }
    return ret;
}

}
