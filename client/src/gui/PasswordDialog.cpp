/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "../../include/openSpeak.h"

using namespace openSpeak;

BEGIN_EVENT_TABLE(GUI::PasswordDialog, wxDialog)
    EVT_BUTTON(wxID_OK, GUI::PasswordDialog::onOK)
    EVT_BUTTON(wxID_CANCEL, GUI::PasswordDialog::onCancel)
    EVT_KEY_UP(GUI::PasswordDialog::onKey)
END_EVENT_TABLE()

namespace GUI
{

PasswordDialog::PasswordDialog(const std::string &title) : wxDialog(wxGetApp().getWindow(), -1, _T("Please enter a password"), wxPoint(150,150), wxSize(1,1))
{
	LOG_DEBUG("GUI: Opening PasswordDialog")
/* The sizers */
    wxBoxSizer *vertsizer = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *horzsizer = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *buttonsizer = new wxBoxSizer(wxHORIZONTAL);
    wxGridSizer *gridsizer = new wxGridSizer(1,2,5,5);

    vertsizer->AddSpacer(15);
    wxStaticText *text = new wxStaticText(this, -1, _T(""), wxPoint(10,10), wxSize(250,15));
    text->SetLabel(std2wx(title));
    text->Wrap(400);
    vertsizer->Add(text, wxSizerFlags().Center());
    vertsizer->AddSpacer(20);

    gridsizer->Add(new wxStaticText(this, -1, _T("Password:"), wxPoint(1,1), wxSize(75,15)));
    mPassword = new wxTextCtrl(this, -1, _T(""), wxPoint(1,1), wxSize(125,27));
    gridsizer->Add(mPassword);
    vertsizer->Add(gridsizer);
    vertsizer->AddSpacer(10);

    buttonsizer->Add(new wxButton(this, wxID_OK, _T("OK"), wxPoint(10,130), wxSize(75,30)));
    buttonsizer->AddSpacer(10);
    buttonsizer->Add(new wxButton(this, wxID_CANCEL, _T("Cancel"), wxPoint(90,130), wxSize(75,30)));
    vertsizer->Add(buttonsizer, wxSizerFlags().Center());
    vertsizer->AddSpacer(15);

    horzsizer->AddSpacer(15),
    horzsizer->Add(vertsizer);
    horzsizer->AddSpacer(15);

    SetSizer(horzsizer);
    horzsizer->SetSizeHints(this);
}

PasswordDialog::~PasswordDialog()
{
	LOG_DEBUG("GUI: Closing PasswordDialog")
}

void PasswordDialog::onOK(wxCommandEvent &event)
{	
    if (mPassword->GetValue().IsEmpty())
    {
        wxMessageBox(_T("Please enter a password!"), _T("ERROR!"), wxOK | wxICON_EXCLAMATION, NULL);
        return;
    }
    LOG_DEBUG("PasswordDialog: Sending Password to server")
    OutStream out;
    openSpeak::Hash hash(wx2std(mPassword->GetValue()));
    out << ID_PASSWORD << hash.getHash();
    THREAD->send(out);
    Destroy();
}

void PasswordDialog::onKey(wxKeyEvent &event)
{
    if (event.GetKeyCode() == WXK_RETURN)
    {
        wxCommandEvent event;
        onOK(event);
    }
}

}
