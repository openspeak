/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"

using namespace openSpeak;

BEGIN_EVENT_TABLE(GUI::ChatWindow, wxDialog)
    EVT_BUTTON(GUI::ChatWindow::SEND, GUI::ChatWindow::onSend)
    EVT_BUTTON(GUI::ChatWindow::CLOSE, GUI::ChatWindow::onCloseBtn)
    EVT_TEXT_ENTER(GUI::ChatWindow::TEXT, GUI::ChatWindow::onSend)
    EVT_CLOSE(GUI::ChatWindow::onClose)
END_EVENT_TABLE()

namespace GUI
{

ChatWindow::ChatWindow(MainWindow *parent, const ushort &id, const std::string &username)
: wxDialog(parent, -1, _T("Chat with ") + std2wx(username)  , wxPoint(150,150), wxSize(375,259)), mWindow(parent), mName(std2wx(username)), mID(id)
{
	LOG_DEBUG("GUI: Opening ChatWindow with user " + username)
/* The sizers */
    wxBoxSizer *vertsizer = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *horzsizer = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *buttonsizer = new wxBoxSizer(wxHORIZONTAL);

/* The TextCtrl where we're displaying incoming and outgoing messages */
    vertsizer->AddSpacer(15);
    mGet = new wxTextCtrl(this, -1, _T(""), wxPoint(25,10), wxSize(325, 100), wxTE_MULTILINE | wxTE_READONLY | wxTE_AUTO_URL);
    vertsizer->Add(mGet);

/* Here you can type your messages */
    vertsizer->AddSpacer(10);
    mSend = new wxTextCtrl(this, TEXT, _T(""), wxPoint(25, 115), wxSize(325, 100), wxTE_MULTILINE | wxTE_PROCESS_ENTER );
    vertsizer->Add(mSend);

/* The buttons */
    vertsizer->AddSpacer(10);
    buttonsizer->Add(new wxButton(this, SEND, _T("Send"), wxPoint(110,220), wxSize(75,30)));
    buttonsizer->AddSpacer(10);
    buttonsizer->Add(new wxButton(this, CLOSE, _T("Close"), wxPoint(190, 220), wxSize(75,30)));
    vertsizer->Add(buttonsizer, wxSizerFlags().Center());
    vertsizer->AddSpacer(15);

    horzsizer->AddSpacer(15);
    horzsizer->Add(vertsizer);
    horzsizer->AddSpacer(15);

    SetSizer(horzsizer);
    horzsizer->SetSizeHints(this);
}

ChatWindow::~ChatWindow()
{
	LOG_DEBUG("GUI: Closing ChatWindow with user " + std::string(mName.ToAscii()))
}

void ChatWindow::addMsg(const std::string &msg)
{
	LOG_DEBUG("Chat: Message from " + std::string(mName.ToAscii()) + ": " + msg)
    wxString text = _T("\n") + mName + _T(":") + std2wx(msg);
    mGet->SetInsertionPointEnd();
    mGet->AppendText(text);
    if (!IsActive())
    	RequestUserAttention();
}

void ChatWindow::addText(const std::string &text)
{
    wxString wtext = _T("\n*** ") + std2wx(text) ;
    mGet->SetInsertionPointEnd();
    mGet->AppendText(wtext);
    if (!IsActive())
    	RequestUserAttention();
}

void ChatWindow::disable()
{
    wxWindowList &list = GetChildren();
    for (wxWindowList::Node *node = list.GetFirst(); node; node = node->GetNext())
        if (node->GetData()->GetLabel() == _T("Send"))
            node->GetData()->Disable();
}

void ChatWindow::onSend(wxCommandEvent &event)
{
    if (mSend->GetValue().IsEmpty())
        return;
    OutStream out;
    out << ID_MESSAGE << mID << mSend->GetValue().ToAscii();
    THREAD->send(out);
    wxString text = _T("\n") + std2wx(wxGetApp().getOption("name")) + _T(": ") + mSend->GetValue();
    LOG_DEBUG("Chat: Sending Message to " + wx2std(mName) + ": " + wx2std(mSend->GetValue()))
    mGet->SetInsertionPointEnd();
    mGet->AppendText(text);
    mSend->Clear();
}

void ChatWindow::onClose(wxCloseEvent &event)
{
    mWindow->removeChat(this);
    Destroy();
}

}
