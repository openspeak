/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"

using namespace openSpeak;

BEGIN_EVENT_TABLE(GUI::FavoritesWindow, wxDialog)
    EVT_BUTTON  (GUI::FavoritesWindow::CONNECT , GUI::FavoritesWindow::onConnect)
    EVT_BUTTON  (GUI::FavoritesWindow::ADD , GUI::FavoritesWindow::onAdd)
    EVT_BUTTON  (GUI::FavoritesWindow::EDIT , GUI::FavoritesWindow::onEdit)
    EVT_BUTTON  (GUI::FavoritesWindow::REMOVE , GUI::FavoritesWindow::onRemove)
    EVT_BUTTON  (GUI::FavoritesWindow::CLOSE , GUI::FavoritesWindow::onClose)
    EVT_LISTBOX_DCLICK (GUI::FavoritesWindow::LISTBOX, GUI::FavoritesWindow::onConnect)
END_EVENT_TABLE()

namespace GUI
{

	FavoritesWindow::FavoritesWindow(wxFrame *parent)
	: wxDialog(parent, -1, _T("Favorites"), wxPoint(150,150), wxSize(375,259))
	{
		LOG_DEBUG("GUI: Opening FavoritesWindow")

	/* Create the main sizer */
		wxBoxSizer *sizer = new wxBoxSizer(wxHORIZONTAL);

	/* Create the sizer for the listbox */
		wxBoxSizer *listsizer = new wxBoxSizer(wxVERTICAL);

	/* Create the sizer for the buttons */
		wxBoxSizer *boxsizer = new wxBoxSizer(wxVERTICAL);

	/* Create all elements */
		mList = new wxListBox(this, LISTBOX, wxPoint(15,25), wxSize(250,210), 0, (const wxString*)"", wxLB_SINGLE | wxLB_SORT);
		listsizer->AddSpacer(25);
		listsizer->Add(mList);
		listsizer->AddSpacer(25);

		sizer->AddSpacer(15);
		sizer->Add(listsizer);

		boxsizer->AddSpacer(35);
		boxsizer->Add(new wxButton(this, CONNECT, _T("Connect"), wxPoint(280,40), wxSize(75,30)));

		boxsizer->AddSpacer(5);
		boxsizer->Add(new wxButton(this, ADD, _T("Add"), wxPoint(280,75), wxSize(75,30)));

		boxsizer->AddSpacer(5);
		boxsizer->Add(new wxButton(this, EDIT, _T("Edit"), wxPoint(280,110), wxSize(75,30)));

		boxsizer->AddSpacer(5);
		boxsizer->Add(new wxButton(this, REMOVE, _T("Remove"), wxPoint(280,145), wxSize(75,30)));

		boxsizer->AddSpacer(5);
		boxsizer->Add(new wxButton(this, CLOSE, _T("Close"), wxPoint(280,180), wxSize(75,30)));
		boxsizer->AddSpacer(15);

		sizer->AddSpacer(25);
		sizer->Add(boxsizer);
		sizer->AddSpacer(15);
		SetSizer(sizer);
		sizer->SetSizeHints(this);
		
	/* Add content to the ListBox */
		mFavMgr = new FavoritesMgr();	
		mFavMgr->initialize();
		FavoritesVector favs = mFavMgr->getFavorites();
		FavoritesVector::const_iterator it;
		for (it = favs.begin(); it != favs.end(); ++it)
		{
			mList->Append(std2wx((*it)->name), *it);
		}    
	}

	FavoritesWindow::~FavoritesWindow()
	{
		LOG_DEBUG("GUI: Closing FavoritesWindow")
		delete mFavMgr;
	}
	
	void FavoritesWindow::setFavorite(Favorite *fav)
	{
		mFavMgr->setFavorite(fav);
		// Iterate trough every item of the listbox and check if we got the favorite
		// then check name and change if necessary
		ushort i;
		wxString name = std2wx(fav->name);
		for (i = 0; i < mList->GetCount(); ++i)
		{
			Favorite *f = static_cast<Favorite*>(mList->GetClientData(i));
			if (f->id == fav->id)
			{
				if (mList->GetString(i) != name)
					mList->SetString(i, name);
			}
		}
	}

	void FavoritesWindow::onConnect(wxCommandEvent &event)
	{
		LOG_DEBUG("FavoritesWindow: Connecting to Favorite")
		int index = mList->GetSelection();
		if (index == wxNOT_FOUND)
		{
			wxMessageBox(_T("Nothing selected"), _T("ERROR!"), wxOK | wxICON_EXCLAMATION, NULL);
		}
		else
		{
			Favorite *fav = static_cast<Favorite*>(mList->GetClientData(index));
			if (!fav)
				return;
			THREAD->connect(fav->server, fav->port, wxGetApp().getOption("name"), fav->password);
			THREAD->enableVAD(openSpeak::StringUtils::toBool(wxGetApp().getOption("voice_activation")));
			Destroy();
		}
	}

	void FavoritesWindow::onAdd(wxCommandEvent &event)
	{
		LOG_DEBUG("FavoritesWindow: Adding Favorite")
		EditFavoritesDialog *dlg = new EditFavoritesDialog(false, this);
		dlg->Show(true);
	}

	void FavoritesWindow::onEdit(wxCommandEvent &event)
	{
		LOG_DEBUG("FavoritesWindow: Editing Favorite")
		int index = mList->GetSelection();
		if (index == wxNOT_FOUND)
		{
			wxMessageBox(_T("Nothing selected"), _T("ERROR!"), wxOK | wxICON_EXCLAMATION, NULL);
			return;
		}
		Favorite *fav = static_cast<Favorite*>(mList->GetClientData(index));
		if (!fav)
			return;
		EditFavoritesDialog *dlg = new EditFavoritesDialog(true, this, fav);
		dlg->Show(true);
	}

	void FavoritesWindow::onRemove(wxCommandEvent &event)
	{
		LOG_DEBUG("FavoritesWindow: Removing Favorite")
		int index = mList->GetSelection();
		if (index == wxNOT_FOUND)
		{
			wxMessageBox(_T("Nothing selected"), _T("ERROR!"), wxOK | wxICON_EXCLAMATION, NULL);
		}
		else
		{
			Favorite *fav = static_cast<Favorite*>(mList->GetClientData(index));
			if (!fav)
				return;
			mFavMgr->removeFavorite(fav->id);
			mList->Delete(index);
		}
	}

	void FavoritesWindow::onClose(wxCommandEvent &event)
	{
		Destroy();
	}

}
