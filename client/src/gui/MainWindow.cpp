/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "openSpeak.h"

#include <wx/aboutdlg.h>

BEGIN_EVENT_TABLE(GUI::MainWindow, wxFrame)
	EVT_MENU(GUI::MainWindow::QUIT, GUI::MainWindow::onBarExit)
	EVT_MENU(GUI::MainWindow::CONNECT, GUI::MainWindow::onConnect)
	EVT_MENU(GUI::MainWindow::FAVORITES, GUI::MainWindow::onFavorites)
	EVT_MENU(GUI::MainWindow::SETTINGS, GUI::MainWindow::onSettings)
	EVT_MENU(GUI::MainWindow::ABOUT, GUI::MainWindow::onAbout)
	EVT_MENU(GUI::MainWindow::CHAT, GUI::MainWindow::onChat)
	EVT_MENU(GUI::MainWindow::DISCONNECT, GUI::MainWindow::onDisconnect)
	EVT_CONTEXT_MENU(GUI::MainWindow::onContextMenu)
#ifdef OS_PLATFORM_WIN32
	EVT_CLOSE(GUI::MainWindow::OnClose)
#endif

/* Custom events */
    EVT_EXCEPTION(GUI::MainWindow::onException)
    EVT_ADDUSER(GUI::MainWindow::onAddUser)
    EVT_REMOVEUSER(GUI::MainWindow::onRemoveUser)
    EVT_CHANGEUSER(GUI::MainWindow::onChangeUser)
    EVT_MESSAGE(GUI::MainWindow::onMessage)
    EVT_CONNECT(GUI::MainWindow::onEvtConnect)
    EVT_DISCONNECT(GUI::MainWindow::onEvtDisconnect)
END_EVENT_TABLE()

using namespace openSpeak;

namespace GUI
{

MainWindow::MainWindow(const wxString &title)
: wxFrame((wxFrame *) NULL, -1, title, wxPoint(100,100), wxSize(300, 500))
{
	LOG_DEBUG("GUI: Opening MainWindow")
    mTree = new KeyTreeCtrl(this);
    mTree->setListener(this);
    mTree->AddRoot(_T("Connect to a server first"));
	EnableCloseButton(true);

/* Create the menu bar */
    mMenuBar = new wxMenuBar();
    mFileMenu = new wxMenu();
    mFileMenu->Append(CONNECT, _T("Quick &Connect"), _T("Connect to a server by entering ip and port"));
    mFileMenu->Append(FAVORITES, _T("&Favorites"), _T("Connect to one of your favorites"));
    mFileMenu->Append(SETTINGS, _T("&Settings"), _T("Change the settings"));
    mFileMenu->AppendSeparator();
    wxMenuItem *disconnect = mFileMenu->Append(DISCONNECT, _T("&Disconnect"), _T("Disconnect from the server"));
    disconnect->Enable(false);
    mFileMenu->AppendSeparator();
    mFileMenu->Append(QUIT, _T("&Quit"), _T("Quit the application"));
    mMenuBar->Append(mFileMenu, _T("&File"));

    wxMenu *help = new wxMenu();
    help->Append(ABOUT, _T("&About"), _T("About the application"));
    mMenuBar->Append(help, _T("&Help"));

    SetMenuBar(mMenuBar);

/* Create the status bar */
    CreateStatusBar(1);
    SetStatusText(_T("Ready ..."), 0);

    Layout();
}

MainWindow::~MainWindow()
{
	LOG_DEBUG("GUI: Closing MainWindow")
}

void MainWindow::OnClose(wxCloseEvent &event)
{
    wxGetApp().ExitMainLoop();
}

void MainWindow::onBarExit(wxCommandEvent &event)
{
	Close(true);
}

void MainWindow::onConnect(wxCommandEvent &event)
{
    GUI::ConnectDialog *dlg = new ConnectDialog;
    dlg->Show(true);
}

void MainWindow::onDisconnect(wxCommandEvent &event)
{
    THREAD->disconnect();
    if (!mChats.empty())
    {
        for (ChatWindowMap::const_iterator it = mChats.begin(); it != mChats.end(); ++it)
        {
            it->second->addText("Disconnected from the server");
            it->second->disable();
        }
    }
}

void MainWindow::onSettings(wxCommandEvent &event)
{
    GUI::SettingsDialog *dlg = new SettingsDialog;
    dlg->Show(true);
}

void MainWindow::onAbout(wxCommandEvent &event)
{
	LOG_DEBUG("Main: Showing About information")
    wxAboutDialogInfo info;
    info.SetName(_T("openSpeak"));
    info.SetVersion(_T("0.1 beta"));
    info.SetDescription(_T("A open source VoIP application aimed on gamers"));
    info.SetCopyright(_T("openSpeak is licensed under the GPL license. See COPYING for more informations.\nCopyright (C) 2006 - 2007 openSpeak Team"));
    info.AddDeveloper(_T("Philipp \"rmbl\" Gildein (rmbl@openspeak-project.org)"));
    info.AddDeveloper(_T("Daniel \"murray\" Wendt (murray@openspeak-project.org)"));
    info.AddDeveloper(_T("Guerraz \"kubrick\" François (kubrick@fgv6.net)"));
    info.AddDeveloper(_T("Lars \"Levia\" Wesselius (levia@openfrag.org)"));
    info.SetWebSite(_T("http://openspeak-project.org"));

    wxAboutBox(info);
}


void MainWindow::onFavorites(wxCommandEvent &event)
{
    FavoritesWindow *fav = new FavoritesWindow(this);
    fav->Show(true);
}

void MainWindow::onChat(wxCommandEvent &event)
{
    std::string name = std::string(mTree->GetItemText(mTree->GetSelection()).ToAscii());
    ushort id = UserMgr::getSingleton()->getUserID(name);
    if (id == 0)
    {
        wxMessageBox(_T("Error: Cannot get the id of the selected User!"), _T("ERROR!"), wxOK | wxICON_EXCLAMATION, NULL);
        return;
    }
    ChatWindow *chat = new ChatWindow(this, id, name);
    chat->Show(true);
    mChats.insert(std::make_pair(id, chat));
}

void MainWindow::onContextMenu(wxContextMenuEvent &event)
{
	LOG_DEBUG("Main: Showing context menu")
    wxPoint point = event.GetPosition();
    if (point.x == -1 && point.y == -1)
    {
        wxSize size = GetSize();
        point.x = size.x / 2;
        point.y = size.x / 2;
    }
    else
    {
        point = ScreenToClient(point);
    }

    wxMenu menu;

    wxTreeItemId sel = mTree->GetSelection();
    if (sel == mTree->GetRootItem())
    {
        menu.Append(CONNECT, _T("&Connect"));
        menu.Append(FAVORITES, _T("&Favorites"));
        if (THREAD->isConnected())
        {
            menu.AppendSeparator();
            menu.Append(DISCONNECT, _T("&Disconnect"));
        }
    }
    else if (mTree->GetItemText(sel) == std2wx(wxGetApp().getOption("name")))
    {
        menu.Append(SETTINGS, _T("&Settings"));
        if (THREAD->isConnected())
        {
            menu.AppendSeparator();
            menu.Append(DISCONNECT, _T("&Disconnect"));
        }
    }
    else
    {
        menu.Append(CHAT, _T("&Chat with the user"));
        if (THREAD->isConnected())
        {
            menu.AppendSeparator();
            menu.Append(DISCONNECT, _T("&Disconnect"));
        }
    }

    PopupMenu(&menu, point.x, point.y);
}

void MainWindow::onKeyDown(wxKeyEvent &event)
{
    if (!StringUtils::toBool(wxGetApp().getOption("voice_activation")) && event.GetKeyCode() == StringUtils::toInt(wxGetApp().getOption("key")))
    {
        THREAD->keyPressed();
    }
}

void MainWindow::onKeyUp(wxKeyEvent &event)
{
    if (!StringUtils::toBool(wxGetApp().getOption("voice_activation")) && event.GetKeyCode() == StringUtils::toInt(wxGetApp().getOption("key")))
    {
        THREAD->keyPressed(false);
    }
}

void MainWindow::onException(StringEvent &event)
{
    wxMessageBox(std2wx(event.getString()), _T("Exception!"), wxOK | wxICON_EXCLAMATION, this);
    refreshGUI();
}

void MainWindow::onEvtDisconnect(wxCommandEvent &event)
{
	if (!mChats.empty())
    {
        for (ChatWindowMap::const_iterator it = mChats.begin(); it != mChats.end(); ++it)
        {
            it->second->addText("Disconnected from the server");
            it->second->disable();
        }
    }
    refreshGUI();
}

void MainWindow::addUser(const std::string &name)
{
    mTree->Expand(mTree->AppendItem(mTree->GetRootItem(),std2wx(name)));
}

void MainWindow::removeUser(const std::string &name)
{
    mTree->Delete(getItemId(mTree->GetRootItem(),std2wx(name)));
    ushort id = UserMgr::getSingleton()->getUserID(name);
    ChatWindowMap::const_iterator it = mChats.find(id);
    if (it != mChats.end())
    {
        it->second->addText("The user disconnected!");
        it->second->disable();
    }
}

void MainWindow::changeUser(const std::string &name, const std::string &newname)
{
    mTree->SetItemText(getItemId(mTree->GetRootItem(),std2wx(name)), std2wx(newname));
}

void MainWindow::addMessage(const ushort &id, const std::string &msg)
{
    ChatWindowMap::const_iterator it = mChats.find(id);
    if (it != mChats.end())
    {
        it->second->addMsg(msg);
    }
    else
    {
        ChatWindow *chat = new ChatWindow(this, id, UserMgr::getSingleton()->getUser(id)->name);
        chat->Show(true);
        chat->addMsg(msg);
        mChats.insert(std::make_pair(id, chat));
    }
}

void MainWindow::removeChat(ChatWindow* win)
{
    ChatWindowMap::iterator it;
    for (it = mChats.begin(); it != mChats.end(); ++it)
    {
        if (it->second == win)
        {
            mChats.erase(it);
            break;
        }
    }
}

void MainWindow::changeRoot(const wxString &title)
{
    mTree->SetItemText(mTree->GetRootItem(), title);
}

void MainWindow::refreshGUI()
{
	LOG_DEBUG("Main: Refreshing the GUI")
    if (THREAD->isConnected())
    {
        if (!wxGetApp().getOption("name").empty() && getItemId(mTree->GetRootItem(), std2wx(wxGetApp().getOption("name"))) == wxTreeItemId())
            addUser(wxGetApp().getOption("name"));
        mFileMenu->Enable(DISCONNECT, true);
        SetStatusText(_T("Connected ..."), 0);
        changeRoot(_T("Connected"));
    }
    else
    {
    	mTree->DeleteAllItems();
    	mTree->AddRoot(_T("Connect to a server first"));
    	while (!mChats.empty())
    	{
        	//(mChats.begin()->second)->Destroy();
        	mChats.erase(mChats.begin());
    	}
        mFileMenu->Enable(DISCONNECT, false);
        SetStatusText(_T("Ready ..."), 0);
        changeRoot(_T("Connect to a server first"));
    }
}

wxTreeItemId MainWindow::getItemId(wxTreeItemId root, const wxString &title)
{
    wxTreeItemIdValue cookie;
	wxTreeItemId search, child;
	wxTreeItemId item = mTree->GetFirstChild( root, cookie );
	while( item.IsOk() )
	{
		wxString sData = mTree->GetItemText(item);
		if( title.CompareTo(sData) == 0 )
		{
			return item;
		}
		if( mTree->ItemHasChildren( item ) )
		{
			wxTreeItemId search = getItemId( item, title );
			if( search.IsOk() )
			{
				return search;
			}
		}
		item = mTree->GetNextChild( root, cookie);
	}

	/* Not found */
	wxTreeItemId dummy;
	return dummy;
}

}
