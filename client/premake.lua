--  openSpeak - The open source VoIP application
--  Copyright (C) 2006 - 2007  openSpeak Team (http://sourceforge.net/projects/openspeak/)
--
--  This program is free software; you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation; either version 2 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License along
--  with this program; if not, write to the Free Software Foundation, Inc.,
--  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
--

package = newpackage()
package.name = "openSpeak Client"
package.target = "openspeak"

if (linux) then
	package.kind = "exe"
elseif (windows) then
	package.kind = "winexe"
end

package.language = "c++"
package.files = {
	matchrecursive("include/*.h", "src/*.cpp")
}

package.includepaths = { "include", "../lib/include" }
package.libpaths = { "../libs" }

-- For Linux
if (linux) then

-- Check if wxw and gtk+-2.0 exists
	local wxwversion = os.execute("wx-config --version")
	if not wxwversion then
		print("openSpeak needs wxWidgets version 2.8.0")
	end
	local gtkversion = os.execute("pkg-config --exists gtk+-2.0")
	if not gtkversion then
		print ("openSpeak needs GTK+ 2.0")
	end

	package.config["Debug"].links = { "openspeak_d", "portaudio", "asound" }
	package.config["Debug"].buildoptions = { "`wx-config --unicode=no --cppflags` `pkg-config gtk+-2.0 --cflags` `pkg-config speex --cflags`" }
	package.config["Debug"].linkoptions = { "`wx-config --unicode=no --libs` `pkg-config gtk+-2.0 --libs` `pkg-config speex --libs`"}

	package.config["Release"].links = { "openspeak", "portaudio", "asound" }
	package.config["Release"].buildoptions = { "`wx-config --unicode=no --cppflags` `pkg-config gtk+-2.0 --cflags` `pkg-config speex --cflags`" }
	package.config["Release"].linkoptions = { "`wx-config --unicode=no --libs` `pkg-config gtk+-2.0 --libs` `pkg-config speex --libs`"}


-- For Windows
elseif (windows) then

	package.config["Debug"].links = { "openspeak_d", "portaudio_x86", "speex", "ws2_32", "comctl32", "rpcrt4", "wxbase28d", "wxmsw28d_core", "wxmsw28d_adv" }
	package.config["Release"].links = { "openspeak", "portaudio_x86", "speex", "ws2_32", "comctl32", "rpcrt4", "wxbase28", "wxmsw28_core", "wxmsw28_adv" }
	package.linkoptions = { "/LTCG /NODEFAULTLIB:LIBCMT /SUBSYSTEM:WINDOWS /ENTRY:WinMainCRTStartup" }

end

package.config["Debug"].buildflags = { "extra-warnings" }
package.config["Release"].buildflags = { "no-symbols", "optimize-speed", "no-frame-pointer" }

package.config["Debug"].objdir = "obj/client/debug"
package.config["Release"].objdir = "obj/client"
