--  openSpeak - The open source VoIP application
--  Copyright (C) 2006 - 2007  openSpeak Team (http://sourceforge.net/projects/openspeak/)
--
--  This program is free software; you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation; either version 2 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License along
--  with this program; if not, write to the Free Software Foundation, Inc.,
--  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
--

project.name = "openSpeak"
project.bindir = "bin"
project.libdir = "libs"

addoption("debug", "Sets Debug as standard configuration for us lazy devs")
addoption("serveronly", "Compiles only the libs needed for the server and the server itself")
addoption("clientonly", "Compiles only the libs needed for the client and the client itself")

if (options["debug"]) then
	project.configs = { "Debug", "Release" }
else
	project.configs = { "Release", "Debug" }
end

dopackage("lib")

if (not options["clientonly"]) then
	dopackage("server")
end

if (not options["serveronly"]) then
	dopackage("client")
end
