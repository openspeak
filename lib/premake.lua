--  openSpeak - The open source VoIP application
--  Copyright (C) 2006 - 2007  openSpeak Team (http://sourceforge.net/projects/openspeak/)
--
--  This program is free software; you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation; either version 2 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License along
--  with this program; if not, write to the Free Software Foundation, Inc.,
--  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
--

package = newpackage()
package.name = "openspeak"
package.kind = "lib"
package.language ="c++"
package.files = {
	matchrecursive("include/*.h", "src/*.cpp")
}

package.includepaths = { "include" }
package.config["Debug"].target = "openspeak_d"
package.config["Debug"].defines = { "OS_DEBUG" }
package.config["Release"].target = "openspeak"
package.config["Release"].buildflags = { "optimize-speed", "extra-warnings", "no-symbols", "no-frame-pointer" }


package.config["Debug"].objdir = "../obj/lib/debug"
package.config["Release"].objdir = "../obj/lib"
