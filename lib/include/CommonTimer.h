/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_COMMON_TIMER_H__
#define __OS_COMMON_TIMER_H__

#include "osLib.h"

namespace openSpeak
{


/** \class CommonTimer
 *	\brief Implements a simple timer based on the Clock class.
 *  
 *  The CommonTimer allows to get its current running time, the time of the last timeframe and pause the timer.
 *	It's getting the current time from the Clock class which it can create if no Clock is supplied.
 */
class CommonTimer : public Observer
{
 public:
	/** \brief Constructor of the CommonTimer class
	 *	\param clock The Clock to get the current time from or 0 to create a new one
	 */
    CommonTimer(Clock *clock = 0);

	/** \brief Destructor of the CommonTimer class */
    virtual ~CommonTimer(void);

	/** \brief Get a notification from the Clock and update the time */
    virtual void notify(void);

	/** \brief Get the current time */
    inline double getTime(void) { return mCurrentTime; }

	/** \brief Get the duration of the last timeframe */
    inline double getFrameTime(void) { return mFrameTime; }

	/** \brief Reset the time to zero */
    inline void reset(void) { mStartTime = mClock->getTime(); mCurrentTime = 0.0; mFrameTime = 0.0; mLastTime = 0.0; }

	/** \brief Pause the timer */
    void pause(void);

	/** \brief Resume the timer after pausing it */
    void resume(void);

 protected:
	/** \brief The time we started */
    double mStartTime;
	/** \brief The current running time */
	double mCurrentTime;
	/** \brief The last time we got */
	double mLastTime;
	/** \brief The duration of the last timeframe */
	double mFrameTime;

	/** \brief The Clock were getting the time from */
    Clock *mClock;

	/** \brief True if we created a new clock and need to clean up */
    bool mCreated;

	/** \brief True if the timer is paused */
    bool mPaused;
};

}

#endif
