/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_CLOCK_H__
#define __OS_CLOCK_H__

#include "osLib.h"

namespace openSpeak
{

/** \brief A std::vector to save Observer's */
typedef std::vector<Observer*> ObserverVector;

/** \class Clock
 *  \brief The main timesource
 *
 *  The Clock class provides timer with the time since the clock has been started. One instance can handle an unlimited amount of timer (called Observer).
 */
class Clock
{
 public:
	/** \brief The void constructor for the Clock class
	 */ 
    Clock(void);
	/** \brief The void deconstructor for the Clock class
	 */
    ~Clock(void);

	/** \brief Updates the clock and the observers
	 *
	 *  When calling makeStep(), the Clock gets the current time and notifies all observers to get the new time
	 */
    void makeStep(void);

	/** \brief Get the current time of the Clock 
	 *  
	 *  Get the newest time calculated in makeStep()
	 *
	 *  \return The current time
	 */
    inline double getTime(void) { return mCurrentTime; }

	/** \brief Add an Observer to the list 
	 *  \param observ Requires a valid Observer object
	 * */
    inline void addObserver(Observer *observ) { if (observ) mObservers.push_back(observ); }

	/** \brief Remove an Observer from the list
	 *  \param observ Requires a valid Observer object
	 * */
    void removeObserver(Observer *observ);

 protected:
	/** \brief The current time of the Clock */
    double mCurrentTime;

#if defined( OS_PLATFORM_WIN32 )
	/** \brief The start time for Windows users */
    LARGE_INTEGER                            mStart;
	/** \brief The frequenzy to calculate the current time for Windows users */
    LARGE_INTEGER                            mFrequency;
#elif defined( OS_PLATFORM_LINUX )
	/** \brief The start time for Linux users */
    struct timeval                           mStart;
#endif

	/** \brief All observers are saved here */
    ObserverVector mObservers;
};

}

#endif
