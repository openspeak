/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_OBSERVER_H__
#define __OS_OBSERVER_H__

#include "osLib.h"

namespace openSpeak
{

/** \class Observer
 *	\brief Abstract class to retrieve notices about an updated time
 *
 *	The Observer class allows to register a class with a Clock and get a notification if the time got updated (Clock::makeStep()).
 *	\sa CommonTimer
 */
class Observer
{
 public:
	/** \brief The virtual destructor of the Observer class */
    virtual ~Observer() { }

	/** \brief Gets called if the time of the clock got updated */
    virtual void notify(void) = 0;
};

}

#endif
