/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __LIBOS_LOG_H__
#define __LIBOS_LOG_H__

#include "osLib.h"

namespace openSpeak
{

/** \class Log
 *	\brief Logs given output to stderr and to a file
 *
 *	The Log class logs every message given to a file and (if needed) to stderr.
 */
class Log : public Singleton<Log>
{
 public:
	/** \brief The level of the log msg */
    enum Level
    {
    	LVL_SILENT,
        LVL_DEBUG,
        LVL_WARNING,
        LVL_INFO,
        LVL_USER,
        LVL_ERROR,
        LVL_FATAL
    };

	/** \brief The constructor of the Log class
	 *	\param filename The file we should log to
	 *	\param silent Determines if we are generally not logging to stderr
	 */
    Log(const std::string &filename, const bool &silent = false);

	/** \brief The destructor of the Log class */
    ~Log(void);

	/** \brief Log a message into file/to console
	 *	\param msg The message we want to log
	 *	\param lvl The level of the message
	 */
    void logMsg(const std::string &msg, const int &lvl = LVL_INFO);

	/** \brief Get the level as a string
	 * 	\param lvl The level to convert
	 *	\return String with the level
	 */
    static std::string levelToString(const int &lvl);

 protected:
	/** \brief The output stream */
    std::ofstream mFile;
        
	/** \brief Are we writing to stderr */
    bool mSilent;

};
/** \brief Log with level INFO (only appears in debug mode) */
#define LOG(x) openSpeak::Log::getSingleton()->logMsg(x, Log::LVL_INFO);
/** \brief Log with level SILENT (not to stderr) */
#define LOG_SILENT(x) openSpeak::Log::getSingleton()->logMsg(x, Log::LVL_SILENT);
/** \brief Log with level DEBUG (only appears in debug mode) */
#define LOG_DEBUG(x) openSpeak::Log::getSingleton()->logMsg(x, Log::LVL_DEBUG);
/** \brief Log with level WARNING (only appears in debug mode) */
#define LOG_WARNING(x) openSpeak::Log::getSingleton()->logMsg(x, Log::LVL_WARNING);
/** \brief Log with level INFO (only appears in debug mode) */
#define LOG_INFO(x) openSpeak::Log::getSingleton()->logMsg(x, Log::LVL_INFO);
/** \brief Log with level USER (only appears in debug mode) */
#define LOG_USER(x) openSpeak::Log::getSingleton()->logMsg(x, Log::LVL_USER);
/** \brief Log with level ERROR */
#define LOG_ERROR(x) openSpeak::Log::getSingleton()->logMsg(x, Log::LVL_ERROR);
/** \brief Log with level FATAL */
#define LOG_FATAL(x) openSpeak::Log::getSingleton()->logMsg(x, Log::LVL_FATAL);


}

#endif
