/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */
  
/**
  *  Written by François "Kubrick" GUERRAZ (kubrick_at_fgv6_dot_net) for the openSpeak Project.
  */

#ifndef __OS_CMD_LINE_PARSER_H__
#define __OS_CMD_LINE_PARSER__

namespace openSpeak
{

/** \brief Different types of commandline arguments */
enum Arguments
{ 
        OPTION_HELP,      	/**< Show the help */
        OPTION_VERSION,		/**< Show version information */
        OPTION_ARG_NONE,  	/**< This Command Line Option doesn't take any argument */
        OPTION_ARG_STRING,	/**< It takes a string argument */
        OPTION_ARG_INT 		/**< It takes a int argument */
};  

/** \brief The format of the option */
enum Form
{
		OPTION_FORM_SHORT,		/**< We are parsing a short-formated option, ex : "-h" */
        OPTION_FORM_LONG, 		/**< We are parsing a long-formated option, ex : "--help" */
        OPTION_FORM_UNKNOWN 	/**< The argument is not a valid option (doesn't begin with "-" or "--") */
}; 

/** \brief Some error codes */
enum Errors
{
		SUCCESS,			/**< Everything is OK */
        ERROR_OPT_UNDEF,	/**< The command line contains unreferenced option */
        ERROR_ARG_WRONG,	/**< The option parameter is incorrect */
        ERROR_PARSE_ERR 	/**< The command line is not parsable */
};

/** \brief Define the Configuration Map type to backup parsed parameters  */
typedef std::map< std::string, std::string > ConfigMap;

/** \brief Define a valid cmdlineoption with this struct */
struct CmdLineOption 
{
    const char* Long;           /**< Ex: "max-connections" */
    char Short;             	/**< Ex: 'm' */
    int Type;               	/**< Ex: C_OPTION_ARG_INT */
    void* Variable;         	/**< Ex: &o_max_connections (where o_max_connections is an integer) */
    const char* HelpText;      	/**< Ex: "Specifies the maximum number of connexions allowed on the server" */
    const char* ArgHelper;      /**< Ex: "N" */
};

/** \brief Linked list to save CmdLineOption's */
struct CmdLineOptionsSet
{
    struct CmdLineOption Option;	/**< The CmdLineOption for this set */
    struct CmdLineOptionsSet* Next; /**< The next set in the list */
};

/** \class CmdLineParser
 *  \brief Class to parse the commandline arguments
 *
 *	This class parses the given commandline (argc, argv) and returnes given parameters.
 *  */
class CmdLineParser 
{
 public:
	/** \brief The constructor of the CmdLineParser class
	 *	\param apptitle The title of the application using the CmdLineParser class
	 *	\param version The version number of the application
	 * */
	CmdLineParser(const std::string &apptitle, const std::string &version);

	/** \brief The void destructor of the CmdLineParser class */
    ~CmdLineParser(void);
        
	/** \brief Add a single CmdLineOption by giving all parameters. */ 
    void addOption(const char* Long, char Short, int Type, void* Variable, const char* HelpText, const char* ArgHelper) { CmdLineOption opt = {Long, Short, Type, Variable, HelpText, ArgHelper}; addOption(opt);  }

	/** \brief Add a single CmdLineOption by copying.
	 *
	 *  Add the Option to the _optionsSet linked list.
	 *	This is the "generic" addOption method, other overloads rely on this one.
	 *	\param Option The CmdLineOption to add
	 * */
    void addOption(CmdLineOption Option);

	/** \brief Add multiple CmdLineOption's through an array
	 *
	 *	Allow many options to be added in one time.
	 *	An array, ending with a { NULL } element must be given, see this example :
	 *	\code
	 *	CmdLineOption ServerOptions[] = {
	 *		{"help", 'h', C_OPTION_HELP, NULL, "Print this help message", ""},
	 *		{"daemon", 'd', C_OPTION_ARG_NONE, &daemon, "Use this option to make the server fork to the background", ""},
	 *		{"listen", 'l', C_OPTION_ARG_INT, &listen_port, "Specifies on which port the server is listening (Not Implemented Option)", "N"},
	 *		{NULL}
	 *	};
	 *	\endcode
	 *	This function relies on addOption (COption Option)
	 *	\param Option An array of CmdLineOption's
	 * */
    void addOption(CmdLineOption* Option);

	/** \brief Parse the given commandline
	 *	\param argc The number of arguments
	 *	\param argv All arguments
	 * */
    int parseLine(int argc, char* argv[]);

	/** \brief Get a boolean value for an option
	 *	\param option The option to get the value from
	 *	\return Returns if the option is true or
	 * */
    bool getSwitch(const std::string &option);

	/** \brief Get the string value for an option
	 *  \param option The option to get the value from
	 *  \return Returns the string given to the commandline or an empty string
	 */
    std::string getStringValue(const std::string &option);

	/** \brief Get the integer value for an option
	 *  \param option The option to get the value from
	 *  \return Returns the integer given to the commandline
	 */
    int getIntValue(const std::string &option);

 protected:
	/** \brief Check which format the argument has 
	 *  \param arg The argument to check
	 *	\return The format of the argument
	 */
    int getArgFormat(char* arg);

	/** \brief Parses the argument on Position
	 *  \param argc The number of arguments
	 *  \param argv Array with all arguments
	 *  \param Position Position of the argument to parse
	 *  \return 0 on success or an errorcode
	 */
    int parseArgument(int argc, char* argv[], int* Position);

	/** \brief Check if the long option (--option) exists
	 *  \param s_Option The option as a string
	 *  \return The corresponding CmdLineOption or 0
	 */
    CmdLineOption* searchLongOption(const std::string &s_Option);

	/** \brief Check if the short option (-o) exists
	 *	\param c_Option The option as a char
	 *	\return The corresponding CmdLineOption or 0
	 */
    CmdLineOption* searchShortOption(char c_Option);

	/** \brief Show the help text (--help)
	 *  \param command The command with which the program got called
	 */
    void showHelp(char* command);

	/** \brief Show some version informations (--version) */	 
    void showVersion(void);

	/** \brief Search in the map with all options for a specific one
	 *	\param option The option to search for
	 *	\return The value of the option or an empty string
	 */
    std::string searchInMap(const std::string &option);

    std::string mApplicationTitle;   /**< Contains the application's title */
    std::string mApplicationVersion; /**< Contains the application's version */
    CmdLineOptionsSet* mOptionsSet;  /**< Contains the options */
    CmdLineOptionsSet* mLastOption;  /**< Contains the lastest options */
    ConfigMap mOptions;              /**< The commandline options get saved here */
};

}

#endif
