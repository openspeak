/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __LIBOS_UDP_PACKET_ENUM_H__
#define __LIBOS_UDP_PACKET_ENUM_H__

#include "osLib.h"

namespace openSpeak
{

/** /brief A version number to check if the protocol has the correct version
 *      
 *  This number should be increased whenever anything gets changed in UDPPacketEnum.h
 */
    const ushort OS_PROTOCOL_VERSION = 13;

/** \brief The different packet types we got */
enum PacketTypes
{
        ID_CHALLENGE = 0,   // Send to server on connect
        ID_WELCOME,     // Response by the server on ID_CHALLENGE
        ID_REGISTER,    // Register on the server with a specific nick
        ID_OK,          // Registration successful
        ID_ERROR,       // Registration failed
        ID_NEW_USER,
        ID_DISCONNECTING_USER,
        ID_NUMBER_OF_USERS,
        ID_USER,
        ID_NEED_PASSWORD,
        ID_PASSWORD,
        ID_VERSION,
        ID_SERVERSETTING,
        ID_FINISHED_CONNECTING,
        ID_DISCONNECTING,
        ID_SERVER_CLOSING,
        ID_CHANGE,
        ID_PING,
        ID_PONG,
        ID_SOUND,
        ID_MESSAGE,
        ID_STOP_SOUND
};

/** \brief Messages for some packets (like ID_OK ID_ERROR ..) */
enum Message
{
        UNKNOWN_MESSAGE = 0,
        OKAY,
        NICK_ACCEPTED ,
        NICK_IN_USE,
        PASSWORD,
        NOT_REGISTERED,
        ALREADY_REGISTERED,
        REGISTRATION,
        SERVER_FULL,
        VERSION_MISMATCH,
        GOT_DISCONNECTED
};

/** \brief Types for ID_CHANGE */
enum Changes
{
    NICK,
    STATUS,
    SERVERSETTING
};

/** \brief Settings the server can send to us */
enum ServerSettings
{
    QUALITY = 0
};

}

#endif
