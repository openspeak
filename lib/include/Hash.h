/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */


/**
  *     This class is based on the CHash class by .rich.w. (see http://www.codeproject.com/cpp/chash.asp).
  *     Thanks to Dr. Brian Gladman for his excellent SHA-2 implementation.
  */


#ifndef __OS_HASH_H__
#define __OS_HASH_H__

#include "osLib.h"

namespace openSpeak
{

/** \class Hash
 *	\brief Creates a SHA-256 hash of a given string
 *
 *	The Hash function creates a hash of a string using the SHA-256 algorithm.
 *	\sa sha2.cpp
 */
class Hash
{
public:
	/** \brief The void constructor of the Hash class */
	Hash(void);

	/** \brief The string constructor of the Hash class
	 *	\param base The string to create a hash from
	 */
	Hash(const std::string &base);

	/** \brief The destructor of the Hash class */
	~Hash(void);

	/** \brief Get the hash of the string
	 *	\return String containing the hash
	 */
	std::string getHash(void);

	/** \brief Set the string that should be hashed
	 *  \param base The string to use
	 */
	void setString(const std::string &base) { mString = base;  }

protected:
	/** \brief Create a SHA-256 hash
	 *	\return String containing the created hash
	 */
    std::string SHA2Hash();

	/** \brief The string we want to parse */
	std::string mString;
};

}

#endif
