/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_STRING_UTILS_H__
#define __OS_STRING_UTILS_H__

#include "osLib.h"

namespace openSpeak
{
    namespace StringUtils
    {
		/** \brief Convert any type to a std::string
		 *	\param t The variable we want to convert
		 *	\return String with the variable
		 */
        template < typename T > std::string toString(const T &t)
        {
            std::stringstream tmp;
            tmp << t;
            return tmp.str();
        }

		/** \brief Convert any string to an uppercase one
		 *	\param str The string to convert
		 *	\return The converted string
		 */
        std::string toUpper(std::string &str);

		/** \brief Convert any string to an uppercase one
		 *	\param str The string to convert
		 *	\return The converted string
		 */
        std::string toUpper(const std::string &str);

		/** \brief Convert any string into a boolean variable
		 *	\param str The string to convert
		 *	\return False if the string is false, 0 or empty
		 */
        bool toBool(const std::string &str);

		/** \brief Convert any string into an integer variable
		 *	\param str The string to convert
		 *	\return The converted integer value
		 */
        int toInt(const std::string &str);

		/** \brief Convert any string into an unsigned short variable
		 *	\param str The string to convert
		 *	\return The converted unsigned short value
		 */
        ushort toUShort(const std::string &str);

        /** \brief Convert any string into a long variable
         * 	\param str The string to convert
         * 	\return The converted long value
         */
        long toLong(const std::string &str);
        
		/** \brief Convert any string into a floating point variable
		 *	\param str The string to convert
		 *	\return The converted floating point value
		 */
        float toFloat(const std::string &str);

		/** \brief Split a string at a given char in as many pieces as possible
		 *	\param str The string to split
		 *	\param where The char where we split
		 *	\return A vector with all string fragments
		 */
		std::vector< std::string > split(const std::string &str, char where = ' ');
    }

    
	/** \brief Compare two sockaddr_in's
	 * 	\param s1 The first socketaddress
	 * 	\param s2 The second socketaddress
	 * 	\return True if both sockaddress's are the same
	 */
	bool sockcmp(const sockaddr_in &s1, const sockaddr_in &s2);
	
	/** \struct compSockaddr
	 *	\brief Helper struct to check if two sockaddr_in's are the same
	 *
	 *	This struct is used by std::map to save values with a key_type of sockaddr_in's.
	 */
	struct compSockaddr
	{
		/** \brief operator() function needed by the std::map
		 *	\param s1 The first sockaddr_in to compare
		 *	\param s2 The second sockaddr_in to compare
		 *	\return True if the first sockaddr_in is less the second one
		 */
		bool operator()(const sockaddr_in &s1, const sockaddr_in &s2) const
		{
			if (s1.sin_addr.s_addr == s1.sin_addr.s_addr)
			{
				std::less<unsigned short> lessport;
				return lessport(s1.sin_port, s2.sin_port);
			}
			else
			{
				std::less<unsigned long> lessaddr;
				return lessaddr(s1.sin_addr.s_addr, s2.sin_addr.s_addr);
			}
		}
	};
}

#endif // STRINGUTILS_H
