/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __LIBOS_CONFIG_H__
#define __LIBOS_CONFIG_H__

#include "osLib.h"

namespace openSpeak
{
	/** \brief The map where we're saving all options in the config */
    typedef std::map< std::string, std::string > ConfigMap;

	/** \class Config
	 *	\brief Opens a XML config file and parses it
	 *
	 *	The Config class opens a XML file and parses all of its nodes.
	 *	New options or changed options get saved back to the file after destroying the class or setting the option.
	 */
    class Config
    {
      public:
		/** \brief The constructor of the Config class
		 *	\param filename The file we want to parse
		 *	\param newfile Should we create the file first?
		 */
        Config(const std::string &filename = "", bool newfile = false);

		/** \brief The destructor of the Config class */
        ~Config(void);

		/** \brief Set the filename of the config and parse it
		 *	\param filename The file we want to parse
		 */
        void setFilename(const std::string &filename);

		/** \brief Create a new configfile
		 *	\param filename The file we want to create
		 */
		void createFile(const std::string &filename = "");

		/** \brief Parse the config file
		 *	\return False if a new file had to be created and we can now add standard options 
		 */
        bool parse(void);

		/** \brief Save all options into the configuration file */
        void save(void);

		/** \brief Get the value of an option from the config file
		 *	\param option The option we want the value from
		 *	\return String with the value
		 */
        std::string getOption(const std::string &option) { if (optionExists(option)) return mOptions[option]; else return ""; }

		/** \brief Set an option in the config file
		 *	\param option The option we want to change/add
		 *	\param value The value of the option
		 */
        void setOption(const std::string &option, const std::string &value);

		/** \brief Check a list of options for their existance
		 *
		 *	Be sure to add a "" as the last option when calling checkOptions() (ex: checkOptions("hi","hello",""))
		 *	\param option The first option 
		 *	\param ... All other options you want to check
		 *	\return Returns true if all options do exist
		 */
		bool checkOptions(const char* option, ...);

		/** \brief Check for the existance of one option
		 *	\param option The option to check
		 *	\return Returns true if the option exists
		 */
        bool optionExists(const std::string &option);

      protected:
		/** \brief If an XML element got several subnodes, this function is parsing them.
		 *	\param node The TiXmlNode which subnodes should get parsed
		 *	\param parentname The name of the parentnode to name config options (parentname.option)
		 */
        void _parseNode(TiXmlElement* node, const std::string &parentname = "");

		/** \brief If an XML element got several subnodes, this function is saving them.
		 *	\param node The TiXmlNode which subnodes should get saved
		 *	\param parentname The name of the parentnode to save config options correctly
		 *	\see openSpeak::Config::_parseNode
		 */
        void _saveNode(TiXmlElement* node, const std::string &parentname = "");

		/** \brief The map which contains all options */
        ConfigMap mOptions;

		/** \brief The XML document we're reading/writing */
        TiXmlDocument *mDoc;

		/** \brief The name of the current file */
        std::string mFile;

		/** \brief True if the file has already been parsed */
        bool mParsed;
    };

}

#endif
