/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __LIBOS_EXCEPTION_H__
#define __LIBOS_EXPEPTION_H__

#include "StringUtils.h"

namespace openSpeak
{
	/** \class Exception
	 *	\brief Provides an easy way to throw exceptions with some informations
	 *
	 *	The Exception class is meant as an easy way to throw exceptions on errors with the help of the EXCEPTION() macro.
	 *	You can get the message of the exception and print it by hand or let the class do everything.
	 */
	class Exception
	{
	 public:
		/** \brief The constructor of the Exception class
		 *	\param msg The message/error of the exception
		 *	\param file The file in which the exception got thrown
		 *	\param line The line in the file where the exception got thrown
		 */
		Exception(const std::string &msg, const std::string &file, const ushort &line) : mMessage(msg), mFile(file), mLine(line) { }

		/** \brief The destructor of the Exception class */
		~Exception(void) { } 

		/** \brief Print the exception to stderr */
		inline void print(void) { std::cerr << getException() << "\n"; }

		/** \brief Return a nice string to the user containing the same data as the print() function does print
		 *	\return Returns a string with all available information
		 */
		inline std::string getException(void) { return "=== Exception throw ===\n" + mFile + ":" + StringUtils::toString(mLine) + " : " + mMessage + "\n"; }

		/** \brief Get only the message/error of the exception
		 *	\return String with the message
		 */
		inline std::string getMessage(void) { return mMessage; }

		/** \brief Get only the filename of the exception
		 *	\return String with the filename
		 */
		inline std::string getFile(void) { return mFile; }

		/** \brief Get only the line of the exception
		 *	\return Unsigned short with the line number
		 */
		inline ushort getLine(void) { return mLine; }
	 protected:
		std::string mMessage;	/**< The message */
		std::string mFile;		/**< The filename */
		ushort mLine;			/**< The line number */
	};

/** \brief A helper macro to ease the use of the exception class */
#define EXCEPTION(x) throw openSpeak::Exception(x, __FILE__, (ushort)__LINE__);

}

#endif
