/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __LIB_OPENSPEAK_H__
#define __LIB_OPENSPEAK_H__

/** Include Platform.h for some platform specific defines */
#include "Platform.h"

/** Some generic headers available on all systems (C++ STL/std C) */
#include <vector>
#include <map>
#include <queue>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cctype>
#include <time.h>
#include <limits.h>
#include <assert.h>

/** Include platform specific headers */
#ifdef OS_PLATFORM_WIN32
#   include <winsock.h>
#else
#   include <unistd.h>
#   include <errno.h>
#   include <sys/types.h>
#   include <sys/socket.h>
#   include <sys/time.h>
#   include <netinet/in.h>
#   include <arpa/inet.h>
#   include <netdb.h>
#	include <fcntl.h>
#endif

typedef unsigned char uchar;
typedef unsigned short int ushort;
typedef unsigned long int ulong;
typedef unsigned int uint;

namespace openSpeak
{
    class Clock;
    class CmdLineParser;
    class Config;
    class Exception;
    class Listener;
    class Log;
    class Observer;
        class CommonTimer;
    class OutStream;
    template<typename T> class Singleton;
    class TimeSource;
    class UDPConnection;
    class Hash;

    struct Packet;
}

/** The tinyxml stuff */
#include "tinyxml/tinyxml.h"

/** Abstract classes & enums */
#include "Observer.h"
#include "Listener.h"
#include "Singleton.h"
#include "UDPPacketEnum.h"

/** Usable Classes */
#include "Clock.h"
#include "CmdLineParser.h"
#include "CommonTimer.h"
#include "Config.h"
#include "Exception.h"
#include "Log.h"
#include "OutStream.h"
#include "StringUtils.h"
#include "UDPConnection.h"

/** The SHA 256 algorithm */
#include "Hash.h"
#include "sha2.h"

#endif
