/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#ifndef __OS_OUT_STREAM_H__
#define __OS_OUT_STREAM_H__

#include "osLib.h"

namespace openSpeak
{

/** \class OutStream
 *	\brief A wrapper class for a std::stringstream
 *
 *	The OutStream class eases the use of a std::stringstream which is used to create outgoing packets.
 *	It automatically adds spaces between several items in the stream and converts the type to std::string.
 */
class OutStream
{
 public:
	/** \brief The constructor of the OutStream class */
	OutStream(void) { }

	/** \brief The copyconstructor of the OutStream class */
	OutStream(const OutStream &out) { mStream.str(out.mStream.str()); }

	/** \brief The destructor of the OutStream class */
	~OutStream(void) { }

	/** \brief Add a new item to the stringstream
	 *	\param toadd Any item to add	 
	 *	\return A reference to itself to allow many adds behind each other (.add(this).add(that))
	 */
	template<typename T> OutStream& add(T toadd)
	{
        if (asString().empty())
            mStream << toadd;
        else
            mStream << " " << toadd;
		return *this;
	}

	/** \brief Specific add() for enums
	 *	\param toadd Any enum to add
	 *	\return A reference to itself to allow many adds behind each other (.add(this).add(that))
	 */
	template<PacketTypes> OutStream& add(PacketTypes toadd)
	{
	    if (asString().empty())
            mStream << (ushort)toadd;
        else
            mStream << " " << (ushort)toadd;
        return *this;
	}

	/** \brief Provides operator << for the add() function
	 *	\param toadd Any item to add
	 *	\return A reference to itself to allow many adds behind each other (.add(this).add(that))
	 */
	template<typename T> OutStream& operator<<(T toadd) { return add(toadd); }

	/** \brief Reset the stream */
	void reset(void)
	{
        mStream.str("");
        mStream.clear();
	}

	/** \brief Get a string with the contents of the stringstream
	 *	\return A string with the contents of the stringstream
	 */
	inline std::string asString(void) { return mStream.str(); }
 protected:
	std::stringstream mStream;	/**< The stringstream we're adding data to */
};

}

#endif

