/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "osLib.h"

namespace openSpeak
{

    Clock::Clock()
    {
        mCurrentTime = 0.0;
	/* Initialize the start time */
#if defined ( OS_PLATFORM_WIN32 )	
        if (QueryPerformanceFrequency(&mFrequency))
            QueryPerformanceCounter(&mStart);
#elif defined ( OS_PLATFORM_LINUX )
        gettimeofday(&mStart, NULL);
#endif
    }

    Clock::~Clock()
    {

    }

    void Clock::makeStep()
    {
    /* Get the new time and notify all observers */
#if defined( OS_PLATFORM_WIN32 )
        LARGE_INTEGER now;
        QueryPerformanceCounter(&now);
        mCurrentTime = ((double)((double)now.QuadPart - (double)mStart.QuadPart) /  (double)mFrequency.QuadPart);
#elif defined( OS_PLATFORM_LINUX )
        struct timeval now;
        gettimeofday(&now, NULL);
        mCurrentTime = (double)(now.tv_sec - mStart.tv_sec) + (double)((double)(now.tv_usec - mStart.tv_usec) / (double)1e6);
#endif
        for (ObserverVector::const_iterator it = mObservers.begin(); it != mObservers.end(); ++it)
            (*it)->notify();
    }

    void Clock::removeObserver(Observer *observ)
    {
        if (!observ) return;

    /* Search for the observer and remove him if found */
        for (ObserverVector::iterator it = mObservers.begin(); it != mObservers.end(); ++it)
        {
            if (*it == observ)
            {
                mObservers.erase(it);
                break;
            }
        }
    }

}
