/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "osLib.h"

namespace openSpeak
{

Hash::Hash()
{
    mString = "";
}

Hash::Hash(const std::string &base)
{
    mString = base;
}


Hash::~Hash()
{

}

std::string Hash::getHash()
{
    if (mString.empty())
        return "";
	std::string tempHash = "";

/* Create the hash */
	tempHash = SHA2Hash();

/* Make it a little bit better looking ;) */
    std::transform(tempHash.begin(), tempHash.end(), tempHash.begin(), toupper);

	return tempHash;
}

std::string Hash::SHA2Hash()
{
	std::string outHash;

	uchar* tempOut = new uchar[256];

	sha2_ctx sha;

/* Work out hash length */
	int hashLength = 256 / 8;

/* Hashing a string, open the object, give it the text, and finalise it */
	sha2_begin(256, &sha);
	sha2_hash(reinterpret_cast<uchar *>(const_cast<char*>(mString.c_str())), (ulong)mString.size(), &sha);
	sha2_end(tempOut, &sha);

/* Create a good looking string */
	for (int i = 0; i < hashLength; i++)
	{
		char tmp[3];
		sprintf(tmp,"%x",tempOut[i]);
		if (strlen(tmp) == 1)
		{
			tmp[1] = tmp[0];
			tmp[0] = '0';
			tmp[2] = '\0';
		}
		outHash += tmp;
	}

	delete[] tempOut;

	return outHash;
}

}
