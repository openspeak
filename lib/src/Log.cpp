/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "osLib.h"

namespace openSpeak
{

Log::Log(const std::string &filename, const bool &silent) : mSilent(silent)
{
/* Open the file for writing */
    mFile.open(filename.c_str());
}

Log::~Log()
{
/* Close the file */
    mFile.flush();
    mFile.close();
}

void Log::logMsg(const std::string &msg, const int &lvl)
{
/* Get the current time */
    time_t rawtime;
    char buffer [80];
    struct tm * timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime (buffer,80,"[%x %X]",timeinfo);

/* Write it to file including the message */
	mFile << buffer;
    mFile << " [" << levelToString(lvl) << "] " << msg << "\n";
    mFile.flush();

/* If the level is high enough also print it to console */
#ifdef OS_DEBUG
    if ((!mSilent && lvl != LVL_SILENT) || (mSilent && lvl >= LVL_ERROR))
#else
	if ((!mSilent && lvl > LVL_USER) || (mSilent && lvl >= LVL_ERROR))
#endif
        std::cerr << "[" << levelToString(lvl) << "] " << msg << std::endl;
}

std::string Log::levelToString(const int &lvl)
{
/* Return a string depending on the level */
    switch (lvl)
    {
    	case LVL_SILENT: return std::string("SILENT");
        case LVL_DEBUG: return std::string("DEBUG");
        case LVL_WARNING: return std::string("WARNING");
        case LVL_INFO: return std::string("INFO");
        case LVL_USER: return std::string("USER");
        case LVL_ERROR: return std::string("ERROR");
        case LVL_FATAL: return std::string("FATAL");
        default: return std::string("DEFAULT");
    }
}

}
