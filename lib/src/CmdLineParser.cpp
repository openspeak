/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */
 
/**
  *  Written by François "Kubrick" GUERRAZ (kubrick_at_fgv6_dot_net) for the openSpeak Project.
  */

#include "osLib.h"

namespace openSpeak
{

CmdLineParser::CmdLineParser(const std::string &apptitle, const std::string &version)
{
    if (apptitle.empty() || version.empty())        
        EXCEPTION("CmdLineParser has no apptitle or version supplied");

/* Initialize variables */
    mApplicationTitle = apptitle;
    mApplicationVersion = version;
    mOptionsSet = 0;
    mLastOption = 0;
}


CmdLineParser::~CmdLineParser()
{
/* Delete all options */
    CmdLineOptionsSet* temp;
    while (mOptionsSet != 0)
    {
        temp = mOptionsSet;
        mOptionsSet = mOptionsSet->Next;
        delete temp;
    }
}


void CmdLineParser::addOption (CmdLineOption Option)
{
/* Create a new CmdLineOptionsSet and add the CmdLineOption */
    if (mLastOption == 0)
    {
        mOptionsSet = new CmdLineOptionsSet;
        mOptionsSet->Option = Option;
        mOptionsSet->Next = 0;
        mLastOption = mOptionsSet;
    }
    else
    {
        mLastOption->Next = new CmdLineOptionsSet;
        mLastOption = mLastOption->Next;
        mLastOption->Option = Option;
        mLastOption->Next = 0;
    }

    if ((Option.Type == OPTION_ARG_NONE) && (Option.Variable != 0))
        *((int*)Option.Variable) = false;

    return;
}

void CmdLineParser::addOption (CmdLineOption* Option)
{
/* Loop as long as there are options in the array */
    while (Option->Long != 0)
    {
        addOption(*Option);
        Option++;
    }
    return;
}

int CmdLineParser::parseLine (int argc, char* argv[])
{
/* Loop through all arguments and parse them */
    int i, ret_code;
	//TODO: This might not work the same way on windows */
    for (i = 1; i < argc; ++i) //We begin with i=1 because argv[0] is the name of the program in itself
    { 
        if ((ret_code = parseArgument(argc, argv, &i)))
            return ret_code;        
    }
    return SUCCESS;
}


int CmdLineParser::parseArgument(int argc, char* argv[], int* Position)
{
    std::string s_arg;
    char* c_arg1 = argv[*Position];
    CmdLineOption* CurrentOption;

    s_arg.assign(c_arg1);

/* First we'd like to know what kind of argument we are processing. */
    switch (getArgFormat(c_arg1))
    {
	/* And then, we try to get the Option it refers to. */
        case OPTION_FORM_SHORT: CurrentOption = searchShortOption(c_arg1[1]); break;
        case OPTION_FORM_LONG: CurrentOption = searchLongOption(s_arg.erase(0,2)); break;
        default : return ERROR_PARSE_ERR;
    }

/* If no option has been found, there is an error in the command line! */
    if (CurrentOption == 0)
        return ERROR_OPT_UNDEF;
    

/* Does this option requires help/version to be shown? */
    if (CurrentOption->Type == OPTION_HELP)
    {
        showHelp(argv[0]);
        exit(SUCCESS);
    }
    else if (CurrentOption->Type == OPTION_VERSION)
    {
    	showVersion();
    	exit(SUCCESS);
    }
    

/* If this option requires an extra argument, we mustn't be at the end of the command line! */
    if ((CurrentOption->Type != OPTION_ARG_NONE) && (*Position == argc))
        return ERROR_PARSE_ERR;    

/* What shall we do w/ this option? */
    switch ((*CurrentOption).Type) {
        case OPTION_ARG_NONE :
            if (CurrentOption->Variable != 0) (*(int*)CurrentOption->Variable) = true;
            mOptions[CurrentOption->Long] = "true";
            break;
        case OPTION_ARG_STRING :
            if (CurrentOption->Variable != 0) (*(std::string*)CurrentOption->Variable).assign(argv[++(*Position)]);
            mOptions[CurrentOption->Long].assign(argv[++(*Position)]);
            break;
        case OPTION_ARG_INT :
            if (CurrentOption->Variable != 0) 
               (*(int*)CurrentOption->Variable) = StringUtils::toInt(argv[++(*Position)]);            
            mOptions[CurrentOption->Long].assign(argv[++(*Position)]);
            break;
        default : return ERROR_OPT_UNDEF;
    }

    return SUCCESS;
}

int CmdLineParser::getArgFormat(char* arg)
{
    std::string s_arg;
    s_arg.assign(arg);

/* Check how long the argument is */
    if (s_arg.length() == 2)
    {
	/* and check if its a correct option */
        if (arg[0] != '-')
            return OPTION_FORM_UNKNOWN;        
        else
            return OPTION_FORM_SHORT;        
    }
    else if (s_arg.length() > 2)
    {
        if ((arg[0] != '-') | (arg[1] != '-'))
        	return OPTION_FORM_UNKNOWN;        
        else 
			return OPTION_FORM_LONG;
    }

    return OPTION_FORM_UNKNOWN;
}

CmdLineOption* CmdLineParser::searchLongOption(const std::string &s_Option)
{
/* Loop through all options and check if s_Option is one of them */
    CmdLineOptionsSet* CurrentOption = mOptionsSet;
    while (CurrentOption != 0)
    {
        if (!s_Option.compare(CurrentOption->Option.Long))
           return &CurrentOption->Option;        
        CurrentOption = CurrentOption->Next;
    }
    return 0;
}


CmdLineOption* CmdLineParser::searchShortOption(char c_Option)
{
/* Loop through all options and check if c_Option is one of them */
    CmdLineOptionsSet* CurrentOption = mOptionsSet;
    while (CurrentOption != 0) 
    {
        if (CurrentOption->Option.Short == c_Option) 
            return &CurrentOption->Option;        
        CurrentOption = CurrentOption->Next;
    }
    return 0;
}

void CmdLineParser::showHelp(char* command)
{
    CmdLineOptionsSet* CurrentOption = mOptionsSet;

/* Print some general informatins about the program */
	printf("This is the command line help for %s %s\n\n", mApplicationTitle.c_str(), mApplicationVersion.c_str());
    printf("%s", command);

/* Print all possible options */
    while (CurrentOption != 0)
    {
        if (CurrentOption->Option.ArgHelper[0] != '\0') 
            printf(" [-%c|--%s %s]", CurrentOption->Option.Short, CurrentOption->Option.Long, CurrentOption->Option.ArgHelper);
        else 
            printf(" [-%c|--%s]", CurrentOption->Option.Short, CurrentOption->Option.Long);        
        CurrentOption = CurrentOption->Next;
    }
    printf("\n\n");

    CurrentOption = mOptionsSet;
/* Print the options with detailed informations */
    while (CurrentOption != 0)
    {
        if (CurrentOption->Option.Type > 0) 
            printf("-%c,--%s %s\t\t%s\n", CurrentOption->Option.Short, CurrentOption->Option.Long, CurrentOption->Option.ArgHelper, CurrentOption->Option.HelpText);
        else 
            printf("-%c,--%s\t\t%s\n", CurrentOption->Option.Short, CurrentOption->Option.Long, CurrentOption->Option.HelpText);
        CurrentOption = CurrentOption->Next;
    }
    printf("\n");
}

void CmdLineParser::showVersion(void)
{
/* Show some version informations */
	printf("%s %s\n\n", mApplicationTitle.c_str(), mApplicationVersion.c_str());
	printf("%s is licensed under the terms of the GPL. See COPYING for more informations.\n", mApplicationTitle.c_str());
	printf("Copyright (c) 2007 openSpeak Team (http://openspeak-project.org)\nFor authors see AUTHORS file\n");
}

bool CmdLineParser::getSwitch(const std::string &option)
{
/* Search for the value and return true if its not empty, false or 0 */
    std::string value = searchInMap(option);
    if (value != "" && value != "false" && value != "0")
        return true;
    else
        return false;
}

std::string CmdLineParser::getStringValue(const std::string &option)
{
/* Return the value directly */
    return searchInMap(option);
}

int CmdLineParser::getIntValue(const std::string &option)
{
/* Return the value after converting it */
    return StringUtils::toInt(searchInMap(option));
}

std::string CmdLineParser::searchInMap(const std::string &option) 
{
/* Search in the map and return the string or an empty string */
    ConfigMap::const_iterator tempMap(mOptions.find(option));
    if (tempMap != mOptions.end())
    	return tempMap->second;
    return "";
}

}

