/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "osLib.h"

namespace openSpeak
{

    namespace StringUtils
    {

        std::string toUpper(std::string &str)
        {
            std::transform(str.begin(), str.end(), str.begin(), (int(*)(int)) std::toupper);
            return str;
        }

        std::string toUpper(const std::string &str)
        {
            std::string tmp = str;
            std::transform(tmp.begin(), tmp.end(), tmp.begin(), (int(*)(int)) std::toupper);
            return tmp;
        }

        bool toBool(const std::string &str)
        {
            return (str == "true" || str == "1" || str == "yes");
        }

        int toInt(const std::string &str)
        {
            return atoi(str.c_str());
        }

        ushort toUShort(const std::string &str)
        {
            return static_cast<ushort>(strtoul(str.c_str(), 0, 10));
        }

        long toLong(const std::string &str)
        {
        	return atol(str.c_str());
        }

        float toFloat(const std::string &str)
        {
            return atof(str.c_str());
        }

		std::vector< std::string > split(const std::string &str, char where)
		{
			std::vector< std::string > tempvec;

			std::string::size_type index = str.find(where);
			tempvec.push_back(str.substr(0,index));
			while (index != std::string::npos)
			{
				std::string::size_type nextpos = str.find(where, index);
				std::string tmp = str.substr(index, nextpos - index);
				if (!tmp.empty())
					tempvec.push_back(tmp);
				if (nextpos == std::string::npos)
					break;
				else
					index = nextpos + 1;
			}
			return tempvec;			
		} 

    }

	bool sockcmp(const sockaddr_in &s1, const sockaddr_in &s2)
	{
		return ( (s1.sin_port == s2.sin_port) && (s1.sin_addr.s_addr == s2.sin_addr.s_addr) );
}

}
