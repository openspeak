/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "osLib.h"

namespace openSpeak
{

    CommonTimer::CommonTimer(Clock *clock)
    {
    /* Set the clock or create one */
        if (clock)
        {
            mClock = clock;
            mCreated = false;
        }
        else
        {
            mClock = new Clock();
            mCreated = true;
        }
    /* Add us to the clock as an observer and set some basic vars */
        mClock->addObserver(this);
        mCurrentTime = 0.0;
        mFrameTime = 0.0;
        mLastTime = 0.0;
        mStartTime = mClock->getTime();
        mPaused = false;
    }

    CommonTimer::~CommonTimer()
    {
    /* Removes us as an observer and delete the clock if we created it */
        mClock->removeObserver(this);
        if (mCreated)
            delete mClock;
    }

    void CommonTimer::notify()
    {
    /* Get the current time from the clock and calculate the actual frametime */
        if (mPaused)
            return;
        mCurrentTime = mClock->getTime() - mStartTime;
        mFrameTime = mCurrentTime - mLastTime;
        mLastTime = mCurrentTime;

    }

    void CommonTimer::pause()
    {
    /* Pause the timer */
        if (!mPaused)
            mPaused = true;
    }

    void CommonTimer::resume()
    {
    /* Resume the timer and set a new starttime */
        if (!mPaused)
            return;
        double newtime = mClock->getTime();
        mStartTime = newtime - mStartTime - mCurrentTime;
        mPaused = false;
    }

}
