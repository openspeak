/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "osLib.h"

namespace openSpeak
{

UDPConnection::UDPConnection(bool _client) : mClient(_client)
{
/* Set some basic values */
    mSocket = 0;
    mConnected = false;
    mBound = false;
    mServer.sin_port = 0;
    mReadIt = mWriteIt = false;

/* On Win32 start the winsock functionality */
#ifdef OS_PLATFORM_WIN32
    WSAData wsa;
    WSAStartup(MAKEWORD(2,0), &wsa);
#endif
}

UDPConnection::~UDPConnection()
{
/* If we're still connected or bound, disconnect */
    if (isConnected())
        disconnect();
    else if (isBound())
        close();

#ifdef OS_PLATFORM_WIN32
    WSACleanup();
#endif
}

bool UDPConnection::select(void)
{
	if (!isConnected() && !isBound())
		return false;

/* Readd our socket to the fdsets */
	FD_SET(mSocket, &mRead);
	FD_SET(mSocket, &mWrite);

/* Select on the socket and check if we can read or write sth. */
	timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 1;
    if (::select((int)mSocket + 1, &mRead, &mWrite, 0, &tv) == -1)
        EXCEPTION("failed to select on socket")

/* Set all required variables and return if somethings true */
	mReadIt = FD_ISSET(mSocket, &mRead) == 1;
	mWriteIt = FD_ISSET(mSocket, &mWrite) == 1 && !mSendQueue.empty();
	return ( mWriteIt || mReadIt );
}

void UDPConnection::process()
{
/* Check if we're connected or bound, else return */
    if (!isConnected() && !isBound())
        return;

/* If we got something to send */
    if (mWriteIt && !mSendQueue.empty())
    {
		if (mClient)
	/* Send it the client way */
			sendAll(mSendQueue.front().first);
		else
	/* Or the server way */
			sendAll(mSendQueue.front().first, mSendQueue.front().second);

	/* delete the entry after sending */
		mSendQueue.pop();
    	mWriteIt = false;
    }
/* If we got something to read */
    if (mReadIt)
    {
		struct sockaddr_in sa;
        int sa_len = sizeof(struct sockaddr);
        const int bufsize = 16384;
        char buf[bufsize];
    /* Recieve the packet from the socket and check for errors */
#ifdef OS_PLATFORM_WIN32
        int n = recvfrom(mSocket, (char*)&buf, bufsize, 0, (struct sockaddr *)&sa, &sa_len);
		if (n == -1 && h_errno != WSAEWOULDBLOCK)
#else
		int n = recvfrom(mSocket, reinterpret_cast<char*>(&buf), bufsize, 0, (struct sockaddr *)&sa, (socklen_t*)&sa_len);
		if (n == -1 && errno != EWOULDBLOCK)
#endif
        {
            std::stringstream tmp;
#ifdef OS_PLATFORM_WIN32
			tmp << "error while recieving packet. Error: " << h_errno;
#else
            if (errno == 111)
                tmp << "couldn't establish a connection to the server";
            else
                tmp << "error while recieving packet. Error: " << errno;
#endif
            EXCEPTION(tmp.str())
        }
		else
		{
		/* Parse the recieved packet and get it to the place it belongs */
			onRecieve(std::string(buf, n), sa);
		}
		mReadIt = false;
    }
}


void UDPConnection::connect(const std::string &server, const ushort port, const std::string &nick, const std::string &password)
{
/* Connect to the server */
    if (!mClient)
        return;

    mPassword = password;

/* Get the ip if server is not an ip adress */
    hostent *he;
    if ((he = gethostbyname(server.c_str())) == 0)
        EXCEPTION("couldn't get the hostname")

/* Try to open a socket */
    if ((mSocket = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
        EXCEPTION("failed to open a socket")

/* Set all properties of the server */
    mServer.sin_family = AF_INET;
    mServer.sin_port = htons(port);
    mServer.sin_addr = *((struct in_addr *)he->h_addr);
    memset(&(mServer.sin_zero), '\0', 8);

/* Finally connect to the server */
    if (::connect(mSocket, (sockaddr*)&mServer, sizeof(sockaddr)) == -1)
        EXCEPTION("connect to server " + server + " failed!")

/* Set the socket non blocking */
#ifdef OS_PLATFORM_WIN32
	u_long arg = 1;
	ioctlsocket(mSocket, FIONBIO, &arg);
#else
	fcntl(mSocket, F_SETFL, O_NONBLOCK);
#endif

/* We're connected now */
    mConnected = true;
    FD_ZERO(&mRead);
    FD_ZERO(&mWrite);

/* and challenge the server afterwards */
    OutStream out;
    out << ID_CHALLENGE << nick;
    if (!password.empty())
        out << password;
    send(out);
}

void UDPConnection::disconnect()
{
    if (!isConnected() || !mClient)
        return;

/* Close the socket and reset it */
    sockclose(mSocket);
    mSocket = 0;

/* We're disconnected now */
    mConnected = false;

/* Clear all msg queues */
    while (!mSendQueue.empty())
        mSendQueue.pop();
}

bool UDPConnection::isConnected()
{
    if (mClient)
        return mConnected;
    else
        return false;
}

void UDPConnection::bind(const ushort port)
{
    if (mClient) return;
    if (isBound()) close();

/* Try to open a socket */
    if ((mSocket = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
        EXCEPTION("failed to open a socket")

/* Set the own settings */
    sockaddr_in myaddr;
    myaddr.sin_family = AF_INET;
    myaddr.sin_port = htons(port);
    myaddr.sin_addr.s_addr = INADDR_ANY;
    memset(&(myaddr.sin_zero), '\0', 8);

/* Try to bind the server to a specific port */
    if (::bind(mSocket, (struct sockaddr *)&myaddr, sizeof(struct sockaddr)) == -1)
    {
        std::ostringstream o;
        o << port;
        EXCEPTION(std::string("couldn't bind to port ") + o.str())
    }

/* Set the socket non blocking */
#ifdef OS_PLATFORM_WIN32
	u_long arg = 1;
	ioctlsocket(mSocket, FIONBIO, &arg);
#else
	fcntl(mSocket, F_SETFL, O_NONBLOCK);
#endif

    FD_ZERO(&mRead);
    FD_ZERO(&mWrite);

    mBound = true;
}

void UDPConnection::close()
{
    if (mClient || !isBound())
        return;

/* Disconnect */
    sockclose(mSocket);
    mBound = false;

/* And clear the queues */
    while (!mSendQueue.empty())
        mSendQueue.pop();
}

bool UDPConnection::isBound()
{
    if (!mClient)
        return mBound;
    else
        return false;
}

void UDPConnection::addCallback(const ushort type, const CallbackFunction function)
{
/* Search for an existing callback and replace the function or create a new callback */
    CallbackMap::iterator it = mCallbacks.find(type);
    if (it != mCallbacks.end())
        it->second = function;
    else
        mCallbacks.insert(std::make_pair(type, function));
}

void UDPConnection::removeCallback(const ushort type)
{
/* Search for the callback and remove it */
    CallbackMap::iterator it = mCallbacks.find(type);
    if (it != mCallbacks.end())
        mCallbacks.erase(it);
}

void UDPConnection::removeListener(Listener* list)
{
/* Search for the listener and remove it if found */
    ListenerVector::iterator it;
    for (it = mListeners.begin(); it != mListeners.end(); ++it)
    {
        if (*it == list)
            mListeners.erase(it);
    }
}

void UDPConnection::sendAll(const std::string &pkt)
{
    if (mClient)
        sendAll(pkt, mServer);
    else
        EXCEPTION("You should use sendAll(std::string, sockaddr_in) if you're a server")
}

void UDPConnection::sendAll(const std::string &pkt, const sockaddr_in &to)
{
    size_t total = 0;
    size_t bytesleft = pkt.size();

/* While we got something left to send we're sending it */
    while(total < pkt.size())
    {
        int n = ::sendto(mSocket, pkt.substr(total).c_str(), (int)bytesleft, 0, (struct sockaddr*)&to, sizeof(struct sockaddr));
        if (n == -1)
            EXCEPTION("got an error while sending")
        total += n;
        bytesleft -= n;
    }
}

void UDPConnection::onRecieve(const std::string &pkt, const sockaddr_in &from)
{
/* Create the arriving packet */
    Packet* tmp = new Packet();

/* Enter all important data into the Packet and tokenize the data */
    std::string::size_type index = pkt.find(' ');
    if (index == std::string::npos)
    {
        tmp->type = StringUtils::toUShort(pkt);
        tmp->data = "";
    }
    else
    {
        tmp->type = StringUtils::toUShort(pkt.substr(0, index));
        tmp->data = pkt.substr(++index);
    }

    tmp->sender = from;

/* Notify all registered listeners */
    for (ListenerVector::const_iterator it = mListeners.begin(); it != mListeners.end(); ++it)
        (*it)->onPacket(tmp);

/* Parse the arriving packet and call the corresponding callback function */
    CallbackMap::const_iterator it = mCallbacks.find(tmp->type);
    if (it != mCallbacks.end())
        it->second(this, tmp);
    else
    {
        std::stringstream buf;
        buf << "Got unknown packet of type: " << tmp->type;
        LOG(buf.str())
    }

/* Delete the packet */
    delete tmp;
}

}
