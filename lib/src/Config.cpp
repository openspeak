/**  openSpeak - The open source VoIP application
  *  Copyright (C) 2006 - 2007  openSpeak Team (http://openspeak-project.org)
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License along
  *  with this program; if not, write to the Free Software Foundation, Inc.,
  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  */

#include "osLib.h"

#include <stdarg.h>

namespace openSpeak
{
    Config::Config(const std::string &filename, bool newfile)
    {
        mFile = filename;
        mDoc = 0;
        mParsed = false;
		if (newfile && filename != "")
			createFile(filename);
    }

    Config::~Config()
    {
    /* Save the currently loaded file */
    	if (mParsed)
        	save();
        if (mDoc)
            delete mDoc;
    }

    void Config::setFilename(const std::string &filename)
    {
        if (mFile == filename || filename == "")
            return;

    /* Save the current file first, then clear the options map and parse the new file */
        save();
        mFile = filename;
        mOptions.clear();
    }

	void Config::createFile(const std::string &filename)
	{
		if (filename != "")
			mFile = filename;

		if (mDoc && mParsed)
		{
			save();
			delete mDoc;
		}
		else if (mDoc)
		{
			delete mDoc;
		}

	/* Clear the settings map */
		mOptions.clear();

	/* Check if the file really doesn't exists */
		std::fstream ftest;
		ftest.open(mFile.c_str(), std::ios::in);
		if (ftest.is_open())
			EXCEPTION("file already exists!")
		ftest.close();

	/* Create a TiXmlDocument and add the root element */
		mDoc = new TiXmlDocument;
		TiXmlElement *elem = new TiXmlElement("openSpeak");
		mDoc->LinkEndChild(elem);

		mParsed = true;
		save();
	}

    bool Config::parse()
    {
        if (mDoc)
            delete mDoc;
        mDoc = new TiXmlDocument;

    /* Check if the file exists */
        std::fstream ftest;
		ftest.open(mFile.c_str(), std::ios::in);
		if (!ftest.is_open())
        {
            createFile();
            return false;
        }
		ftest.close();

    /* Load the xml file */
        if (!mDoc->LoadFile(mFile))
            EXCEPTION("loading of xml file " + mFile + " failed")

    /* Get the root element */
        TiXmlElement *root = mDoc->FirstChildElement("openSpeak");
        if (!root)
            EXCEPTION("no or wrong document root found")

    /* Get the first option */
        TiXmlElement *option = root->FirstChildElement();
        std::string name, value;

    /* Iterate through all options and add the values into the map */
        while(option)
        {
            name = option->Value();
        /* We got a value so we save it */
            if (option->Attribute("value"))
            {
                value = option->Attribute("value");
                mOptions.insert(std::make_pair(name, value));
            }
        /* It got no value but childs? lets parse them */
            else if (option->FirstChild())
            {
                _parseNode(option);
            }
        /* Must be a boolean switch so its probably true */
            else
            {
                mOptions.insert(std::make_pair(name, "true"));
            }
        /* Get the next element */
            option = option->NextSiblingElement();
        }

        return mParsed = true;
    }

    void Config::save()
    {
        if (!mParsed)
            EXCEPTION("can't save a unparsed file")

    /* Get root element and the first child */
        TiXmlElement *root = mDoc->FirstChildElement("openSpeak");
        TiXmlElement *option = root->FirstChildElement();
        std::string name;

    /* Set all attributes to their new value */
        while (option)
        {
            name = option->Value();
            if (option->Attribute("value"))
                option->SetAttribute("value", mOptions[name]);
            else if (option->FirstChild())
                _saveNode(option);
            option = option->NextSiblingElement();
        }

    /* Save the file */
        if (!mDoc->SaveFile(mFile))
            EXCEPTION("saving file " + mFile + " failed")
    }

    void Config::setOption(const std::string &option, const std::string &value)
    {
        if (option == "")
            EXCEPTION("can't set an empty option")

    /* Get the root element */
        TiXmlElement *root = mDoc->FirstChildElement("openSpeak");
		if (!root)
			EXCEPTION("can't find document root")

    /* Check if the element already exists and create it if not*/
        if (!root->FirstChildElement(option) && !optionExists(option))
        {
        /* Check if this element is inside of a node */
            if (option.find('.') == std::string::npos)
            {
			/* It's a root element .. so create a new element and add it */
                TiXmlElement *tmp = new TiXmlElement(option);
                tmp->SetAttribute("value", value);
                mOptions.insert(std::make_pair(option, value));
                root->LinkEndChild(tmp);
                save();
            }
            else
            {
			/* Get the parent node, create a new node and save it */
                std::vector<std::string> nodes = StringUtils::split(option, '.');
                TiXmlElement *old, *current = 0;
                old = root;
                for (ushort i = 0; i < nodes.size(); ++i)
                {
                    if (old->FirstChild(nodes[i]))
                    {
                        old = old->FirstChildElement(nodes[i]);
                    }
                    else
                    {
                        current = new TiXmlElement(nodes[i]);
                        old->LinkEndChild(current);
                        old = current;
                    }
                }
                current->SetAttribute("value", value);
                mOptions.insert(std::make_pair(option,value));
                save();
            }
        }
        else if (mOptions[option] != value)
        {
    /* Save the changes */
            mOptions[option] = value;
            save();
        }
    }

	bool Config::checkOptions(const char* options, ...)
	{
		if (!optionExists(options))
			return false;
	/* Iterate through all options in the array and check for their existance */
		va_list arg;
		const char* option;
		va_start(arg, options);
		option = va_arg(arg, const char*);
		while(strlen(option) != 0)
		{
			if (!optionExists(option) && strlen(option) > 1)
			{
				va_end(arg);
				return false;
			}
			option = va_arg(arg, const char*);
		}
		
		va_end(arg);
		return true;
	}

    bool Config::optionExists(const std::string &option)
    {
	/* Search for the option in the config */
        ConfigMap::iterator it = mOptions.find(option);
        return it != mOptions.end();
    }

    void Config::_parseNode(TiXmlElement *node, const std::string &parentname)
    {
        std::string parent;
    /* Set the "pretag" of all cumulated parents names */
        if (parentname != "")
            parent = parentname + "." + node->Value() + ".";
        else
            parent = node->Value() + std::string(".");
    /* Loop through all nodes */
        TiXmlElement* child = node->FirstChildElement();
        while(child)
        {
        /* This is the same as in parse() */
            std::string name = parent + child->Value();
            if (child->Attribute("value"))
            {
                std::string value = child->Attribute("value");
                mOptions.insert(std::make_pair(name, value));
            }
            else if (child->FirstChild())
            {
                _parseNode(child, parent);
            }
            else
            {
                mOptions.insert(std::make_pair(name, "true"));
            }
            child = child->NextSiblingElement();
        }
    }

    void Config::_saveNode(TiXmlElement *node, const std::string &parentname)
    {
        std::string parent;
    /* Set the "pretag" of all cumulated parents names */
        if (parentname != "")
            parent = parentname + "." + node->Value() + ".";
        else
            parent = node->Value() + std::string(".");
    /* Loop through all nodes */
        TiXmlElement* child = node->FirstChildElement();
        while(child)
        {
        /* This is the same as in save() */
            std::string name = parent + child->Value();
            if (child->Attribute("value"))
                child->SetAttribute("value", mOptions[name]);
            else if (child->FirstChild())
                _saveNode(child, parent);
            child = child->NextSiblingElement();
        }
    }

}
